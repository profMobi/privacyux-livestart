<?php
//This is the documentation page for PrivacyUX Livestart



//Get the MySQL/ConsentCheq credentials
include("cred.inc");


?><!DOCTYPE html>



<!--
<?php
echo "DEBUG BLOCK"  . PHP_EOL;
?>
-->

<?php
include 'shared.php';

//write analytics 
//analytics("PAGE LOAD - LOGIN.PHP");

?>

<html lang="en">

<head>

<?php
readfile('html/head.html');
readfile('html/style.html');
?>


</head>
<body>


<?php

readfile ('html/blankNavigation.html');

?>




<!-- Page Content -->
<div class="container">


<br/><br/>



<h1 id="headingMainMessage" class="my-4">Need Help?</h1>

<div class="row">
	<!-- Left Panel -->
	<div id="divLeftPanel" class="col-sm-6 col-md-6">

		<p>If you need help, please contact us at <a href="mailto:support@agecheq.com">support@agecheq.com</a> and we'll do our best to help.</p>
	
	</div>

	<!-- Right Panel -->
	<div id="divRightPanel" class="col-sm-6 col-md-6">


	
	</div>
</div>	
<!-- end of the row -->




</div>
<!-- end of the container -->

<br/><br/>



<?php
readfile ('html/bootstrapCore.html');
readfile ('html/footer.html');
?>
	
	
</body>


</html>