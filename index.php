<?php

	//check for a cookie
	$cookie_name = 'privacyux_uid';
	if(!isset($_COOKIE[$cookie_name])) {
		header("Location: build.php");
	} else {
		header("Location: login.php?uid=" . $_COOKIE[$cookie_name]);
	}

?>