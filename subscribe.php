<?php
//This is the PrivacyUX subscription page


//Test to confirm that we have a valid login
session_start();
if (!isset($_SESSION['auth'])) {
	$_SESSION['auth'] = "0";
	$_SESSION['userid'] = "";
	$_SESSION['level'] = "";
} else {
	
	$level = "";
}

//set the level of the login	
if (isset($_SESSION['level'])) {
	$level = $_SESSION['level'];	
	$userid = $_SESSION['userid'];
} else {
	$level = "";	
}


//global variables
$pStatusMessage = "";


?><!DOCTYPE html>



<!--
<?php
echo "DEBUG BLOCK"  . PHP_EOL;
echo "AUTHORIZATION:" .  $_SESSION['auth'] . PHP_EOL;
echo "LEVEL:" .  $_SESSION['level'] . PHP_EOL;
echo "USER ID:" .  $_SESSION['userid'] . PHP_EOL;
?>
-->

<?php
include 'shared.php';
include 'cred.inc';
?>

<html lang="en">

<head>

<?php
readfile('html/head.html');
readfile('html/style.html');
?>


</head>
<body onload="doLoad();">


<?php if ($_SESSION['auth'] == "1") {

readfile('html/navigation.html');

} else { 

readfile ('html/blankNavigation.html');

}  ?>


<!-- Page Content -->
<div class="container">


<br/><br/>

<?php
	if ($_SESSION['auth'] == "1" ) {
		//if there IS a valid login, show the subscription page	
?>



<div class="row">
	<!-- Left Panel -->
	<div id="divLeftPanel" class="col-sm-6 col-md-6">

<?php
	
	//test the level
	if ($level > 0) { ?>
	
		<h1 class="my-4">Thanks for subscribing to PrivacyUX Livestart!</h1>
		<p><a href="login.php">Return to Home</a></p>
		
		
<?php } else { ?>	
	
		<h1 class="my-4">Subscribe</h1>
		
<p>
PrivacyUX LiveStart gives you rapid and easily verified compliance with CCPA while giving your customers a trust-building privacy experience similar to Apple's recently introduced App Store privacy.</p>
<p>The service is priced at just $200 per month of service ($2400 annually).  Until January 31, 2021, we are offering an introductory annual subscription price of just $1995.00. </p>

<!--			
<p style="color:#FF1B00;">FOR TESTING PURPOSES, THIS CHARGE IS CURRENTLY SET AT $0.01</p>
-->			
			
		<script src="https://www.paypal.com/sdk/js?client-id=<?php echo $payPalClientID; ?>"> //Live client id
		</script>
		<div id="paypal-button-container"></div>
		<script src="https://www.paypal.com/sdk/js?client-id=<?php echo $payPalClientID; ?>&currency=USD" data-sdk-integration-source="button-factory"></script>
		<script>
		  paypal.Buttons({
			  style: {
				  shape: 'pill',
				  color: 'gold',
				  layout: 'vertical',
				  label: 'paypal',
				  
			  },
			  createOrder: function(data, actions) {
				  return actions.order.create({
					  purchase_units: [{
						  amount: {
							  value: '1995.00'
						  }
					  }]
				  });
			  },
			  onApprove: function(data, actions) {
				  return actions.order.capture().then(function(details) {
					  console.log(details);
					  transactionComplete(details);
				  });
			  }
		  }).render('#paypal-button-container');
		</script>	
	
<?php } ?>	
	
	
	
	</div>
	<!-- Right Panel -->
	<div id="divRightPanel" class="col-sm-6 col-md-6">

		<div class="divSpacerTop"><br/></div>	
	
		<!-- cell phone example -->
		<div id="phone" style="position:relative;display:none;">
			<div id="phoneSilhouette" class="phone"></div>
			<div id="phoneButtonLeft0" class="phone"></div>
			<div id="phoneButtonLeft1" class="phone"></div>	
			<div id="phoneButtonLeft2" class="phone"></div>	
			<div id="phoneButtonRight0" class="phone"></div>			
			<div id="phoneContent" class="phone"><iframe id="framedPFIN" src="privacyNotice.php?uid=<?php echo $_SESSION['userid'] ?>" style="width:100%;height:100%; border:none;"></iframe></div>
		</div>
	
	</div>	

<!-- end of the row -->	
</div>	

<?php
	} else {
		//return to the login page if there is no login
		header("Location: login.php");
		
	}
?>



<br/><br/>

<!-- end of the container -->
</div>
	
	
<?php
readfile('html/footer.html');
readfile('html/bootstrapCore.html');
readfile('html/modal.html');
?>	

	
</body>
<script>
function transactionComplete(paypalTransactionDetails) {
	var userid = "<?php echo $_SESSION['userid'] ?>";
	
	var objPostData = {"cmd":"setuserlevel","userid":userid,"userlevel":"1"};
	
	//trigger a call to update the user's level
	$.post("usertools.php",objPostData,function(data) {		

		//turn the string data returned back into a JSON object
		var objReturnData = JSON.parse(data);
		
		console.log(data);

		if (objReturnData.rtn != "ok") {
			
			console.log(data);
			
		} else {
			
			window.location="subscriptionSuccess.php";
		}

	});	
	
	
}

//called when this page loads
function doLoad() {
	
	//hide the gradient background in the PFIN and resize it
	document.getElementById("framedPFIN").contentWindow.document.getElementsByTagName("body")[0].style.backgroundImage = "none";
	onResize();
	
}


//called when the PFIN resizes
function onResize() {
	
	var phoneWidth = 440;
	
	//console.log(window.innerHeight);
	var scaleRatio = (window.innerHeight * 0.8) / 1000;
	var maxWidth = document.getElementById("divLeftPanel").offsetWidth;	
	
	//do I scale based on height or width?
	if ((phoneWidth * scaleRatio) >= maxWidth) {
		scaleRatio = maxWidth/phoneWidth;
	}	

	document.getElementById("phone").style.transform 	= "scale(" + scaleRatio + ")";
	//document.getElementById("phone").style.top 		=  "-" + (window.innerHeight * 0.1) + "px";
	document.getElementById("phone").style.display		="block";	
	
}

//set the window's resize event
window.onresize = onResize;

</script>
</html>