<?php

/*
This library picks out all the PFIN data from the database and delivers it as three JSON objects. 
It is ugly, and needs some tweaking, but it can be substituted just for a flat JSON file. 
*/

//Set the max timeout at 5 minutes
ini_set('max_execution_time', 300);

//for analytics 
include 'shared.php';

//this functon replaces special characters and makes them safe
function replaceStuff($strToReplace) {
	$strOutput = $strToReplace;
	$strOutput = str_replace("\\","\\\\",$strOutput);
	return $strOutput;
}

function getJSON($iNoticeID = null) {
	
	$returnJSON = "{";
	
	//if no notice id passed, return the blank one
	if (is_null ($iNoticeID)) {
		
		$returnJSON = '{
  "id": "0",
  "name": "PFIN Soft Creation",
  "title": "CCPA Notice at Collection",
  "icon": "",
  "intro": "Click <span style=\'color:#007bff;\'>blue</span> facts for more detail.",
  "elements": [
    {
      "order": "0",
      "elementid": "",
      "questionid": "",
      "members": [
        {
          "memberid": "",
          "text": "Our company name",
          "format": "L",
          "link": "",
          "type": "fact"
        },
        {
          "memberid": "",
          "text": "Your organization\'s name",
          "format": "R",
          "link": "",
          "type": "info"
        }
      ]
    },
    {
      "order": "3",
      "elementid": "",
      "questionid": "",
      "members": [
        {
          "memberid": "",
          "text": "Full privacy details",
          "format": "L",
          "link": "",
          "type": "fact"
        },
        {
          "memberid": "",
          "text": "our privacy policy",
          "format": "R",
          "link": "",
          "type": "info"
        }
      ]
    }
  ],
  "details": []
}';
		
	} else {
		
		//!!! TODO: test to make sure that the data coming back is actually good data
		$returnJSON = $returnJSON . getNoticeJSON($iNoticeID);
		
		$returnJSON = $returnJSON . ",\"elements\":" . getElementsArray($iNoticeID);

		$returnJSON = $returnJSON . ",\"details\":" . getDetailsArray($iNoticeID);	

		$returnJSON = $returnJSON . "}";
		
		//!!! TODO: test the final product before it is returned, passing an error if it doesn't work		
			
	}

	return $returnJSON;
}


function getNoticeJSON($iNoticeID) {
	//Get the credentials
	include "cred.inc";
	
	//Read into the database
	include "conn.inc";
	
	$returnJSON = "";
				
	//build SQL statement
	//$sql = "SELECT * FROM `notices` WHERE `id` = " .  $iNoticeID . ";";
	$sql = "SELECT `notices`.`id` as `id`, `notices`.`userid` as `userid`, `notices`.`name` as `name`, `notices`.`title` as `title`, `notices`.`icon` as `icon`, `notices`.`intro` as `intro`,`notices`.`timestamp` as `timestamp`, `users`.`level` as `level`  FROM `notices` INNER JOIN `users` ON `notices`.`userid` = `users`.`id` WHERE `notices`.`id` = " .  $iNoticeID . ";";	
	
	//send the query
	$result = $connection->query($sql);
	
	//if we have at least one matching account	
	if ($result->num_rows > 0) {
		
		//$totalRows = $result->num_rows;
		//$counter = 0;
		
		// get the data for each row
		while($row = $result->fetch_assoc()) {

			$returnJSON = $returnJSON . "\"id\":\"" . $row["id"] . "\","; 
			$returnJSON = $returnJSON . "\"name\":\"" . $row["name"] . "\","; 	
			$returnJSON = $returnJSON . "\"title\":\"" . $row["title"] . "\","; 	
			$returnJSON = $returnJSON . "\"icon\":\"" . $row["icon"] . "\","; 	
			$returnJSON = $returnJSON . "\"level\":\"" . $row["level"] . "\","; 	
			$returnJSON = $returnJSON . "\"intro\":\"" . $row["intro"] . "\""; 	

		}
	}	
	
	//write analytics
	analytics("PFIN LOAD - " . $iNoticeID);
	
	return $returnJSON;
	
}

function getElementsArray($iNoticeID) {
	//Get the credentials
	include "cred.inc";
	
	//Read into the database
	include "conn.inc";

				
	//build SQL statement
	$sql = "SELECT `order`,`elementid`, `members`.`id` as `memberid`,`text`,`format`,`link`,`type` FROM `elements` INNER JOIN `members` ON `elements`.`id` = `members`.`elementid` WHERE `noticeid` =" .  $iNoticeID . " ORDER BY `order`,`memberid`;";
	
	//send the query
	$result = $connection->query($sql);
	



	//elements array
	$arrElements = [];

	//holds the current element object
	$objElement = new stdClass();
	$objElement -> elementid = "SKIP_FIRST_ITERATION";
	$objElement -> members = [];

	// get the data for each row
	while($row = $result->fetch_assoc()) {

		if (($objElement -> elementid != $row["elementid"])&&($objElement -> elementid != "SKIP_FIRST_ITERATION")) {

	   		//push the element object to the elements array
	   		array_push($arrElements,$objElement);

			//clear the element object
			$objElement = new stdClass();
			$objElement -> members = [];

		}

		//write the element object
   		$objElement -> order = $row["order"];
   		$objElement -> elementid = $row["elementid"];

   		//create the member object
  		$objMember = new stdClass(); 	
  		
  		//write the member object
  		$objMember -> memberid = $row["memberid"];
  		$objMember -> text = $row["text"];	
  		$objMember -> format = $row["format"];	
  		$objMember -> link = $row["link"];		
  		$objMember -> type = $row["type"];	

   		//push the member into the element
   		array_push($objElement -> members, $objMember);
	}

	//push the final element object to the elements array
	array_push($arrElements,$objElement);

	return json_encode($arrElements);
	
}

function getDetailsArray($iNoticeID) {
	
	//Get the credentials
	include "cred.inc";
	
	//Read into the database
	include "conn.inc";
	
	$returnElementJSON = "[";
				
	//build SQL statement
	$sql = "SELECT `eidlink`,`head`,`foot`,`details`.`linktext` as `detaillinktext`, `details`.`linkurl` as `detaillinkurl`, `text`,`info`,`factors`.`link` as `factorlink`,`order` FROM `details` LEFT JOIN `factors` ON `details`.`id` = `factors`.`detailid` WHERE `noticeid` =" .  $iNoticeID . " ORDER BY `eidlink`,`order`;";
	
	//send the query
	$result = $connection->query($sql);
	
	//if we have at least one matching account	
	if ($result->num_rows > 0) {
		
		$iCounter = 0;
		
		$eidlink = "";
		$head = "";
		$foot = "";
		$detaillinktext = "";
		$detaillinkurl = "";
		
		$arrFactors = [];
		$detailJSON = "";
		
		// get the data for each row
		while($row = $result->fetch_assoc()) {
			
			//echo "<br/><b>" . $eidlink . "</b><br/>";

			if (($eidlink != $row["eidlink"])&&($eidlink != "")) {
				
				if ($iCounter > 0) {
					$detailJSON = ",";
				}
				$iCounter = $iCounter +1;
				
				//complete writing the JSON for the detail
				$detailJSON = $detailJSON . "{";
				$detailJSON = $detailJSON . "\"eidlink\":\"" . $eidlink. "\","; 
				if (addslashes($head) == 'BLANK') { //if a BLANK is passed, just ignore this entry
					$detailJSON = $detailJSON . "\"head\":\"\","; 				
				} else {
					$detailJSON = $detailJSON . "\"head\":\"" . str_replace("\\'","'",addslashes($head)) . "\","; 		
				}				
				
				if (addslashes($foot) == 'BLANK') { //if a BLANK is passed, just ignore this entry
					$detailJSON = $detailJSON . "\"foot\":\"\","; 				
				} else {
					$detailJSON = $detailJSON . "\"foot\":\"" . str_replace("\\'","'",addslashes($foot)) . "\","; 					
				}

				if (addslashes($detaillinktext) == 'BLANK') {
					$detailJSON = $detailJSON . "\"detaillinktext\":\"\",";
				} else {
					$detailJSON = $detailJSON . "\"detaillinktext\":\"" . $detaillinktext . "\","; 					
				}

				$detailJSON = $detailJSON . "\"detaillinkurl\":\"" . $detaillinkurl . "\","; 
				$detailJSON = $detailJSON . "\"factors\":[";
				
				for ($x=0;$x<count($arrFactors);$x++) {
					if ($x > 0) {
						$detailJSON = $detailJSON . ",";
					}
					$detailJSON = $detailJSON . $arrFactors[$x]; 
					
				}
				
				$detailJSON = $detailJSON . "]}";
				$returnElementJSON = $returnElementJSON . $detailJSON;
				
				//clear the detail data
				$eidlink = "";
				$head = "";
				$foot = "";
				$detaillinktext = "";
				$detaillinkurl = "";
				
				$arrFactors = array();
				$factorJSON = "";
				
				
			}
			
			$eidlink = $row["eidlink"];
			$head = $row["head"] ;
			$foot = $row["foot"];
			
			$detaillinktext = $row["detaillinktext"];
			$detaillinkurl = $row["detaillinkurl"];

			
			if ($row["text"] != '') {
				$factorJSON = "{";
				$factorJSON = $factorJSON . "\"text\":\"" . str_replace("\\'","'",addslashes($row["text"])) . "\","; 	
				$factorJSON = $factorJSON . "\"info\":\"" . str_replace("\\'","'",addslashes($row["info"])) . "\","; 	
				$factorJSON = $factorJSON . "\"factorlink\":\"" . $row["factorlink"] . "\","; 
				$factorJSON = $factorJSON . "\"order\":\"" . $row["order"] . "\"";			
				$factorJSON = $factorJSON . "}";				
			}
			
			$arrFactors[count($arrFactors)] = $factorJSON;
		}
		
			//write the final detail
			if ($iCounter > 0) {
				$detailJSON = ",";
			}
			$detailJSON = $detailJSON . "{";
			$detailJSON = $detailJSON . "\"eidlink\":\"" . $eidlink. "\","; 
			if (addslashes($head) == 'BLANK') { //if a BLANK is passed, just ignore this entry
				$detailJSON = $detailJSON . "\"head\":\"\","; 				
			} else {
				$detailJSON = $detailJSON . "\"head\":\"" . str_replace("\\'","'",addslashes($head)) . "\","; 		
			}				
			
			if (addslashes($foot) == 'BLANK') { //if a BLANK is passed, just ignore this entry
				$detailJSON = $detailJSON . "\"foot\":\"\","; 				
			} else {
				$detailJSON = $detailJSON . "\"foot\":\"" . str_replace("\\'","'",addslashes($foot)) . "\","; 					
			}

			if (addslashes($detaillinktext) == 'BLANK') {
				$detailJSON = $detailJSON . "\"detaillinktext\":\"\",";
			} else {
				$detailJSON = $detailJSON . "\"detaillinktext\":\"" . $detaillinktext . "\","; 					
			}

			$detailJSON = $detailJSON . "\"detaillinkurl\":\"" . $detaillinkurl . "\","; 
			$detailJSON = $detailJSON . "\"factors\":[";
			
			for ($x=0;$x<count($arrFactors);$x++) {
				if ($x > 0) {
					$detailJSON = $detailJSON . ",";
				}
				$detailJSON = $detailJSON . $arrFactors[$x]; 
				
			}
			
			$detailJSON = $detailJSON . "]}";
			$returnElementJSON = $returnElementJSON . $detailJSON;
				
	}	
	
	$returnElementJSON = $returnElementJSON . "]";
	
	return $returnElementJSON;	
	
}

//***********************************

function getEntries() {
	
	//Get the credentials
	include "cred.inc";
	
	//Read into the database
	include "conn.inc";
	
	$jsonEntries = "";
				
	//build SQL statement
	$sql = "SELECT prompts.EID, prompts.title, prompts.prompt, entries.data, entries.link, prompts.section, prompts.order, prompts.justify as promptjustify, prompts.fieldtype as promptfieldtype, entries.justify as entryjustify, entries.fieldtype as entryfieldtype FROM prompts LEFT JOIN entries ON prompts.EID = entries.EID ORDER BY section,prompts.order;";
	
	//send the query
	$result = $connection->query($sql);
	
	//if we have at least one matching account	
	if ($result->num_rows > 0) {
		
		$totalRows = $result->num_rows;
		$counter = 0;
		
		$jsonEntries = "[";
		// get the data for each row
		while($row = $result->fetch_assoc()) {
	
			$objEntry = "{";
			$objEntry = $objEntry . "\"id\":\"" . $row["EID"] . "\","; 
			$objEntry = $objEntry . "\"title\":\"" . $row["title"] . "\","; 			
			$objEntry = $objEntry . "\"prompt\":\"" . replaceStuff($row["prompt"]) . "\","; 
			$objEntry = $objEntry . "\"data\":\"" . $row["data"] . "\","; 
			$objEntry = $objEntry . "\"section\":\"" . $row["section"] . "\","; 
			$objEntry = $objEntry . "\"link\":\"" . $row["link"] . "\","; 
			$objEntry = $objEntry . "\"order\":\"" . $row["order"] . "\","; 	
			$objEntry = $objEntry . "\"promptjustify\":\"" . $row["promptjustify"] . "\","; 
			$objEntry = $objEntry . "\"promptfieldtype\":\"" . $row["promptfieldtype"] . "\","; 	
			$objEntry = $objEntry . "\"entryjustify\":\"" . $row["entryjustify"] . "\","; 
			$objEntry = $objEntry . "\"entryfieldtype\":\"" . $row["entryfieldtype"] . "\""; 				
			$objEntry = $objEntry . "}";
			$jsonEntries = $jsonEntries . $objEntry;
		
			//don't add a trailing comma
			$counter = $counter +1;
			if ($counter < $totalRows) {
				 $jsonEntries = $jsonEntries . ",";				
			}
		
		}
		
		$jsonEntries = $jsonEntries . "]";
		
	} 
	
	
	mysqli_close($connection);
	
	return $jsonEntries;

}

//This function builds a JavaScript array with the details in [0] and the factors in [1]
function getDetails() {
	//Get the credentials
	include "cred.inc";
	
	//Read into the database
	include "conn.inc";
	
	$jsonDetails = "";
	$jsonFactors = "";
				
	//build SQL statement
	$sql = "SELECT details.id,details.head,details.foot,details.hotlink FROM details;";
	
	//$sql = 	"SELECT details.id,details.head,details.foot,details.hotlink,prompts.prompt FROM details INNER JOIN entries on details.id = entries.link INNER JOIN prompts on  prompts.EID = entries.EID;";

	//send the query
	$result = $connection->query($sql);
	
	//if we have at least one matching account	
	if ($result->num_rows > 0) {	
	
		//build a JSON object with the details 
		$totalRows = $result->num_rows;
		$counter = 0;
		
		$jsonDetails = "[";
		// get the data for each row
		while($row = $result->fetch_assoc()) {
	
			$objEntry = "{";
			$objEntry = $objEntry . "\"id\":\"" . $row["id"] . "\",";
			$objEntry = $objEntry . "\"head\":\"" . str_replace("\"","\\\"",$row["head"]) . "\","; 			
			$objEntry = $objEntry . "\"foot\":\"" . str_replace("\"","\\\"",$row["foot"]) . "\","; 
			$objEntry = $objEntry . "\"link\":\"" . $row["hotlink"] . "\""; 
			$objEntry = $objEntry . "}";
			$jsonDetails = $jsonDetails . $objEntry;
		
			//don't add a trailing comma
			$counter = $counter +1;
			if ($counter < $totalRows) {
				 $jsonDetails = $jsonDetails . ",";				
			}
		
		}
		
		$jsonDetails = $jsonDetails . "]";		

	}
	

	
	//build SQL statement
	$sql = "SELECT * FROM factors;";

	//send the query
	$result = $connection->query($sql);
	
	//if we have at least one matching account	
	if ($result->num_rows > 0) {	
	
		//build a JSON object with the factors 
		$totalRows = $result->num_rows;
		$counter = 0;
		
		$jsonFactors = "[";
		// get the data for each row
		while($row = $result->fetch_assoc()) {
	
			$objEntry = "{";
			$objEntry = $objEntry . "\"id\":\"" . $row["detailid"] . "\",";
			$objEntry = $objEntry . "\"text\":\"" . str_replace("\"","\\\"",$row["text"]) . "\","; 
			$objEntry = $objEntry . "\"detail\":\"" . str_replace("\"","\\\"",$row["detail"]) . "\""; 			
			$objEntry = $objEntry . "}";
			$jsonFactors = $jsonFactors . $objEntry;
		
			//don't add a trailing comma
			$counter = $counter +1;
			if ($counter < $totalRows) {
				 $jsonFactors = $jsonFactors . ",";				
			}
		
		}
		
		$jsonFactors = $jsonFactors . "]";		

	
	}
	
	//return two arrays of arrays of objects
	return  $jsonDetails . "," . $jsonFactors;
	
}



//build a JSON object of the notice based on the ?noticeid= query string parameter
if (isset($_GET["noticeid"])) {
	echo getJSON($_GET['noticeid']);	
} else {
echo getJSON();
}




?>
