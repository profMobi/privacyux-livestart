<?php
//This is the unsubscribe page - passing the userid of the user to unsubscribe from future email

//Get the MySQL/ConsentCheq credentials
include("cred.inc");


?><!DOCTYPE html>


<!--
<?php
echo "DEBUG BLOCK"  . PHP_EOL;
?>
-->

<?php
include 'shared.php';

//write analytics 
//analytics("PAGE LOAD - LOGIN.PHP");
?>

<html lang="en">

<head>

<?php
readfile('html/head.html');
readfile('html/style.html');
?>


</head>
<body>


<?php

readfile ('html/blankNavigation.html');

?>




<!-- Page Content -->
<div class="container">


<br/><br/>



<h1 id="headingMainMessage" class="my-4">Unsubscribe</h1>

<div class="row">
	<!-- Left Panel -->
	<div id="divLeftPanel" class="col-sm-6 col-md-6">

<?php
if (isset($_GET["uid"])) {

	//get user id from the query string
	$userid = $_GET["uid"];

	include ("functions/helperFunctions.php");
	include ("functions/userFunctions.php");	
	include ("cred.inc");	
	
	//write to the database indicating that the user does not want to subscribe anymore
	$rtn = updateSubscription($userid,false);
	
	echo "<!-- " . $rtn . " -->";
	
	$objReturn = json_decode($rtn);

	
	if ($objReturn -> rtn == "ok") {
		

?>	
	
		<!-- user unsubscribed successfully -->  
		<p>You have successfully unsubscribed from all communications from PrivacyCheq with respect to PrivacyUX Livestart in the future.</p>
  
<?php  
	} else {
?>
		<!-- error unsubscribing -->
		<!-- <?php echo $objReturn -> rtnmsg;   ?> -->
		<p>There was an unexpected error unsubscribing from communications with PrivacyUX Livestart. Please write <a href="mailto:privacy@privacycheq.com">privacy@privacycheq.com</a> to unsubscribe instead.</p>		

<?php		
	}
	
	
} else {  
?>

	<!-- No userid passed - unable to unsubscribe -->
	<p>No user identifier passed, we are unable to unsubscribe you. Please make sure to link using the entire URL</p>

<?php
}
?>

	
	</div>

	<!-- Right Panel -->
	<div id="divRightPanel" class="col-sm-6 col-md-6">


	
	</div>
</div>	
<!-- end of the row -->




</div>
<!-- end of the container -->

<br/><br/>



<?php
readfile ('html/bootstrapCore.html');
readfile ('html/footer.html');
?>
	
	
</body>


</html>