<?php


// ----------------------------------------
// CREATE QR CODE AND RETURN URL
// ------------------------------------------
function generateQR($data,$errorCorrectionLevel = 'L',$matrixPointSize = 4) {
	
   
    //echo "<h1>PHP QR Code</h1><hr/>";
    
    //set it to writable location, a place for temp generated PNG files
    $PNG_TEMP_DIR = dirname(__FILE__).DIRECTORY_SEPARATOR.'temp'.DIRECTORY_SEPARATOR;
    
    //html PNG location prefix
    $PNG_WEB_DIR = 'temp' . DIRECTORY_SEPARATOR;

    include "phpqrcode/qrlib.php";    
    
    //ofcourse we need rights to create temp dir
    if (!file_exists($PNG_TEMP_DIR))
        mkdir($PNG_TEMP_DIR);
    

    if ($data != "") { 
    
        //it's very important that we don't just have a blank space
        if (trim($data) == '') {
				return "";
				die();
		}
		
		//echo "writing filename <br/><br/>";
            
        // user data
        $filename = $PNG_TEMP_DIR.'notice'.md5($data.'|'.$errorCorrectionLevel.'|'.$matrixPointSize).'.png';

		//echo $filename . "<br/><Br/>";		
		
		//echo "building QR <br/><br/>";		
		
        QRcode::png($data, $filename, $errorCorrectionLevel, $matrixPointSize, 2);
		
		//echo "QR Built <br/><br/>";	
        
    } else {   
		return "";
		die();   
        
    }    
        
    //return filename
    return  $PNG_WEB_DIR.basename($filename);  
    

}

?>