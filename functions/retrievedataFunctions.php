<?php

function getNoticeFromUserId($userid) {

	//Get the credentials
	include "cred.inc";
	
	//Read into the database
	include "conn.inc";
	
	//validate input
	if ($userid == "") {
		//no user passed
		$rtn = json_encode(array(
			"rtn"  => "fail",  
			"rtnmsg" => "there is no userid passed"
			));
			   
		return $rtn;	
		die();
	}

    //get the highest id associated with the userid
    $sql = "SELECT `id` FROM `notices` WHERE `userid` = '" . $userid . "' ORDER BY `id` DESC;";			


    //send the query
    $result = $connection->query($sql); 
    
    //get the number of affected rows
    $iRows = mysqli_affected_rows($connection);

    //get any SQL error
    $sqlError = mysqli_error($connection);
    
    if ($sqlError != "") {
        
        //sql error
        $rtn = json_encode(array(
            "rtn"  => "error",  
            "rtnmsg" => "error reading from database: " . $sqlError,
            "userid" => $userid,
            "id" => ""
            ));
               
        return $rtn;    
        die();      
        
    }

    if ( $iRows > 0 ) {

        $row = mysqli_fetch_assoc($result);

		//return success passing along the notice id
		$rtn = json_encode(array(
			"rtn"  => "ok",
			"rtnmsg" => "notice id retrieved successfully",
			"userid" => $userid,
			"id" => $row['id'],
			"sql" => $sql
			));
			
		return $rtn;	
		die();			
			
	} else {
		//user not found in the database
		$rtn = json_encode(array(
			"rtn"  => "fail",  
			"rtnmsg" => "there is no matching user for this userid in the database, or there is no notice created yet",
			"userid" => $userid,
			"id" => ""
			));
			   
		return $rtn;	
		die();
	}
        

	
}



?>