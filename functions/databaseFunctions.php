<?php


// ------------------------------------------
// CREATE NOTICE - CREATES NOTICE IN THE DATABASE
// ------------------------------------------

// Notice Table		
//   id - A unique identifier
//   userid - The unique GUID of the user this notice belongs to
//   name - The plain language name of the privacy facts notice
//   title - The title to display at the top of the privacy facts notice; typically "PRIVACY FACTS"
//   icon - The URL of an 50x50 pixel icon to display at the top of the notice
//   intro - The paragraph introducing the privacy facts notice
//   timestamp -  A timestamp of the last change

//Adds an entry into the "notice" table of the database
function createNotice($name, $title, $icon, $intro, $email, $userid) {

	//Get the credentials
	include "cred.inc";
	
	//Read into the database
	include "conn.inc";
	
	//!!!validate input
	
	//escape single quotes
	$name = formatQuotes($name);
	$title = formatQuotes($title);
	$intro = formatQuotes($intro);
	
	

	//If email and not user id is passed, get the userid from the email address, otherwise just use the user id
	if ($userid == "" && $email != "") {
		$sql = "INSERT INTO `notices` (`userid`,`name`, `title`, `icon`, `intro`) SELECT id, '" . $name . "','" . $title . "','" . $icon . "','" . $intro . "' FROM `users` WHERE `email` = '" . $email . "';";			
	} else {
		$sql = "INSERT INTO `notices` (`userid`,`name`, `title`, `icon`, `intro`) SELECT id, '" . $name . "','" . $title . "','" . $icon . "','" . $intro . "' FROM `users` WHERE `id` = '" . $userid . "';";		
	}

	//send the query
	$result = $connection->query($sql);

	//get the number of affected rows
	$iRows = mysqli_affected_rows($connection);
	
	//get the inserted id
	$insertedID = mysqli_insert_id($connection); 

	//get the SQL error
	$sqlError = mysqli_error($connection);
	
	mysqli_close($connection);
	
	if ($sqlError != "") {
		
		//email/user not found in the database
		$rtn = json_encode(array(
			"rtn"  => "error",  
			"rtnmsg" => "error creating new notice in the database: " . $sqlError,
			"sql" => $sql,
			"email" => $email,
			"id" => ""
			));
			   
		return $rtn;	
		die();		
		
	}

	//return the result
	if ( $iRows > 0 ) {
		//return success passing along the notice id
		$rtn = json_encode(array(
			"rtn"  => "ok",
			"rtnmsg" => "notice written successfully",
			"email" => $email,
			"id" => $insertedID
			));
			
		return $rtn;	
		die();			
			
	} else {
		//email/user not found in the database
		$rtn = json_encode(array(
			"rtn"  => "fail",  
			"rtnmsg" => "there is no matching user for this notice in the database",
			"email" => $email,
			"id" => ""
			));
			   
		return $rtn;	
		die();
	}
	
}



// -----------------------------------------
// INSERT ELEMENT - ADDS AN ELEMENT TO A NOTICE
// ------------------------------------------

// ELEMENTS 
// Each line of a notice is an element 
//   id - A unique identifier for the element
//   noticeid - The id of the notice this element belongs to
//   order - The order the elements should follow in numerical order
//   timestamp - A timestamp of the last change

function insertElement ($iNoticeID, $iOrder = "") {

 	//Get the credentials
	include "cred.inc";
	 
	//Connect to the database
	include "conn.inc";
	
	//test to see if the notice exists, and get the latest order id
	$sql = "SELECT * FROM `notices` WHERE id = '" . $iNoticeID . "';";
	
	//send the query
	$result = $connection->query($sql);	
	
	//get the number of affected rows
	$iRows = mysqli_affected_rows($connection);

	//return any SQL error
	$sqlError = mysqli_error($connection);
	if ($sqlError != "") {
		
		//return the SQL error
		$rtn = json_encode(array(
			"rtn"  => "error",  
			"rtnmsg" => "error accessing elements table - sql error: " . $sqlError
			));
			   
		return $rtn;	
		die();		
	}
		
	if ($iRows > 0) {
		
		//found a matching notice id

		if ($iOrder == "") {
			$sql = "INSERT INTO `elements` (`noticeid`) VALUES (" . $iNoticeID . ");";			
		} else {
			$sql = "INSERT INTO `elements` (`noticeid`,`order`) VALUES (" . $iNoticeID . "," . $iOrder  . ");";			
		}
			
		//send the query
		$result = $connection->query($sql);

		//get the number of affected rows
		$iRows = mysqli_affected_rows($connection);
		
		//get the inserted id
		$insertedID = mysqli_insert_id($connection); 

		//get the SQL error
		$sqlError = mysqli_error($connection);
		
		//get the number of affected rows
		$iRows = mysqli_affected_rows($connection);

		//return any SQL error
		$sqlError = mysqli_error($connection);

		if ($sqlError != "") {
			
			//return the SQL error
			$rtn = json_encode(array(
				"rtn"  => "error",  
				"rtnmsg" => "error inserting into elements table - sql error: " . $sqlError,
				"sql" => $sql
				));
				   
			return $rtn;	
			die();		
		}
		
		if ($iRows > 0) {
			$rtn = json_encode(array(
				"rtn"  => "ok",  
				"rtnmsg" => "element added successfully",
				"id" => $insertedID
				));			
		} else {
			$rtn = json_encode(array(
				"rtn"  => "fail",  
				"rtnmsg" => "unable to add the new element",
				"sql" => $sql
				));			
		}
		
		mysqli_close($connection);	
		

		
	} else {
		
		//no notice id found in the database
		$rtn = json_encode(array(
			"rtn"  => "fail",  
			"rtnmsg" => "no notice is associated with this notice id: " . $iNoticeID
			));
			   
	}
	
	return $rtn;		
	

}
 
 
 

 
// -----------------------------------------
// INSERT MEMBER - ADDS A MEMBER TO AN ELEMENT
// ------------------------------------------ 

// MEMBERS  
// Each element of a notice may be made up of one or more members 

//  id - A unique identifier
//  elementid - The id of the element this member belongs to
//  text - The optional text of the member
//  format - Any optional formatting of the member 
//  link - The optional target of a link from this member; either a URL or a link to a detail
//  type - The type of member. Possible types include "fact", "info","button","text", or "frame"  
//  timestamp - A timestamp of the last change


function insertMember($iElementID, $strText, $strFormat, $strLink, $strType) {
	
	//echo "in insertMember <br/>";
	
	//Get the credentials
	include "cred.inc";
	
	//Read into the database
	include "conn.inc";
				
	//build SQL statement
	$sql = "INSERT INTO `members` (`elementid`,`text`,`format`,`link`,`type`) VALUES (" . $iElementID . ",'" . addslashes($strText) . "','" . $strFormat . "','" . $strLink . "','" . $strType . "');";

	//send the query
	$result = $connection->query($sql);

	//get the number of affected rows
	$iRows = mysqli_affected_rows($connection);
	
	//get the inserted id
	$insertedID = mysqli_insert_id($connection); 

	//get the SQL error
	$sqlError = mysqli_error($connection);
	
	//get the number of affected rows
	$iRows = mysqli_affected_rows($connection);

	//return any SQL error
	$sqlError = mysqli_error($connection);

	if ($sqlError != "") {
		
		//return the SQL error
		$rtn = json_encode(array(
			"rtn"  => "error",  
			"rtnmsg" => "error inserting into members table - sql error: " . $sqlError,
			"sql" => $sql
			));
			   
		return $rtn;	
		die();		
	}
	 
	//if we have a result 
	if ($result == "1") {

		//we have a result, indicating that we have successfully added a member
		$rtn = json_encode(array(
			"rtn"  => "ok",  
			"rtnmsg" => "member added successfully",
			"id" => $insertedID
			));	

	} else {
		
		//fail
		$rtn = json_encode(array(
			"rtn"  => "fail",  
			"rtnmsg" => "There was a problem adding this member",
			"id" => ""
			));
			   
	}
	
	return $rtn;
 
}





// -----------------------------------------
// INSERT DETAIL - ADDS A DETAIL LINK
// ------------------------------------------

// DETAILS 
// Details are descriptions of the second layer of information available to the PFIN, made up of one or more factors 

//  id - A unique identifier
//  eidlink - The linking code provided by the spreadsheet (this may go away later)
//  noticeid - The notice this particular detail belongs to
//  head - The text at the top of the detail layer, above all the factors
//  foot - The text at the bottom of the detail layer, below all the factors
//  linktext - The link below the footer to an URL 
//  linkurl - The url of the text to link to
//  timestamp - A timestamp of the last change

function insertDetail($strEIDLink, $iNoticeID, $strHead, $strFoot, $strLinkText, $strLinkURL ) {
	 

	//Get the credentials
	include "cred.inc";
	
	//Read into the database
	include "conn.inc";
				
	//build SQL statement
	$sql = "INSERT INTO `details` (`eidlink`,`noticeid`,`head`,`foot`,`linktext`,`linkurl`) VALUES ('" . $strEIDLink . "'," . $iNoticeID . ",'" . addslashes($strHead) . "','" . addslashes($strFoot) . "','" . addslashes($strLinkText) . "','" . addslashes($strLinkURL) . "');";

	//send the query
	$result = $connection->query($sql);

	//get the number of affected rows
	$iRows = mysqli_affected_rows($connection);
	
	//get the inserted id
	$insertedID = mysqli_insert_id($connection); 

	//get the SQL error
	$sqlError = mysqli_error($connection);
	
	//get the number of affected rows
	$iRows = mysqli_affected_rows($connection);

	//return any SQL error
	$sqlError = mysqli_error($connection);

	if ($sqlError != "") {
		
		//return the SQL error
		$rtn = json_encode(array(
			"rtn"  => "error",  
			"rtnmsg" => "error inserting into details table - sql error: " . $sqlError,
			"sql" => $sql
			));
			   
		return $rtn;	
		die();		
	}

	//if we have a result 
	if ($result == "1") {

		//we have a result, indicating that we have successfully added a detail
		$rtn = json_encode(array(
			"rtn"  => "ok",  
			"rtnmsg" => "detail layer added successfully",
			"id" => $insertedID,
			"linkcode" => $strEIDLink
			));	

	} else {
		
		//fail
		$rtn = json_encode(array(
			"rtn"  => "fail",  
			"rtnmsg" => "failure adding the detail level to the database",
			"id" => "",
			"linkcode" => $strEIDLink
			));
			   
	}
	
	return $rtn;

}



// -----------------------------------------
// INSERT FACTOR - ADDS A FACTOR TO A DETAIL PAGE
// ------------------------------------------

// FACTORS
// Factors are entries in a particular detail layer 

//  id - A unique identifier
//  detailid - The id of the detail this factor belongs to
//  text - The text of the prompt for the factor
//  link  - An optional link to an URL or another detail layer
//  info - If there is no link, instead this information blows out about this factor; possibly pretty large apparently
//  order - The order the factors should appear in
//  timestamp - A timestamp of the last change

function insertFactor ($iDetailID, $strText, $strLink, $strInfo, $iOrder ) {
	//Get the credentials
	include "cred.inc";
	
	//Read into the database
	include "conn.inc";

	//data validation
	if ($iDetailID == "") {
		//fail
		$rtn = json_encode(array(
			"rtn"  => "fail",  
			"rtnmsg" => "no detail id to associate the factor with",
			"id" => ""
			));		

		return $rtn;
	}

	//!!! possibly more data validation needed
	  // - what if another factor with the same order is added?
	  // - what if a link is passed with no target detail?


	//build the SQL
	if ($iOrder !== "") {
		$sql = "INSERT INTO `factors` (`detailid`,`text`,`link`,`info`,`order`) VALUES (" . $iDetailID . ",'" . addslashes($strText) . "','" . $strLink . "','" . addslashes($strInfo) . "'," .  $iOrder . ");";
	} else {
		$sql = "INSERT INTO `factors` (`detailid`,`text`,`link`,`info`) VALUES (" . $iDetailID . ",'" . addslashes($strText) . "','" . $strLink . "','" . addslashes($strInfo) . "');";		
	}
				

	//send the query
	$result = $connection->query($sql);

	//get the number of affected rows
	$iRows = mysqli_affected_rows($connection);
	
	//get the inserted id
	$insertedID = mysqli_insert_id($connection); 

	//get the SQL error
	$sqlError = mysqli_error($connection);
	
	//get the number of affected rows
	$iRows = mysqli_affected_rows($connection);

	//return any SQL error
	$sqlError = mysqli_error($connection);

	if ($sqlError != "") {
		
		//return the SQL error
		$rtn = json_encode(array(
			"rtn"  => "error",  
			"rtnmsg" => "error inserting into factors table - sql error: " . $sqlError,
			"sql" => $sql
			));
			   
		return $rtn;	
		die();		
	}

	//if we have a result 
	if ($result == "1") {

		//we have a result, indicating that we have successfully added a factor
		$rtn = json_encode(array(
			"rtn"  => "ok",  
			"rtnmsg" => "factor added successfully",
			"id" => $insertedID
			));	

	} else {
		
		//fail
		$rtn = json_encode(array(
			"rtn"  => "fail",  
			"rtnmsg" => "error adding the factor to the database",
			"id" => ""
			));
			   
	}

	return $rtn;

}




?>
