<?php 


// ******************************************************************
// ****** FUNCTION CALLS ********************************************
// ******************************************************************


// --------------------------------------------------------------------
// SET SUBSCRIPTION - UNSUBSCRIBE FROM COMMUNICATIONS (OR RE-SUBSCRIBE)
// --------------------------------------------------------------------
function updateSubscription ($userid, $boolSubscribe = false) {

	//validate the user id
	if (uuid_validate($userid) == false) {
		
		//invalid GUID
		$rtn = json_encode(array(
			"rtn"  => "fail",
			"rtnmsg" => "invalid user id",
			"userid" => $userid
			));
			   
		return $rtn;	
		die();		
	} else {	
	
		//good GUID, test to see if there is a user with this userid
		

		//Get the MySQL/ConsentCheq credentials
		include("cred.inc");

		//MySQL include library
		include("conn.inc");

		//test to see if the user is already in the database
		$sql = "SELECT * FROM `users` WHERE id = '" . $userid . "';";
		
		//send the query
		$result = $connection->query($sql);	
		
		//get the number of affected rows
		$iRows = mysqli_affected_rows($connection);

		//get any SQL error
		$sqlError = mysqli_error($connection);
		
		if ($sqlError != "") {
			
			//email/user not found in the database
			$rtn = json_encode(array(
				"rtn"  => "error",  
				"rtnmsg" => "error unsubscribing/subscribing user: " . $sqlError,
				"userid" => $userid
				));
				   
			return $rtn;	
		
			
		}	
			
		//if there is already a user, then update them
		if ($iRows > 0) {	

			//add the email address to the user row
			$sql = "UPDATE `users` SET `subscribed` = '" . $boolSubscribe . "' WHERE `id` = '" . $userid .  "';"; 

			//send the query
			$result = $connection->query($sql);
			
			//get the number of affected rows
			$iRows = mysqli_affected_rows($connection);

			//get any SQL error
			$sqlError = mysqli_error($connection);

			if ($sqlError != "") {
				
				//email/user not found in the database
				$rtn = json_encode(array(
					"rtn"  => "error",  
					"rtnmsg" => "error writing to database while unsubscribing/subscribing user: " . $sqlError,
					"userid" => $userid
					));
					   
				return $rtn;	
	
				
			}		
	
		} else {
			$rtn = json_encode(array(
				"rtn"  => "fail",
				"rtnmsg" => "no user with this id in the database",
				"userid" => $userid
				));
				   
			return $rtn;	
			
		}		
		
		$rtn = json_encode(array(
			"rtn"  => "ok",
			"rtnmsg" => "user unsubscribed successfully",
			"id" => $userid
			));	

		if ($boolSubscribe) {
			$rtn = json_encode(array(
				"rtn"  => "ok",
				"rtnmsg" => "user subscribed successfully",
				"id" => $userid
				));				
		}
			
		return $rtn;	
				
	}


}

// ----------------------------------------------
// IDENTIFY USER - ADD AN EMAIL ADDRESS TO A USER
// ----------------------------------------------
function identifyUser ($email, $userid) {

	//add a user level 
	$userLevel = 0; 

	//validate the user email 
	if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
		$rtn = json_encode(array(
			"rtn"  => "fail",
			"rtnmsg" => "invalid email address",
			"email" => $email,
			"id" => ""
			));
			   
		return $rtn;	
		die();
	}		
	
	//validate the user id
	if (uuid_validate($userid) == false) {
		//invalid GUID
		$rtn = json_encode(array(
			"rtn"  => "fail",
			"rtnmsg" => "invalid user id",
			"id" => $userid
			));
			   
		return $rtn;	
		die();		
	}

	//Get the MySQL/ConsentCheq credentials
	include("cred.inc");

	//MySQL include library
	include("conn.inc");

	//test to see if the user is already in the database
	$sql = "SELECT * FROM `users` WHERE email = '" . addslashes($email) . "';";
	
	//send the query
	$result = $connection->query($sql);	
	
	//get the number of affected rows
	$iRows = mysqli_affected_rows($connection);

	//get any SQL error
	$sqlError = mysqli_error($connection);
		
	//if there is already a user, and there is a result return it
	if ($iRows > 0) {	
	
		$row = mysqli_fetch_assoc($result);
	
		$rtn = json_encode(array(
			"rtn"  => "fail",
			"rtnmsg" => "this email already exists in the database with a different user id",
			"email" => $row['email'],
			"id" => $row['id']
			)); 
			
		return $rtn;
		die();
	
	}

	//add the email address to the user row
	$sql = "UPDATE `users` SET `email` = '" . addslashes($email) . "' WHERE `id` = '" . $userid .  "' AND `email` = '" . $userid .  "';"; 

	//send the query
	$result = $connection->query($sql);
	
	//get the number of affected rows
	$iRows = mysqli_affected_rows($connection);

	//get any SQL error
	$sqlError = mysqli_error($connection);

	if ($sqlError != "") {
		
		//email/user not found in the database
		$rtn = json_encode(array(
			"rtn"  => "error",  
			"rtnmsg" => "error creating new user in the database: " . $sqlError,
			"email" => $email,
			"id" => ""
			));
			   
		return $rtn;	
		die();		
		
	}
	
	//define the return object
	$rtn = null;
		
	//return the result
	if ($result) {

		$rtn = json_encode(array(
			"rtn"  => "ok",
			"rtnmsg" => "user identified successfully",
			"email" => $email,
			"id" => $userid
			));   
	} else {

		$rtn = json_encode(array(
			"rtn"  => "error",
			"rtnmsg" => "unable to successfully execute the identifyuser CMD",
			"email" => $email,
			"id" => $userid,
			"SQL" => $sql,
			"result" => $result,
			"rows" => $iRows
			));	
	}

	mysqli_close($connection);	
	
	return $rtn;
	
}




// ------------------------------------------
// CREATE USER - CREATE A NEW USER FROM AN EMAIL ADDRESS
//    * ALTERNATELY - pass nothing and create a placeholder with just a userid
// ------------------------------------------
function createUser($email = null,$secret = null,$userLevel = null) {

	//get the password, or set it to the word "password"
	if ($secret == "" || $secret == null) {
		$secret = "********";  //if the user hasn't been validated, just set the password to eight asterisks
	} else {
		//validate the password
		if (strlen($secret) < 8) {
		
			$rtn = json_encode(array(
				"rtn"  => "fail",
				"rtnmsg" => "password must be at least 8 characters long"
				));
				   
			return $rtn;	
			die();
		
		}
	}

	//add a user level if there isn't already one
	if ($userLevel == null) { $userLevel = 0; }

	//create a new id 
	$id = createGUID(); 

	//validate the user email if it isn't null
	if ($email == "" || $email == null) {
		$email = $id;
	} else {
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			$rtn = json_encode(array(
				"rtn"  => "fail",
				"rtnmsg" => "invalid email address",
				"email" => $email,
				"id" => ""
				));
				   
			return $rtn;	
			die();
		}		
	}

	


	//Get the MySQL/ConsentCheq credentials
	include("cred.inc");

	//MySQL include library
	include("conn.inc");

	//hash the password
	$hashedPassword = password_hash($secret, PASSWORD_DEFAULT);

	//if we're creating a user without an email, no need to test if the user already exists
	if ($email != "") {
		
		//test to see if the user is already in the database
		$sql = "SELECT * FROM `users` WHERE email = '" . addslashes($email) . "';";
		
		//send the query
		$result = $connection->query($sql);	
		
		//get the number of affected rows
		$iRows = mysqli_affected_rows($connection);

		//get any SQL error
		$sqlError = mysqli_error($connection);
			
		//if there is already a user, and there is a result return it
		if ($iRows > 0) {	
		
			$row = mysqli_fetch_assoc($result);
		
			$rtn = json_encode(array(
				"rtn"  => "ok",
				"rtnmsg" => "user already exists",
				"email" => $row['email'],
				"id" => $row['id']
				)); 
				
			return $rtn;
			die();
		
		}
	
	}
	
	//insert the new user into the table, updating the user level and password if the email conflicts
	$sql = "INSERT INTO `users` (`id`,`email`, `secret`,`level`) VALUES ('" . $id .  "','" . addslashes($email) . "','" . addslashes($hashedPassword) . "',". $userLevel . ") ON DUPLICATE KEY UPDATE level = " . $userLevel . ", secret = '" . addslashes($hashedPassword) . "';";

	//send the query
	$result = $connection->query($sql);
	
	//get the number of affected rows
	$iRows = mysqli_affected_rows($connection);

	//get any SQL error
	$sqlError = mysqli_error($connection);

	if ($sqlError != "") {
		
		//email/user not found in the database
		$rtn = json_encode(array(
			"rtn"  => "error",  
			"rtnmsg" => "error creating new user in the database: " . $sqlError,
			"email" => $email,
			"id" => ""
			));
			   
		return $rtn;	
		die();		
		
	}
	
	//define the return object
	$rtn = null;
		
	//return the result
	if ($result == "1" && $iRows > 0) {

		$rtn = json_encode(array(
			"rtn"  => "ok",
			"rtnmsg" => "new user created successfully",
			"email" => $email,
			"id" => $id
			));   
	} else {

		$rtn = json_encode(array(
			"rtn"  => "error",
			"rtnmsg" => "unable to successfully execute the new user CMD",
			"email" => $email,
			"id" => $id,
			"SQL" => $sql
			));	
	}

	mysqli_close($connection);	
	
	return $rtn;
	
}


// ------------------------------------------------------------------------
//  UPDATES THE ACCESS LEVEL FOR A USER IDENTIFIED BY AN ID
// ------------------------------------------------------------------------
function updateUserLevel($userid,$userLevel) {
	
		if (!($userLevel == "0" || $userLevel == "1" || $userLevel == "2" || $userLevel == "3")) {
			
			//invalid user level
			$rtn = json_encode(array(
			"rtn"  => "error",
			"rtnmsg" => "invalid user level"
			));	
			return $rtn;	
			die();
			
		} else {
			//update the user level
			
			//Get the credentials
			include "cred.inc";

			//Read into the database
			include "conn.inc";
						
			//build SQL statement
			$sql = "UPDATE users SET level = '" . $userLevel . "', subscriptiondate = NOW() WHERE id = '" . $userid . "';"; 

			//send the query
			$result = $connection->query($sql);

			//get the number of affected rows
			$iRows = mysqli_affected_rows($connection);

			//get the SQL error
			$sqlError = mysqli_error($connection);

			if ($sqlError != "") {
				
				//return the SQL error
				$rtn = json_encode(array(
					"rtn"  => "error",  
					"rtnmsg" => "error altering user level - sql error: " . $sqlError,
					"sql" => $sql
					));
					   
				return $rtn;	
				die();		
			}

			//if we have a result 
			if ($result == "1") {

				//we have a result
				$rtn = json_encode(array(
					"rtn"  => "ok",  
					"rtnmsg" => "user level updated"
					));	
					
				$_SESSION['level'] = "1";	

			} else {
				
				//fail
				$rtn = json_encode(array(
					"rtn"  => "fail",  
					"rtnmsg" => "failure updating the user level"
					));
					   
			}

			return $rtn;
		}
	
}



		
?>