<?php




// ------------------------------------------------------------------------
//  SAVE ELEMENTS - TAKES AN ELEMENTS OBJECT AND SAVES THEM AND ALL MEMBERS
// ------------------------------------------------------------------------
function saveElements($iNoticeID, $arrElements) {
	
	//turn the string back into an array
	$arrElements = json_decode($arrElements);
	
	foreach($arrElements as $objElement) {
		
		$iOrder = $objElement -> order;
		//$iElementID = $objElement -> elementid;  //!!!??? do we throw this away when we go to the database? I guess so
		$arrMembers = $objElement -> members;
		
		$objReturn = json_decode(insertElement($iNoticeID, $iOrder));
		
		if ($objReturn -> rtn != "ok") {
			//error or failure, pass the error along
			return json_encode($objReturn);
		}
		
		//get the element id returned from the database
		$iElementID = $objReturn -> id;
		
		foreach ($arrMembers as $objMember) {
			
			$objReturn = json_decode(insertMember($iElementID, $objMember -> text, $objMember -> format, $objMember -> link, $objMember -> type));
			
			if ($objReturn -> rtn != "ok") {
				//error or failure, pass the error along
				return json_encode($objReturn);
			}
			
		}

	}	
	
	//return a success
	$rtn = json_encode(array(
		"rtn"  => "ok",  
		"rtnmsg" => "all elements and members saved to database"
		));	

	return $rtn;
	
}



// ------------------------------------------------------------------------
//  SAVE DETAILS - TAKES A DETAILS OBJECT AND SAVES THEM AND ALL FACTORS
// ------------------------------------------------------------------------
function saveDetails($iNoticeID, $arrDetails) {


	//data validation

	//make sure we have a notice id
	if($iNoticeID == "") {
		$rtn = json_encode(array(
			"rtn"  => "fail",
			"rtnmsg" => "no notice id passed to saveDetails function"
			));		
	}
	
	//confirm that the JSON is a valid object
	if($arrDetails == "") {
		$rtn = json_encode(array(
			"rtn"  => "fail",
			"rtnmsg" => "no array of details passed to the saveDetails function"
			));		
	}


	//turn the string back into an array
	$arrDetails = json_decode($arrDetails);

	//loop through each member
	foreach($arrDetails as $objDetail) {
		
		//!!!! TODO: test to make sure these elements are there and valid??

		$strLink 			= $objDetail -> eidlink;
		$strHead 			= $objDetail -> head;
		$strFoot 			= $objDetail -> foot;
		$strLinkText 		= $objDetail -> detaillinktext;
		$strLinkURL 		= $objDetail -> detaillinkurl;
		$arrFactors 		= $objDetail -> factors;

		//write the detail to the database
		$objReturn = json_decode(insertDetail($strLink, $iNoticeID, $strHead, $strFoot, $strLinkText, $strLinkURL));

		if ($objReturn -> rtn != "ok") {
			//in case of error or failure, pass the error along
			return json_encode($objReturn);
		}

		//get the detail id returned from the database
		$iDetailID = $objReturn -> id;	

		//loop through the array of factors
		foreach($arrFactors as $objFactor) {

			$strText = $objFactor -> text;
			$strInfo = $objFactor -> info;
			$strFactorLink = $objFactor -> factorlink;
			$iOrder = $objFactor -> order;

			$objReturn = json_decode(insertFactor($iDetailID,$strText,$strFactorLink,$strInfo,$iOrder));


			if ($objReturn -> rtn != "ok") {
				//in case of error or failure, pass the error along
				return json_encode($objReturn);
			}

		}


	}		
	
	//return a success
	$rtn = json_encode(array(
		"rtn"  => "ok",  
		"rtnmsg" => "all details and factors saved to database"
		));	

	return $rtn;
}


// -------------------------------------
//  SAVE JSON AS A NEW NOTICE FOR USER
// --------------------------------------
function saveNoticeFromJSON($userid, $strNoticeJSON) {
	
	//data validation
	
	//confirm that there is a valid user id
	if($userid == "") {
		$rtn = json_encode(array(
			"rtn"  => "fail",
			"rtnmsg" => "no userid passed to saveNoticeFromJSON"
			));		
	}
	
	//confirm that the JSON is a valid object
	$jsonError = json_validate($strNoticeJSON);
	if ($jsonError !== '' ) {
		$rtn = json_encode(array(
			"rtn"  => "error",
			"rtnmsg" => "data object is not a json object: " . $jsonError
			));
			   
		return $rtn;	
	}
		
	//we have good JSON string, convert it to an object
	$noticeJSON = json_decode($strNoticeJSON);
	
	//test to see that data is set in the object
	if (!( isset($noticeJSON -> name) && isset($noticeJSON -> title) && isset($noticeJSON -> icon) && isset($noticeJSON -> intro) && isset($noticeJSON -> elements) &&  isset($noticeJSON -> details))) {
		
		$rtn = json_encode(array(
			"rtn"  => "error",
			"rtnmsg" => "JSON data not set appropriately in saveNoticeFromJSON" 
			));
		return $rtn;
		
	}
	
	//create a new notice
	$objNewNotice = json_decode(createNotice($noticeJSON -> name, $noticeJSON -> title, $noticeJSON -> icon, $noticeJSON -> intro, "", $userid));	
		
	if ($objNewNotice -> rtn != "ok") {
		//in case of error or failure, pass the error along
		return json_encode($objNewNotice);
	}

	//save the elements of the notice
	$objElementsReturn = json_decode(saveElements($objNewNotice -> id, json_encode($noticeJSON -> elements)));	
	
	if ($objElementsReturn -> rtn != "ok") {
		//in case of error or failure, pass the error along
		return json_encode($objElementsReturn);
	}

	//save the details of the notice
	$objDetailsReturn = json_decode(saveDetails($objNewNotice -> id, json_encode($noticeJSON -> details)));	

	if ($objDetailsReturn -> rtn != "ok") {
		//in case of error or failure, pass the error along
		return json_encode($objDetailsReturn);
	}	

	//return a success including the notice id for the new notice
	$rtn = json_encode(array(
		"rtn"  => "ok",  
		"rtnmsg" => "notice saved to database",
		"id" => $objNewNotice -> id,
		"name" => $noticeJSON -> name
		));	

	return $rtn;
}



?>