<?php

// Import PHPMailer classes into the global namespace
// These must be at the top of your script, not inside a function
use PHPMailer\PHPMailer\PHPMailer;
use PHPMailer\PHPMailer\Exception;

// -------------------------------------
//  SEND EMAIL
// --------------------------------------
function sendEmail($email, $subject, $message, $headers, $strPlaceholderData = null, $urlAttachment = null) { 
	
	$responseurl = SERVER_PATH;


	//validate the  email
	if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
		$rtn = json_encode(array(
			"rtn"  => "fail",
			"rtnmsg" => "invalid email address",
			"email" => $email
			));
			   
		return $rtn;	
	}
	
	if ($subject == "") {
		$subject = 'PrivacyUX Live Start TEST MESSAGE';
	}
	

	if ($headers == "") {
		$headers = 'From: noreply@privacycheq.com' . "\r\n" .
			'Reply-To: noreply@privacycheq.com' . "\r\n" .
			'Content-Type: text/html; charset=ISO-8859-1' . "\r\n" .
			'X-Mailer: PHP/' . phpversion();			
	}

	// GET PHPMailer all set up *****************
	$usernameSmtp = "AKIAYGF7RB7I5NXCMQ4O"; 
	$passwordSmtp = "BEeaUUN7K6M1au9uqj2Tud5apLqYYVmHPoYrlOnY1GyJ"; 



	//host and port of the SMTP server
	$host = 'email-smtp.us-east-1.amazonaws.com';
	$port = 587;		
	
	// GET MAIL HTML CONTENT ************************

	if ($message == "") {
		try {
			$filename = "emailTemplates/emailTemplate.html";
			$fileMailHTML = fopen($filename, "r") or die('{"rtn":"fail","rtnmsg":"Unable to open email template file"}');
			$message = fread($fileMailHTML,filesize($filename));
			fclose($fileMailHTML);
			
		} catch (Exception $e) {
			return '{"rtn":"fail","rtnmsg":"Error accessing email template file"}'; 
			die();
		}
	}

	// REPLACE PLACEHOLDERS IN MESSAGE ******************
	if ($strPlaceholderData) {
		
		//loop through looking for placeholders to replace		
		$objPlaceholderData = json_decode($strPlaceholderData);
		foreach($objPlaceholderData as $key => $value) 
		{
			//replace the key with the value in the email HTML
			$message = str_replace("[[" . $key . "]]",$value , $message);
		}
		
	} 

	// SEND EMAIL *************************************
	$to = $email;
	$sender = 'noreply@privacycheq.com';
	$senderName = 'ConsentCheq';

	if (file_exists ('/home/bitnami/vendor/autoload.php') == false) {
		return '{"rtn":"error","rtnmsg":"bitnami email file autoload.php does not exist on this server"}';
		die();
	}
	
	try {
		
		// Point to the location of your Composer autoload.php file.
		require '/home/bitnami/vendor/autoload.php';
		
	} catch (Exception $e) {
			return '{"rtn":"error","rtnmsg":"Exception - email server not available"}';
	}
	

	if ($to != "") {
		
		$bodyHtml = $message;

		$bodyText = ""; 

		$mail = new PHPMailer(true);

		try {
			// Specify the SMTP settings.
			$mail->isSMTP();
			$mail->setFrom($sender, $senderName);
			$mail->Username   = $usernameSmtp;
			$mail->Password   = $passwordSmtp;
			$mail->Host       = $host;
			$mail->Port       = $port;
			if ($urlAttachment) {
				$mail->AddAttachment($urlAttachment);
			}
			
			$mail->SMTPAuth   = true;
			$mail->SMTPSecure = 'tls';

			// Specify the message recipients.
			$mail->addAddress($to);
			// You can also add CC, BCC, and additional To recipients here.

			// Specify the content of the message.
			$mail->isHTML(true);
			$mail->Subject    = $subject;
			$mail->Body       = $bodyHtml;
			$mail->AltBody    = $bodyText;
			$mail->Send();
			return '{"rtn":"ok","rtnmsg":"Email sent successfully"}'; 
		} catch (phpmailerException $e) {
			//Catch errors from PHPMailer.
			return '{"rtn":"error","rtnmsg":"An error occurred sending the email message: ' . $e->errorMessage() . '"}'; 

		} catch (Exception $e) {
			return '{"rtn":"error","rtnmsg":"Exception - email not sent: ' . $mail->ErrorInfo . '"}';//Catch errors from Amazon SES.
		}



	}


}



?>