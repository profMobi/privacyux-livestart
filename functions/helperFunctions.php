<?php


// ----------------------------
//  CONSTANTS IDENTIFIED HERE
// ----------------------------

//define("SERVER_PATH", "http://localhost/PrivacyUX");
//define("SERVER_PATH", "http://model.consentcheq.com/PrivacyUX");
//define("SERVER_PATH", "https://xcheq.com/PrivacyUX");

$strPath = $_SERVER['HTTP_HOST'] . substr($_SERVER['REQUEST_URI'],0,strrpos ($_SERVER['REQUEST_URI'],"/",-1));

if(strpos($strPath, "localhost") !== false){
    define ("SERVER_PATH","http://" . $strPath );
} else{
    define ("SERVER_PATH","https://" . $strPath );
}


// -----------------------------------------
//  GET DATA PASSED BY GET/POST KEY/VAUE PAIR
// ------------------------------------------
function getPassedData($strKeyString) {
	$return = "";
	
	//try with the case passed
	if(isset($_GET[$strKeyString])) {
		return $_GET[$strKeyString];		
	}

	if(isset($_POST[$strKeyString])) {
		return $_POST[$strKeyString];		
	}
	
	//try again with all lower case
	$strKeyString = strtolower($strKeyString);

	if(isset($_GET[$strKeyString])) {
		return $_GET[$strKeyString];		
	}

	if(isset($_POST[$strKeyString])) {
		return $_POST[$strKeyString];		
	}
	
}


// -----------------------------------------
//  ESCAPE DOUBLE AND SINGLE SLASHES 
// ------------------------------------------
function formatQuotes($strString) {
	
	//test to see if there are already escaped characters
	if (!strpos($strString,"\\")) {
		//if not, search for single quotes and then escape
		if (strpos($strString,"'")) {
			$strString = addslashes($strString);
		}		
	}

	return $strString;
}

// -----------------------------------------
// REFORMAT DATA WITH POST/GET ISSUES
// ------------------------------------------
function formatSignsSpaces($strString) {

	//format plus signs and spaces
    $strString = str_replace("+", "%252b", $strString);
    $strString = str_replace(" ", "+", $strString);

  	return $strString;

}

// -----------------------------------------
// RETURN A NEW RANDOM GUID
// ------------------------------------------
function createGUID() {
    if (function_exists('com_create_guid') === true)
    {
        return trim(com_create_guid(), '{}');
    }

    return sprintf('%04X%04X-%04X-%04X-%04X-%04X%04X%04X', mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(16384, 20479), mt_rand(32768, 49151), mt_rand(0, 65535), mt_rand(0, 65535), mt_rand(0, 65535));
}

// ------------------
// TEST GUID IS VALID
// ------------------
function uuid_validate( $uuid ) {
	
	$uuid = strtolower($uuid);
    
    if (!is_string($uuid) || (preg_match('/^[0-9a-f]{8}-[0-9a-f]{4}-4[0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}$/', $uuid) !== 1)) {
        return false;
    }
    return true;
}

// -------------------
// TEST STRING IS JSON
// -------------------
function json_validate($string) {
	
    // decode the JSON data
    $result = json_decode($string);

    // switch and check possible JSON errors
    switch (json_last_error()) {
        case JSON_ERROR_NONE:
            $error = ''; // JSON is valid // No error has occurred
            break;
        case JSON_ERROR_DEPTH:
            $error = 'The maximum stack depth has been exceeded.';
            break;
        case JSON_ERROR_STATE_MISMATCH:
            $error = 'Invalid or malformed JSON.';
            break;
        case JSON_ERROR_CTRL_CHAR:
            $error = 'Control character error, possibly incorrectly encoded.';
            break;
        case JSON_ERROR_SYNTAX:
            $error = 'Syntax error, malformed JSON.';
            break;
        // PHP >= 5.3.3
        case JSON_ERROR_UTF8:
            $error = 'Malformed UTF-8 characters, possibly incorrectly encoded.';
            break;
        // PHP >= 5.5.0
        case JSON_ERROR_RECURSION:
            $error = 'One or more recursive references in the value to be encoded.';
            break;
        // PHP >= 5.5.0
        case JSON_ERROR_INF_OR_NAN:
            $error = 'One or more NAN or INF values in the value to be encoded.';
            break;
        case JSON_ERROR_UNSUPPORTED_TYPE:
            $error = 'A value of a type that cannot be encoded was given.';
            break;
        default:
            $error = 'Unknown JSON error occured.';
            break;
    }

    if ($error !== '') {
        // throw the Exception or exit // or whatever :)
        return ($error);
    }

    // everything is OK
    return '';
}



// ------------------------------------------
// GET EMAIL ADDRESS FROM USER ID 
// ------------------------------------------
function getEmailFromUserId($userid) {

    //validate the GUID?    

    //Get the MySQL/ConsentCheq credentials
    include("cred.inc");

    //MySQL include library
    include("conn.inc");

    //get the user id from the email address
    $sql = "SELECT `email`,`id` FROM `users` WHERE id = '" . $userid . "';";

    //send the query
    $result = $connection->query($sql); 
    
    //get the number of affected rows
    $iRows = mysqli_affected_rows($connection);

    //get any SQL error
    $sqlError = mysqli_error($connection);
    
    if ($sqlError != "") {
        
        //sql error
        $rtn = json_encode(array(
            "rtn"  => "error",  
            "rtnmsg" => "error reading user row from database: " . $sqlError,
            "email" => $email
            ));
               
        return $rtn;    
        die();      
        
    }
        
    //return the answer
    if ($iRows > 0) {   
    
        $row = mysqli_fetch_assoc($result);
    
        $rtn = json_encode(array(
            "rtn"  => "ok",
            "rtnmsg" => "email found",
            "email" => $row['email'],
            "id" => $row['id']
            )); 
            
        return $rtn;
        die();
    
    } else {
        //email/user not found in the database
        $rtn = json_encode(array(
            "rtn"  => "fail",  
            "rtnmsg" => "no matching record in the database",
            "userid" => $userid
            ));
               
        return $rtn;    
        die();  
    }
}


// ------------------------------------------
// GET USER ID FROM EMAIL ADDRESS 
// ------------------------------------------
function getUserIdFromEmail($email) {

    //validate the user email
    if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
        $rtn = json_encode(array(
            "rtn"  => "fail",
            "rtnmsg" => "invalid email address",
            "email" => $email,
            "id" => ""
            ));
               
        return $rtn;    
        die();
    }   

    //Get the MySQL/ConsentCheq credentials
    include("cred.inc");

    //MySQL include library
    include("conn.inc");

    //get the user id from the email address
    $sql = "SELECT `email`,`id` FROM `users` WHERE email = '" . addslashes($email) . "';";

    //send the query
    $result = $connection->query($sql); 
    
    //get the number of affected rows
    $iRows = mysqli_affected_rows($connection);

    //get any SQL error
    $sqlError = mysqli_error($connection);
    
    if ($sqlError != "") {
        
        //sql error
        $rtn = json_encode(array(
            "rtn"  => "error",  
            "rtnmsg" => "error reading user row from database: " . $sqlError,
            "email" => $email
            ));
               
        return $rtn;    
        die();      
        
    }
        
    //return the answer
    if ($iRows > 0) {   
    
        $row = mysqli_fetch_assoc($result);
    
        $rtn = json_encode(array(
            "rtn"  => "ok",
            "rtnmsg" => "userid found",
            "email" => $row['email'],
            "id" => $row['id']
            )); 
            
        return $rtn;
        die();
    
    } else {
        //email/user not found in the database
        $rtn = json_encode(array(
            "rtn"  => "fail",  
            "rtnmsg" => "no matching record in the database",
            "email" => $email
            ));
               
        return $rtn;    
        die();  
    }
}

// ------------------------------------------
// WRITE ANALYTICS MILESTONE TO THE DATABASE
// ------------------------------------------
function writeAnalytics($userid,$ipaddress,$referrer,$agent,$name) {

	//!!! THIS IS DUPLICATED In SHARED.PHP

    //Get the MySQL/ConsentCheq credentials
    include("cred.inc");

    //MySQL include library
    include("conn.inc");
	
	if ($name == "") {
		$name = "PAGE LOAD";
	}
	
	$userhash = password_hash($ipaddress . $referrer, PASSWORD_DEFAULT);

    //insert the analytics record
    $sql = "INSERT INTO `analytics` (`userid`,`userhash`, `referrer`, `name`) VALUES ('" . $userid . "','" . $userhash . "','" . $referrer . "','" . $name . "');";

    //send the query
    $result = $connection->query($sql); 
    
    //get any SQL error
    $sqlError = mysqli_error($connection);
    
	$rtn = "";
    if ($sqlError != "") {
        
        //sql error
        $rtn = json_encode(array(
            "rtn"  => "error",  
            "rtnmsg" => "error inserting analytics entry: " . $sqlError,
            "email" => $email
            ));
               
    
        
    } else {

        $rtn = json_encode(array(
            "rtn"  => "ok",
            "rtnmsg" => "analytics milestone saved"
            )); 
		
	}

	//return the result
	return $rtn;
	die();	
	
}



?>