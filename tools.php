<?php

//This is a page to maintain the functions that DO NOT require a user login to execute

//It is called in separate post/get calls
//USE THESE COMMANDS FOR MySQL ACCESS 
//Use these commands for async calls

//There is no UI to this page - It echoes a JSON string with at least the following elements:
//  - rtn  	 : ok on success, error or fail in other cases
//  - rtnmsg : A plain language explanation

//Get the MySQL/ConsentCheq credentials
include("cred.inc");

//include functions to lighten the load
include ("functions/helperFunctions.php");
include ("functions/emailFunctions.php");
include ("functions/databaseFunctions.php");
include ("functions/savedataFunctions.php");
include ("functions/retrievedataFunctions.php");
include ("functions/qrFunctions.php");
include ("functions/userFunctions.php");

//!!! TODO: go through error messags and list what functions they are called from since they all bubble up??

//Test to confirm that we have a valid login
session_start();

//Set the max timeout at 5 minutes
ini_set('max_execution_time', 300);


// *** COMMANDS *********************

// Get the Command
$command = strtolower(getPassedData("cmd"));	

	//---------------------------
	// TEST "HELLO WORLD" COMMAND
	//---------------------------
	if ($command=="test") {

		$rtn = json_encode(array(
		"rtn"  => "ok",
		"rtnmsg" => "test command successful"
		));
		       
		echo $rtn;	

	}
	
	//---------------------------------------------------------------
	// GET NOTICE JSON FROM UNIQUE USERID
	//---------------------------------------------------------------
	elseif ($command =="getnoticeidfromuserid") {

		//get passed 
		$userid = getPassedData("userid");


		$objReturn = json_decode(getNoticeFromUserId($userid));


		if ($objReturn -> rtn != "ok") {
			//in case of error or failure, pass the error along
			echo json_encode($objReturn);
		} else {

			//success
			$rtn = json_encode(array(
			"rtn"  => "ok",
			"rtnmsg" => "most recent notice id found",
			"userid" => $userid,
			"noticeid" => $objReturn -> id
			
			));
			       
			echo $rtn;			
		}	


	}

	
	//---------------------------------------------------------------
	// INSERT JSON OBJECT TO DATABASE
	//---------------------------------------------------------------
	elseif ($command =="createnoticejson") {
	
		//get passed 
		$userid = getPassedData("userid");
		$email = getPassedData("email");
		$objNotice = getPassedData("noticejson"); 
		
		//data validation
		if ($email == "" && $userid == "") {
			
			$rtn = json_encode(array(
				"rtn"  => "error",
				"rtnmsg" => "no email address or user id passed to identify the owner of this notice"
				));
				   
			echo $rtn;	
			die();			
			
		}
		
		if ($objNotice == "") {
			$rtn = json_encode(array(
				"rtn"  => "error",
				"rtnmsg" => "no json data object passed to create a new notice"
				));
				   
			echo $rtn;	
			die();				
			
		} else {
			//validate JSON
			$jsonError = json_validate($objNotice);
			if ($jsonError !== '' ) {
				$rtn = json_encode(array(
					"rtn"  => "error",
					"rtnmsg" => "data object is not a json object: " . $jsonError
					));
					   
				echo $rtn;	
				die();					
			} 
		}
		
		//get userid from email address if the userid isn't passed
		if ($userid == "") {
			$objUserEmail = json_decode(getUserIdFromEmail($email));
			if ($objUserEmail -> rtn == "ok") {
				$userid = $objUserEmail -> id;			
			} else {
				//return the fail or error condition and terminate
				echo json_encode($objUserEmail);
				die();
			}			
			
		}
		
		$objReturn = json_decode(saveNoticeFromJSON($userid, $objNotice));

		if ($objReturn -> rtn != "ok") {
			//in case of error or failure, pass the error along
			echo json_encode($objReturn);
		} else {

			//success
			$rtn = json_encode(array(
			"rtn"  => "ok",
			"rtnmsg" => "JSON saved to database",
			"userid" => $userid,
			"email" => $email,
			"name" => $objReturn -> name,
			"noticeid" => $objReturn -> id
			
			));
			       
			echo $rtn;			
		}	

	}

		
	
	//---------------------------
	// CREATE NEW USER / UPDATE USER LEVEL
	//---------------------------
	elseif ($command=="newuser") {

		//get passed data
		$email = getPassedData("email");

		//userlevel is stubbed out for this application
		$userLevel = getPassedData("userlevel");
		if ($userLevel == null) { $userLevel = 0; }

		//validate the user email
		if ($email != "") {

			if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
				$rtn = json_encode(array(
					"rtn"  => "fail",
					"rtnmsg" => "invalid email address",
					"email" => $email
					));
				       
				echo $rtn;	
				die();
			}
		}		

		//get the password, or set it to the word "password" if blank
		$secret = getPassedData("secret");
		if ($secret == "") {
			$secret = "password";
		} else {
			if (strlen($secret) < 8) {
				$rtn = json_encode(array(
					"rtn"  => "fail",
					"rtnmsg" => "The password chosen must be at least eight characters" 
					));
					   
				echo $rtn;	
				die();					
			}
		}		

		//call the function to create database entry
		$objReturn = json_decode(createUser($email,$secret,$userLevel));
			
		//pass the return value from the function through
		echo json_encode($objReturn);

	}

	//---------------------------
	// UPDATE USER LEVEL
	//---------------------------
	elseif ($command=="identifyuser") {

		//get passed data
		$email = getPassedData("email");
		$userid = getPassedData("userid");
		
		$rtn = identifyUser ($email, $userid);
		echo $rtn;
	
	}

	//-----------------------------
	// CREATE NOTICE COMMAND
	//-----------------------------
	elseif ($command == "createnotice") {

		//The ID of the new notice created returned by this command
		$iNoticeID = null;

		//get passed data
		$userid = getPassedData("userid");
		$email = getPassedData("email"); //this call requires either the email address or userid but userid is more efficient
		$name = getPassedData("name"); //can be blank
		$title = getPassedData("title"); //if blank, set to "Privacy Facts"
		$icon = getPassedData("icon"); //can be blank
		$intro = getPassedData("intro"); //if blank set to default
		
		if ($title == "") {
			$title = "Privacy Facts";
		}

		if ($intro == "") {
			$intro = "Click <span style=''color:#007bff;''>blue</span> facts for more detail.";
		}

		//data validation 
		if ($userid == "" && $email == "") {
			$rtn = json_encode(array(
				"rtn"  => "fail",
				"rtnmsg" => "This command requires a valid email address or user id"
				));
			       
			echo $rtn;	
			die();			
		}

		//call the function to create the notice
		$objReturn = json_decode(createNotice($name, $title, $icon, $intro, $email, $userid));

		if ($objReturn -> rtn != "ok") {
			//in case of error or failure, pass the error along
			echo json_encode($objReturn);
		} else {

			//success
			$rtn = json_encode(array(
			"rtn"  => "ok",
			"rtnmsg" => "notice created in database",
			"userid" => $userid,
			"email" => $email,
			"noticeid" => $objReturn -> id
			
			));
			       
			echo $rtn;			
		}	


	}
	


	//------------------------
	// INSERT ELEMENT
	//------------------------
	elseif ($command =="addelement") {

		//get passed data
		$noticeid = getPassedData("noticeid");
		$order = getPassedData("order");
		
		if ($order == "") {
			
		}

		//data validation 
		if ($noticeid == "" ) {
			$rtn = json_encode(array(
				"rtn"  => "fail",
				"rtnmsg" => "This command requires a valid notice id"
				));
			       
			echo $rtn;	
			die();			
		}

		//call the function to insert the element
		$objReturn = json_decode(insertElement ($noticeid, $order)); 
		
		//pass the return value from the function through
		echo json_encode($objReturn);	
		



	}

	// ------------------------
	// INSERT MEMBER
	// ------------------------
	elseif ($command == "addmember") {

		//get passed data
		$elementid = getPassedData("elementid");
		$text = getPassedData("text");
		$format = getPassedData("format");   //format left/right center or a HEX COLOR for a button - can be blank
		$link = getPassedData("link");   //link to a URL or a detail layer
		$type = getPassedData("type");  //Possible types include "fact", "info","button","text", or "frame"  
		


		//data validation 
		if ($elementid == "" ) {
			$rtn = json_encode(array(
				"rtn"  => "fail",
				"rtnmsg" => "This command requires a valid element id to associate the member with"
				));
			       
			echo $rtn;	
			die();			
		}

		//data validation
		if ($type == "") {
			$rtn = json_encode(array(
				"rtn"  => "fail",
				"rtnmsg" => "This command requires a valid type: fact, info, button, text, or frame"
				));
			       
			echo $rtn;	
			die();					
		}

		//call the function to insert the member of the element 
		$objReturn = json_decode(insertMember ($elementid, $text, $format, $link, $type)); 
		
		//pass the return value from the function through
		echo json_encode($objReturn);	

	}

	// -----------------------------
	//  INSERT DETAIL LAYER
	// -----------------------------
	elseif ($command == "adddetail") {

		//get passed data
		$linkcode = getPassedData("link");  //the code to link from an element (one to many association)
		$noticeid = getPassedData("noticeid");  //the notice id to associate the detail with
		$head = getPassedData("head");
		$foot = getPassedData("foot");
		$footlinktext = getPassedData("linktext");   
		$linkurl = getPassedData("linkurl");   


		//data validation 
		if ($noticeid == "" ) {
			$rtn = json_encode(array(
				"rtn"  => "fail",
				"rtnmsg" => "This command requires a valid notice id to associate the detail layer with"
				));
			       
			echo $rtn;	
			die();			
		}

		//data validation 
		if ($linkcode == "" ) {
			$rtn = json_encode(array(
				"rtn"  => "fail",
				"rtnmsg" => "this command requires a valid link code to identify the new detail layer with"
				));
			       
			echo $rtn;	
			die();			
		}

		//data validation
		if ($head == "") {
			$rtn = json_encode(array(
				"rtn"  => "fail",
				"rtnmsg" => "This command requires at least some text in the heading"
				));
			       
			echo $rtn;	
			die();					
		}

		//call the function to insert the detail layer into the database
		$objReturn = json_decode(insertDetail ($linkcode, $noticeid, $head,$foot,$linktext, $linkurl)); 
		
		//pass the return value from the function through
		echo json_encode($objReturn);	

	}

	// -----------------------------
	//  INSERT FACTOR INTO DETAIL LAYER
	// ---------------------------------
	elseif ($command == "addfactor") {

		//get passed data
		$detailid = getPassedData("detailid");  //the id of the detail layer to add this factor to
		$text = getPassedData("text");  //the text line of the factor
		$info = getPassedData("info");  //if the factor is clicked, this is the text that is shown
		$link = getPassedData("link");  //the URL or other detail to redirect to if a factor is clicked
		$order = getPassedData("order"); //an integer in numerical order to show the factors in 

		//data validation 
		if ($detailid == "" ) {
			$rtn = json_encode(array(
				"rtn"  => "fail",
				"rtnmsg" => "This command requires a valid detail layer id to associate the factor with"
				));
			       
			echo $rtn;	
			die();			
		}

		//data validation
		if ($text == "") {
			$rtn = json_encode(array(
				"rtn"  => "fail",
				"rtnmsg" => "This command requires at least some text to put into the factor"
				));
			       
			echo $rtn;	
			die();					
		}

		//call the function to insert the detail layer into the database
		$objReturn = json_decode(insertFactor ($detailid, $text, $link, $info, $order )); 
		
		//pass the return value from the function through
		echo json_encode($objReturn);


	}

	//-----------------------------
	// GET NOTICE DATA
	//-----------------------------
	elseif ($command=="getnotice") {

		//get passed data
		$id  = getPassedData("noticeid");
		$userid = getPassedData("userid");

		//see jsonData.php for this function

	}

	//-----------------------------
	// SAVE FILE OR IMAGE
	//-----------------------------
	elseif ($command=="upload") {

		//!!! upload an image and save a record of it in the database?

	}	

	//-----------------------------
	// SEND EMAIL COMMAND - A generic "send mail" command, mostly for a test 
	//-----------------------------
	elseif ($command=="sendemail") {
	
		//get passed data
		$email 			= getPassedData("email");
		$userid 		= getPassedData("userid");
		$filename 		= "emailTemplates/" . getPassedData("emailtemplate");
		$subject		= getPassedData("subject");
		$attachment		= getPassedData("attachment");
		
		if ($attachment == "") { $attachment == null; }

		//validate the  email
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			$rtn = json_encode(array(
				"rtn"  => "fail",
				"rtnmsg" => "invalid email address",
				"email" => $email
				));
			       
			echo $rtn;	
			die();
		}	
		
		//open the email template	
		if ($filename != "") {
			try {
				$fileMailHTML = fopen($filename, "r") or die('{"rtn":"fail","rtnmsg":"Unable to open email template file"}');
				$strTemplateContent = fread($fileMailHTML,filesize($filename));
				fclose($fileMailHTML);
				
			} catch (Exception $e) {
				echo '{"rtn":"fail","rtnmsg":"Error accessing email template file " . $filename}'; 
				die();
			}				
		}
		
		//!!! if they are needed, we should set the placeholders
		$objPlaceholderData = json_encode(array(
			"ccuserid"  => $userid,
			"serverpath" => SERVER_PATH,
			"noticeurl" => "http://model.consentcheq.com/PFIN/privacycheq/"
		));

		//call the function to send an email message
		$objReturn = json_decode(sendEmail($email,$subject,$strTemplateContent,null,$objPlaceholderData,$attachment));
			
		//pass the return value from the function through
		echo json_encode($objReturn);		
		
		

		
	}

	//-----------------------------
	// SEND INVITE EMAIL COMMAND - Sends the email to invite a new user to user PrivacyUX 
	//-----------------------------
	elseif ($command=="sendinviteemail") {
	
		//get passed data
		$email 			= getPassedData("email");
		$userid 		= getPassedData("userid");
		
		//validate the  email
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			$rtn = json_encode(array(
				"rtn"  => "fail",
				"rtnmsg" => "invalid email address",
				"email" => $email
				));
			       
			echo $rtn;	
			die();
		}

		//get a userid from the email if not passed
		if ($userid == "") {

			//get the userid from the database
			$objReturn = json_decode(getUserIdFromEmail($email));

			if ($objReturn -> rtn != "ok") {

				//in case of error or failure, pass the error along
				echo json_encode($objReturn);
				die();

			} else {

				$userid = $objReturn -> id;
			
			}	

		}
	

		//get the URL for the link to onboard the user
		$responseurl = SERVER_PATH . "/" . "onboarding.php?uid=" . $userid;

		//get the URL for the notice itself
		$noticeURL = SERVER_PATH . "/privacyNotice.php?uid=" . $userid;
		
	
	    //Send an email, passing a unique token for a user to reply with to verify that they are valid and link to a user
		
        $subject = 'Thanks for signing up for PrivacyUX LiveStart, Just 2 steps left to deliver a great CCPA privacy experience...';
	
		//open the email template	
		$filename = "emailTemplates/emailWelcomeMessage.html";
		try {
			$fileMailHTML = fopen($filename, "r") or die('{"rtn":"fail","rtnmsg":"Unable to open email template file"}');
			$strTemplateContent = fread($fileMailHTML,filesize($filename));
			fclose($fileMailHTML);
			
		} catch (Exception $e) {
			echo '{"rtn":"fail","rtnmsg":"Error accessing email template file " . $filename}'; 
			die();
		}		


		//create QRCode image URL to point to the PFIN
		$qrFile = generateQR($noticeURL);
		$qrURL = "";
		if ($qrFile != "") {
			$qrURL = SERVER_PATH . "/functions/" . $qrFile;		
		}

	
		//set the placeholders
		$objPlaceholderData = json_encode(array(
			"ccuserid"  => $userid,
			"responseurl" => $responseurl,
			"noticeurl" => $noticeURL,
			"serverpath" => SERVER_PATH,
			"qrcodeurl" => $qrURL
		));

		//call the function to send an email message
		$objReturn = json_decode(sendEmail($email,$subject,$strTemplateContent,null,$objPlaceholderData,"functions/" . $qrFile));
			
		//pass the return value from the function through
		echo json_encode($objReturn);
           
	} 
	

	//-----------------------------
	// SEND VIRAL EMAIL COMMAND - Sends the email for user to share 
	//-----------------------------
	elseif ($command=="sendviralemail") {
	
		//get passed data
		$email 			= getPassedData("email");
		$userid 		= getPassedData("userid");
		
		//validate the  email
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			$rtn = json_encode(array(
				"rtn"  => "fail",
				"rtnmsg" => "invalid email address",
				"email" => $email
				));
			       
			echo $rtn;	
			die();
		}

		//get a userid from the email if not passed
		if ($userid == "") {

			//get the userid from the database
			$objReturn = json_decode(getUserIdFromEmail($email));

			if ($objReturn -> rtn != "ok") {

				//in case of error or failure, pass the error along
				echo json_encode($objReturn);
				die();

			} else {

				$userid = $objReturn -> id;
			
			}	

		}
	

		//get the URL for the link to onboard the user
		$responseurl = SERVER_PATH . "/" . "onboarding.php?uid=" . $userid;

		//get the URL for the notice itself
		$noticeURL = SERVER_PATH . "/privacyNotice.php?uid=" . $userid;
		
	
	    //Send an email, passing a unique token for a user to reply with to verify that they are valid and link to a user
		
        $subject = "Here's the custom CCPA \"nutrition label\" privacy notice you just built.";
	
		//open the email template	
		$filename = "emailTemplates/emailViralHelper.html";
		try {
			$fileMailHTML = fopen($filename, "r") or die('{"rtn":"fail","rtnmsg":"Unable to open email template file"}');
			$strTemplateContent = fread($fileMailHTML,filesize($filename));
			fclose($fileMailHTML);
			
		} catch (Exception $e) {
			echo '{"rtn":"fail","rtnmsg":"Error accessing email template file " . $filename}'; 
			die();
		}		

		//create QRCode image URL to point to the PFIN
		$qrFile = generateQR($noticeURL);
		$qrURL = "";
		if ($qrFile != "") {
			$qrURL = SERVER_PATH . "/functions/" . $qrFile;		
		}
	
		//set the placeholders
		$objPlaceholderData = json_encode(array(
			"ccuserid"  => $userid,
			"responseurl" => $responseurl,
			"noticeurl" => $noticeURL,
			"serverpath" => SERVER_PATH,
			"qrcodeurl" => $qrURL
		));

		//call the function to send an email message
		$objReturn = json_decode(sendEmail($email,$subject,$strTemplateContent,null,$objPlaceholderData,"functions/" . $qrFile));
			
		//pass the return value from the function through
		echo json_encode($objReturn);
           
	} 	

	//-----------------------------
	// SEND POST SIGNUP EMAIL COMMAND - Sends the email confirming payment
	//-----------------------------
	elseif ($command=="sendsignupmail") {
	
		//get passed data
		$email 			= getPassedData("email");
		$userid 		= getPassedData("userid");
		
		//validate the  email
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			$rtn = json_encode(array(
				"rtn"  => "fail",
				"rtnmsg" => "invalid email address",
				"email" => $email
				));
			       
			echo $rtn;	
			die();
		}

		//get a userid from the email if not passed
		if ($userid == "") {

			//get the userid from the database
			$objReturn = json_decode(getUserIdFromEmail($email));

			if ($objReturn -> rtn != "ok") {

				//in case of error or failure, pass the error along
				echo json_encode($objReturn);
				die();

			} else {

				$userid = $objReturn -> id;
			
			}	

		}
	

		//get the URL for the link to onboard the user
		$responseurl = SERVER_PATH . "/" . "onboarding.php?uid=" . $userid;

		//get the URL for the notice itself
		$noticeURL = SERVER_PATH . "/privacyNotice.php?uid=" . $userid;
		
	    //Send an email, passing a unique token for a user to reply with to verify that they are valid and link to a user
		
        $subject = "Thanks for choosing PrivacyUX Livestart";
	
		//open the email template	
		$filename = "emailTemplates/emailPaymentSuccess.html";
		try {
			$fileMailHTML = fopen($filename, "r") or die('{"rtn":"fail","rtnmsg":"Unable to open email template file"}');
			$strTemplateContent = fread($fileMailHTML,filesize($filename));
			fclose($fileMailHTML);
			
		} catch (Exception $e) {
			echo '{"rtn":"fail","rtnmsg":"Error accessing email template file " . $filename}'; 
			die();
		}		

		//get the subscription date
		$now = new DateTime();
		 
		//Get next year's date 
		$nextYearDT = $now->add(new DateInterval('P1Y'));

		//Retrive the date in a MM-DD-YYYY format.
		$nextYear = $nextYearDT->format('m-d-Y');
 	
	
	
		//set the placeholders
		$objPlaceholderData = json_encode(array(
			"ccuserid"  => $userid,
			"responseurl" => $responseurl,
			"noticeurl" => $noticeURL,
			"serverpath" => SERVER_PATH,
			"subscriptionenddate" => $nextYear
		));

		//call the function to send an email message
		$objReturn = json_decode(sendEmail($email,$subject,$strTemplateContent,null,$objPlaceholderData));
			
		//pass the return value from the function through
		echo json_encode($objReturn);
           
	} 	
	
	//--------------------------------
	// Get User ID from existing email address
	// -------------------------------
	elseif ($command=="idfromemail") {
		
		//get passed data
		$email = getPassedData("email");
		
		//format plus signs and spaces
		$email = formatSignsSpaces($email);
		
		//call the function to create database entry
		$objReturn = json_decode(getUserIdFromEmail($email));
			
		//pass the return value from the function through
		echo json_encode($objReturn);		
		
		
	}

	
	//--------------------------------
	// Get email address from User ID
	// -------------------------------
	elseif ($command=="emailfromid") {
		
		//get passed data
		$userid = getPassedData("userid");
		
		//call the function to create database entry
		$objReturn = json_decode(getEmailFromUserId($userid));
			
		//pass the return value from the function through
		echo json_encode($objReturn);		
		
		
	}	
	
	//---------------------------------------------------------------------
	// Write analytics milestone - an alternative to the call in shared.php
	// -------------------------------------------------------------------
	elseif ($command=="analytics") {
		
		//get passed data
		$userid 	= getPassedData("userid");
		$ipaddress 	= getPassedData("ipaddress");
		$referrer	= getPassedData("referrer");
		$agent		= getPassedData("agent");
		$name		= getPassedData("name");
		
		//call the function to create database entry
		$objReturn = json_decode(writeAnalytics($userid,$ipaddress,$referrer,$agent,$name));
			
		//pass the return value from the function through
		echo json_encode($objReturn);		
		
	}

	//---------------------------------------------------------------------
	// Unsubscribe or re-subscribe to the service
	// -------------------------------------------------------------------	
	elseif ($command == "unsubscribe") {
		
		//
		//get passed data
		$userid 	= getPassedData("uid");
		$boolSubscribe 	= false;
		
		if (getPassedData("subscribed") == "true" || getPassedData("subscribed") == "1") {
			$boolSubscribe 	= true;
		}
		
		//call the function to create database entry
		$objReturn = json_decode(updateSubscription($userid,$boolSubscribe));
			
		//pass the return value from the function through
		echo json_encode($objReturn);		
		
	}
	
	//---------------------------------------------------------------------
	// Create a QR code based on data and return a file on the Internet
	// -------------------------------------------------------------------
	elseif ($command=="getqr") {
		
		//get passed data
		$data = getPassedData("data");
		
		//call the function to generate the QR
		$filename = generateQR($data);
		
		if ($filename != "") {
			
			$rtn = json_encode(array(
			"rtn"  => "ok",
			"rtnmsg" => "QR code generated",
			"file" => $filename
			));
			
		} else {

			$rtn = json_encode(array(
			"rtn"  => "fail",
			"rtnmsg" => "Unable to create QR code",
			"file" => ""
			));
		
		}
			
		echo $rtn;	
		
	}	
	
	//-----------------------------
	// ERROR CATCH CONDITION - ELSE
	//-----------------------------
	else {

		//Invalid CMD passed
		$rtn = json_encode(array(
		"rtn"  => "error",
		"rtnmsg" => "please enter valid command"
		));
		       
		echo $rtn;	

	}



?>


