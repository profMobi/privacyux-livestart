<?php
//This is the landing page following a successful subscription
include ("functions/helperFunctions.php");

//Test to confirm that we have a valid login
session_start();
if (!isset($_SESSION['auth'])) {
	$_SESSION['auth'] = "0";
	$_SESSION['userid'] = "";
	$_SESSION['level'] = "";
} else {
	
	$level = "";
}

//set the level of the login	
if (isset($_SESSION['level'])) {
	$level = $_SESSION['level'];	
	$userid = $_SESSION['userid'];
} else {
	$level = "";	
}

//global variables
$pStatusMessage = "";

//get the URL of the PFIN


?><!DOCTYPE html>


<!--
<?php
echo "DEBUG BLOCK"  . PHP_EOL;
echo "AUTHORIZATION:" .  $_SESSION['auth'] . PHP_EOL;
echo "LEVEL:" .  $_SESSION['level'] . PHP_EOL;
echo "USER ID:" .  $_SESSION['userid'] . PHP_EOL;
?>
-->

<?php
include 'shared.php';

?>

<html lang="en">

<head>

<?php
readfile('html/head.html');
readfile('html/style.html');


$now = new DateTime();
 
//Get next year's date 
$nextYearDT = $now->add(new DateInterval('P1Y'));

//Retrive the date in a MM-DD-YYYY format.
$nextYear = $nextYearDT->format('m-d-Y');
 
$userid = $_SESSION['userid']; 

//get the email from the database
$objReturn = json_decode(getEmailFromUserId($userid));

if ($objReturn -> rtn != "ok") {

	//in case of error or failure, pass the error along
	$email = "";

} else {

	$email = $objReturn -> email;
}	

 
?>


</head>
<body onload="sendSignupEmail();">


<?php if ($_SESSION['auth'] == "1") {

readfile('html/navigation.html');

} else { 

readfile ('html/blankNavigation.html');

}  ?>


<!-- Page Content -->
<div class="container">


<br/><br/>

<?php
	if ($_SESSION['auth'] == "1" ) {
		
		$noticeURL = SERVER_PATH . "/privacyNotice.php?uid=" . $_SESSION['userid'];
		//if there IS a valid login, show the subscription success page	
?>

<h1 class="my-4">Thank You!</h1>
<p>
Thanks for choosing PrivacyUX Livestart to comply with CCPA while building customer trust. 
</p><p>
You can continue to use the same URL below to serve your CCPA Notice at Collection.
</p><p>
<a href="<?php echo $noticeURL ?>" target="PFIN"><?php echo $noticeURL ?></a>
</p><p>
Your annual subscription is in force until <?php echo $nextYear; ?>.  There is no automatic renewal on this product, and we will contact you by email about renewal approximately six (6) weeks prior to that date. 
</p><p>
If you have any questions please contact us at<br/>
<a href="mailto:support@privacycheq.com">support@privacycheq.com</a>
</p><p>
Thanks again for choosing PrivacyUX.<br/>
The PrivacyCheq Team
</p>


<?php
	} else {
		//redirect to the login page if there is no login
?>


<div class="row">
	<!-- Left Panel -->
	<div class="col-sm-6 col-md-4">




	</div>

	<!-- Right Panel -->
	<div class="col-sm-6 col-md-4">




	</div>
</div>	
	
<?php	
	}
?>

</div>

<br/><br/>
	
<?php
readfile('html/footer.html');
readfile('html/bootstrapCore.html');
readfile('html/modal.html');
?>	
	
	
</body>
<script>
function sendSignupEmail() {

	//send signup email
	$.post("tools.php",{"cmd":"sendsignupmail","email":"<?php echo $email; ?>"},function(data) {
			
		//!!! TODO: More error checking here as well
		console.log(data);

	});	
	
	
}
</script>
</html>