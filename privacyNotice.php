<!DOCTYPE html>

<html lang="en">

<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>Privacy Facts</title>

	<!-- Bootstrap core CSS -->
	<link href="bootstrap/css/bootstrap.min.css" rel="stylesheet">
	<link href="fonts/helvetica-neue/style.css" rel="stylesheet">
	<link href="stylePrime.css" rel="stylesheet">
	
	<script>
	//reload the page on a back button
	window.addEventListener('popstate',function(data) {
		 location.reload(true);
	});

	</script>
	<link rel="shortcut icon" type="image/x-icon" href="https://xcheq.com/assets/favicon-16x16.png"  />		
</head><?php
include ("functions/retrievedataFunctions.php");
if (isset($_GET["noticeid"])) {

	echo '<body onload="loadData(\'jsonData.php?noticeid=' . $_GET["noticeid"]  . '\');">';

} else if (isset($_GET["uid"])) {
	
	$userid = $_GET["uid"];
	
	$objReturn = json_decode(getNoticeFromUserId($userid));

		if ($objReturn -> rtn != "ok") {
			
			//!!! WHAT IF THIS DOESN'T WORK??!?
			echo json_encode($objReturn);
			die();
			
			
		} else {

			$noticeid = $objReturn -> id;
	
		}	
	
	
	echo '<body onload="loadData(\'jsonData.php?noticeid=' . $noticeid  . '\');">';	
	
	
} else {
	
	echo '<body onload="loadData();">';	
	
}

?>

<!-- Page Content -->
<div class="container">

<br/><br/>
<div class="row">
	<!-- Detail Cell -->
	<div class="col-md-8">
	<div id="detail_container">
	<div id="detail">
	<div id="PFIN_detail_title" class="facts_title">Detailed Privacy Facts</div>
	<div id="dataDetailLoad"></div>
	</div>
	</div>
	<br/>
	</div>
	
	<!-- Facts Cell -->
	<div class="col-md-4">
	<div id="facts_container">
	<div id="facts" style="display:none;">
    </div><!--#facts-->
	</div>
	<br/>
	<div id="buttons_container"></div>
	
	</div><!-- /facts cell -->
	
</div> <!-- /row -->
	
	
</div><!-- END Page Content -->

	
    <!-- Bootstrap core JavaScript -->
    <script src="jquery/jquery.min.js"></script>
    <script src="bootstrap/js/bootstrap.bundle.min.js"></script>
	
	<!-- PFIN Javascript Libraries-->
    <script src="script/utility.js"></script>	
    <script src="script/loadData.js"></script>		







	<!-- Modal Popup -->
	<div class="modal fade" id="divModal" tabindex="-1" role="dialog" aria-labelledby="divModal" aria-hidden="true">
	  <div class="modal-dialog modal-dialog-centered" role="document">
		<div class="modal-content">
		  <div class="modal-header">
			<h5 class="modal-title" id="headModalTitle">What Just Happened?</h5>
			<button type="button" style="width:50px;" class="close" data-dismiss="modal" aria-label="Close">
			  <span aria-hidden="true">&times;</span>
			</button>
		  </div>
		  <div id="divModalBody" class="modal-body"></div>
		  <div class="modal-footer">
			<button type="button" class="btn btn-primary" data-dismiss="modal">Close</button>
			<button type="button" class="btn" data-dismiss="modal" onclick="printWJH();">Print</button>
		  </div>
		</div>
	  </div>
	</div>
	

	<script>
	//modal popup code
	function showModal(strTitle, strBody) {
		if (strTitle) $('#headModalTitle').html(strTitle);
		if (strBody) $('#divModalBody').html(strBody);
		$('#divModal').modal('show');
		
	}
	</script>
	
</body>
</html>