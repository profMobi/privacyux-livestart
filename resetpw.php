<?php
//This page holds the admin page

//Test to confirm that we have a valid login
session_start();
if (isset($_SESSION['auth']) && isset($_SESSION['userid'])) {
	if ($_SESSION['auth'] != "1" ) {
		echo "Please log in to reset the password";	
		exit();
	}	
} else {
	echo "Please confirm the user";	
	exit();	
}

//get the user id
$userid = "";
$userid = $_SESSION['userid'];	

$pass = "";
$email = "";
$level  = "";


//Read users from the database to confirm that this is a real user id

//Get the MySQL/ConsentCheq credentials
include("cred.inc");

//check for valid credentials
$sql = "SELECT * FROM users WHERE id = '" . $userid . "';";

//MySQL 
include("conn.inc");

//send the query
$result = $connection->query($sql);

//if we have at least one matching account	
if ($result->num_rows > 0) {

	// output data 
	while($row = $result->fetch_assoc()) {
		$userid =  $row["id"];
		$email = $row["email"];
		$level = $row["level"];
	}

	mysqli_close($connection);
} else {
	echo "Please use a valid email address";	
	echo $email;
	mysqli_close($connection);
	exit();
	
}

//get the level of the login	
if (isset($_SESSION['level'])) {
	$level = $_SESSION['level'];	
} else {
	$level = "";	
}



			


?><!DOCTYPE html>
<!--
<?php
echo "DEBUG BLOCK:" . $_SESSION['user'] .  PHP_EOL;


?>
-->

<?php
include 'shared.php';
?>

<html lang="en">

<head>

<?php
readfile('html/head.html');
readfile('html/style.html');
?>


</head>
<body>

<?php
readfile('html/navigation.html');
?>


<!-- Page Content -->
<div class="container">

<br/><br/>

<h1 class="my-4">Reset Password</h1>
<p></p>

<div class="row">

	<div class="col-sm-4 col-md-4">
		<!-- First column -->
		

		<form id="npass" class="" action="javascript:doSubmit('npass');" method="post" onsubmit="return(validateForm('npass'));">

			<h4>Your Email Address: <?php
			
			echo $email;
			
			?></h4><br/>
			

			<input type="hidden" id="email" name="email" value="<?php
			
			echo $email;
			
			?>"/>
			
			
			<label for="newPassword">Update Password</label><br/>			
			<input id="newpassword" name="newpassword" type="password" class="form-control" placeholder="New Password" required><br/>
			
			<label for="newpasswordconfirm">Confirm Password</label><br/>			
			<input id="newpasswordconfirm" name="newpasswordconfirm" type="password" class="form-control"  placeholder="Confirm Password" required><br/>

			<button type="submit" class="btn btn-primary">Update Your Password</button>		

		</form>	
		<br/><br/>   
		

		
	</div>
	
	<div class="col-sm-4 col-md-4">
		<!-- Second column -->
		 
		
	</div>	
	
	<div class="col-sm-4 col-md-4">
		<!-- Third column -->
		
		
	</div>	
</div>	
	

</div>
<!-- END CONTENT -->

<br/><br/>


	
<?php
readfile('html/footer.html');
readfile('html/bootstrapCore.html');
readfile('html/modal.html');
?>	
	
</body>

<script>
//validate the tool forms
function validateForm(strForm) {
	
	//identify the command
	var cmd = strForm;

	//pull the form fields into an object
	objData = {}
	objForm = $("#" + strForm).find(':input');
	for (var x=0;x<objForm.length;x++) {
		
		//get the value of inputs
		objData[objForm[x].name] = objForm[x].value;
		
		//get the checked value of checkboxes
		if (objForm[x].className == "form-check-input") {
			objData[objForm[x].name] = objForm[x].checked;			
		}
		
	}	

	//return false if validation fails
	boolReturn = false;
	
	//validate the new user tool
	if (cmd == "nuser") {
		
		if (objData.newUser == "") {
			modalShow("No Username","Please choose a username","OK");
			return false;
		}
		else if (objData.newpassword != objData.newpasswordconfirm) {
			modalShow("Please reconfirm the password","Password Matching","OK");
			return false;			
		}
		boolReturn = true;
	}
	
	//validate the new password tool
	if (cmd == "npass") {

		if (objData.newpassword != objData.newpasswordconfirm) {
			modalShow("Please reconfirm the password","Password Matching","OK");
			return false;			
		}
		else if (objData.newpassword.length < 8) {
			modalShow("Please choose a password at least eight characters long","Password Length","OK");
			return false;			
		}		
		boolReturn = true;

	}
	
	return boolReturn;
	
}

//submit the tool forms to the PHP page
function doSubmit(strForm) {
	console.log("doSubmit");
	
	//identify the command
	var cmd = strForm;
	
	//pull the form fields into an object
	objData = {"cmd":strForm};
	objForm = $("#" + strForm).find(':input');
	for (var x=0;x<objForm.length;x++) {
		
		//get the value of inputs
		if (cmd == "uaccess") {
			//for dialog access, make sure all the checkboxes are lowercased
			objData[objForm[x].name] = objForm[x].value.toLowerCase();
		} else {
			objData[objForm[x].name] = objForm[x].value;			
		}

		
		//get the checked value of checkboxes
		if (objForm[x].className == "form-check-input") {
			objData[objForm[x].name] = objForm[x].checked;			
		}
	}	

	//debug
	console.log(objData);
	
	//post the information to the tools
	$.post("usertools.php",objData,function(data) {
		
		console.log(data);
		

		modalShow("Password Updated","Update Complete","OK",function(){
			//logout after successfully changing the password
			window.location.replace("logout.php");
			
		});			
		
	}); 
	
}



</script>

</html>
