<?php
//This is the main login page and sales portal


//Test to confirm that we have a valid login
session_start();
if (!isset($_SESSION['auth'])) {
	$_SESSION['auth'] 	= "0";		
	$_SESSION['userid'] = "";
	$_SESSION['level'] 	= "";
	
	$userid = $_SESSION['userid'];
	$level = $_SESSION['level'];		
} else {
	
	//get the userid
	if (isset($_SESSION['userid'])) {	
		$userid = $_SESSION['userid'];
	} else {
		$userid = "";	
	}	

	//set the level of the login and get the user id	
	if (isset($_SESSION['level'])) {	
		$level = $_SESSION['level'];
	} else {
		$level = "";	
	}	

}



//global variables
$pStatusMessage = "";

//Get the MySQL/ConsentCheq credentials
include("cred.inc");

//check for existence of credentials
if ( isset($_POST["email"]) && isset($_POST["password"]) ){

	//set fail message
	$pStatusMessage = "Invalid email or password";

	//MySQL 
	include("conn.inc");
	
	//check for valid credentials
	$sql = "SELECT * FROM `users` WHERE `email` = '" . $_POST["email"] . "'";
	
	//send the query
	$result = $connection->query($sql);

		
	//We'll get a number of rows of credentials
	if ($result->num_rows > 0) {
		
		// output data of each row
		$userid = "";
		$pass = "";
		$email = "";

		while($row = $result->fetch_assoc()) {
			$userid =  $row["id"];
			$email = $row["email"];
			$pass = $row["secret"];
			$level = $row["level"];
		}

		
		//test against the hashed password
		if (password_verify($_POST["password"], $pass)) {

			$_SESSION['auth'] = "1";	
			$_SESSION['userid'] = $userid;	
			$_SESSION['level'] = $level;		
			$pStatusMessage  = "";

		} else {
			
			$pStatusMessage  = "Invalid Username or Password";
			
		}		
	} 
	
	mysqli_close($connection);

}





?><!DOCTYPE html>



<!--
<?php
echo "DEBUG BLOCK"  . PHP_EOL;
echo "AUTHORIZATION:" .  $_SESSION['auth'] . PHP_EOL;
echo "LEVEL:" .  $_SESSION['level'] . PHP_EOL;
echo "USER ID:" .  $_SESSION['userid'] . PHP_EOL;
?>
-->

<?php
include 'functions/helperFunctions.php';
include 'shared.php';

//write analytics 
analytics("PAGE LOAD - LOGIN.PHP");
?>

<html lang="en">

<head>

<?php
readfile('html/head.html');
readfile('html/style.html');
?>


</head>
<body onload="doLoad();">


<?php if ($_SESSION['auth'] == "1") {

readfile('html/navigation.html');

} else { 

readfile ('html/blankNavigation.html');

}  ?>




<!-- Page Content -->
<div class="container">


<br/><br/>

<?php
	if ($_SESSION['auth'] == "1" ) {
		//if there IS a valid login, show the dashboard page	
?>

<h1 id="headingMainMessage" class="my-4">Admin Dashboard</h1>

<div class="row">
	<!-- Left Panel -->
	<div id="divLeftPanel" class="col-sm-6 col-md-6">

<?php
	
	//test the level
	if ($level == 1 || $level == "1") { ?>
	
		<p>Thanks for subscribing to PrivacyUX Livestart!</p>
		
		
<?php } else { ?>
	

		<p>Congratulations, your custom CCPA Notice at Collection is complete and ready to use. All that's left is to <a href="subscribe.php">complete your subscription</a> and add the notice to your data ingestion touchpoints<!-- as directed in the implementation guide we'll send with your completed authorization-->.</p>

		
<?php } ?>

		<p>Link to your customized notice at this URL:<br/>
		<a href="<?php echo SERVER_PATH . "/privacyNotice.php?uid=" . $userid; ?>" target="PFIN"><?php echo SERVER_PATH . "/privacyNotice.php?uid=" . $userid; ?></a></p>

		<p>Need to customize the notice? <a href="edit.php">Access the custom editing tool here</a>. </p>
		
		<p>Do you like what you see? Want to learn more? <a href="https://privacycheq.com/?page_id=35621">Contact us through this web form</a> or email us at <a href="mailto:info@privacycheq.com">info@privacycheq.com</a> and we will get in contact with you.</p>
	
	</div>

	<!-- Right Panel -->
	<div id="divRightPanel" class="col-sm-6 col-md-6">

		<!-- cell phone example -->
		<div id="phone" style="position:relative;display:none;">
			<div id="phoneSilhouette" class="phone"></div>
			<div id="phoneButtonLeft0" class="phone"></div>
			<div id="phoneButtonLeft1" class="phone"></div>	
			<div id="phoneButtonLeft2" class="phone"></div>	
			<div id="phoneButtonRight0" class="phone"></div>			
			<div id="phoneContent" class="phone"><iframe id="framedPFIN" src="privacyNotice.php?uid=<?php echo $userid ?>" style="width:100%;height:100%; border:none;"></iframe></div>
		</div>
	
	</div>
</div>	
<!-- end of the row -->





<?php
	} else {
		//show the login page if there is no login
?>

<h1 class="my-4">Login Page</h1>
<p>Use this page to log in and edit your privacy facts interactive notice.</p>
<p id="pStatusMessage" style="color:red;"><?php echo $pStatusMessage; ?></p>

<div class="row">
  <div class="col-sm-6 col-md-4">
        <form class = "form-signin" role="form" action="login.php" method="post"><br/>
            <h4 class = "form-signin-heading">Use the email address you used to register your privacy notice</h4><br/>
            <label for="email">Email Address</label><br/>
			<input id="email" type="text" class="form-control" name="email" placeholder="Your email address" required autofocus></br>
            <label for="password">Password</label><br/>			
            <input type="password" class="form-control" name="password" placeholder="Your password" required><br/>
            <button class="btn btn-lg btn-primary btn-block" type="submit" name="login">Login</button>
         </form>
		 <br/><br/>
	</div>
</div>	
<!-- end of the row -->
	
<?php	
	}
?>



</div>
<!-- end of the container -->

<br/><br/>


	
<?php
readfile('html/footer.html');
readfile('html/bootstrapCore.html');
readfile('html/modal.html');
?>	
	
	
</body>
<script>
function getUrlParameter(name) {
    name = name.replace(/[\[]/, '\\[').replace(/[\]]/, '\\]');
    var regex = new RegExp('[\\?&]' + name + '=([^&#]*)');
    var results = regex.exec(location.search);
    return results === null ? '' : decodeURIComponent(results[1]);
};

//called when this page loads
function doLoad() {
	
	//prefill with email query string
	

<?php
	if ($_SESSION['auth'] == "1" ) {
		//if there IS a valid login, add the code to update the PFIN	
?>	
	try {
		//hide the gradient background in the PFIN and resize it
		document.getElementById("framedPFIN").contentWindow.document.getElementsByTagName("body")[0].style.backgroundImage = "none";
		onResize();		
		
	} catch (e) {
		//the framed PFIN just isn't loaded
	}
<?php } ?>

	try {
		var passedEmail = getUrlParameter('email');

		if (passedEmail) {
			$("#email").val(passedEmail);
		}
		
	} catch(e) {}
	
}


//called when the PFIN resizes
function onResize() {
	
	var phoneWidth = 440;
	
	//console.log(window.innerHeight);
	var scaleRatio = (window.innerHeight * 0.8) / 1000;
	var maxWidth = document.getElementById("divLeftPanel").offsetWidth;	
	
	//do I scale based on height or width?
	if ((phoneWidth * scaleRatio) >= maxWidth) {
		scaleRatio = maxWidth/phoneWidth;
	}	

	document.getElementById("phone").style.transform 	= "scale(" + scaleRatio + ")";
	document.getElementById("phone").style.top 		=  "-" + (window.innerHeight * 0.1) + "px";
	document.getElementById("phone").style.display		="block";	
	
}

//set the window's resize event
window.onresize = onResize;

</script>

</html>