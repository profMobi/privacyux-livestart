//buildValidation.js
//validation script for build.php
// - This library is used to validate the input from the build process


//validate the form
function validateForm(strForm) {
	
	//identify the command
	var cmd = strForm;
	
	//console.log(cmd);

	//pull the form fields into an object
	objData = {}
	objForm = $("#" + strForm).find(':input');
	for (var x=0;x<objForm.length;x++) {

		//get the checked value of checkboxes
		if (objForm[x].className == "form-check-input") {
			objData[objForm[x].name] = objForm[x].checked;			
		}

		//get the selected value from radio buttons 
		else if (objForm[x].className == "form-radio-input") {
			if (objForm[x].checked) {
				objData[objForm[x].name] = objForm[x].value;
			}
				
		} else {
			//DEFAULT - get the value of inputs
			objData[objForm[x].name] = objForm[x].value;
		}
		
	}	

	//console.log(objData);
	
	//return false if validation fails
	boolReturn = false;
	
	//validate the input
	if (cmd == "build0" || cmd=="buildAll") {
		
		if (objData.organization == "") {
			if (cmd == "buildAll") { $("#mobileStepFive").removeClass('show'); $("#mobileStepOne").addClass('show');  }
			modalShow("No Organization","Please enter the name of your organization","OK");
			return false;
		}
		else if (objData.collectdata == "") {
			if (cmd == "buildAll") { $("#mobileStepFive").removeClass('show'); $("#mobileStepOne").addClass('show');  }
			modalShow("No Data Choice","Please indicate whether you collect personal information or not","OK");
			return false;			
		}
		else if (objData.privacypolicyurl == "") {
			if (cmd == "buildAll") { $("#mobileStepFive").removeClass('show'); $("#mobileStepOne").addClass('show');  }
			modalShow("No Privacy Policy URL","Please enter the web address of your company's legal privacy policy.","OK");
			return false;			
		}

		if (objData.privacypolicyurl.slice(7).toLowerCase() != "http://" || objData.privacypolicyurl.slice(8).toLowerCase() != "https://") {
			$('input[name ="privacypolicyurl"]').val("http://" + objData.privacypolicyurl); //just add http to the forms			
		}			
		
		boolReturn = true;
	}

	if (cmd == "build1" || cmd=="buildAll") {
		//validate the organization information 
		if (objData.orgfunction == "") {
			if (cmd == "buildAll") { $("#mobileStepFive").removeClass('show'); $("#mobileStepTwo").addClass('show');  }
			modalShow("No Description","Please describe in under 200 characters what your company does.","OK");
			return false;
		}
		else if (objData.orglocation == "") {
			if (cmd == "buildAll") { $("#mobileStepFive").removeClass('show'); $("#mobileStepTwo").addClass('show');  }
			modalShow("No Organization Location","Please give an address for your organization's headquarters.","OK");
			return false;			
		}
		else if (!emailIsValid(objData.orgemail) && objData.orgemail != "") {
			if (cmd == "buildAll") { $("#mobileStepFive").removeClass('show'); $("#mobileStepTwo").addClass('show');  }
			modalShow("Invalid Email Syntax","Please provide a valid email address.","OK");
			return false;			
		}
		else if (objData.orgurl == "") {
			if (cmd == "buildAll") { $("#mobileStepFive").removeClass('show'); $("#mobileStepTwo").addClass('show');  }
			modalShow("No Website URL","Please provide the address of your organization's corporate website.","OK");
			return false;			
		}
		
		//just add a period to the forms if there isn't any punctuation
		if (objData.orgfunction.slice(-1) != "." && objData.orgfunction.slice(-1) != "!" && objData.orgfunction.slice(-1) != "?") {
			$('input[name ="orgfunction"]').val(objData.orgfunction + "."); 
		}
		
		//just add http to the website if it isn't already there			
		if (objData.orgurl.slice(0,4).toLowerCase() != "http") {
			$('input[name ="orgurl"]').val("http://" + objData.orgurl); 
		}		

		boolReturn = true;
		
	}

	if (cmd == "build2" || cmd=="buildAll") {
		//validate the categories of data collected information

		//there must be at least one true
		boolReturn = false;
		for (var x=0;x<arrCatChoices.length;x++)  {

			if (objData[arrCatChoices[x]] == true || objData[arrCatChoices[x]] == "true") {

				boolReturn = true;
				
				//force it to be the value true not the string
				objData[arrCatChoices[x]] = true;				
			}
			else {
				//force it to be the value false not the string
				objData[arrCatChoices[x]] = false;	
			}
		} 
		


		if (boolReturn == false) {
			if (cmd == "buildAll") { $("#mobileStepFive").removeClass('show'); $("#mobileStepThree").addClass('show');  }
			modalShow("No Categories Chosen","If you were incorrect earlier, and you actually do not collect any personal data, please start over from the beginning and indicate that your company does not collect personal data.","OK");
			return false;
		}
		
	}

	if (cmd == "build3" || cmd=="buildAll") {
		//validate the purposes of data collected information

		//console.log(objData);		
		
		//there must be at least one true
		boolReturn = false;
		for (var x=0;x<arrPurChoices.length;x++)  {
			
			if (objData[arrPurChoices[x]] == true || objData[arrPurChoices[x]] == "true") {

				boolReturn = true;
				
				//force it to be the value true not the string
				objData[arrPurChoices[x]] = true;				
			}
			else {
				//force it to be the value false not the string
				objData[arrPurChoices[x]] = false;	
			}			
		} 

		if (boolReturn == false) {
			if (cmd == "buildAll") { $("#mobileStepFive").removeClass('show'); $("#mobileStepFour").addClass('show');  }
			modalShow("No Purposes Chosen","If you were incorrect earlier, and you actually do not collect any personal data, please start over.","OK");
			return false;
		}
		
	}
	if (cmd == "build4" || cmd=="buildAll") {

		//validate the user's email address
		if (objData.emailcustomer == "") {
			modalShow("No Email Entered","Please provide an email address identify yourself.","OK");
			return false;			
		}
		else if (!emailIsValid(objData.emailcustomer)) {
			modalShow("Invalid Email Syntax","Please provide a valid email address.","OK");
			return false;			
		}


		boolReturn = true;
	}

	return boolReturn;

}
