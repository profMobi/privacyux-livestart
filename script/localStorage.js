/* This JavaScript library holds code used to pass the PFIN data object from step to step and manage it while editing */

//Store to PFINData in local storage as 'PFINData'
function storeNoticeJSON() {
	if (typeof (Storage) !== "undefined") {
	    localStorage.setItem('PFINData', d);
	}
	else {
		//!!! TODO: what happens for browsers that don't support? Write to DB?
	    console.log("This browser does not support web storage");
	}

}

//Retrieve PFINData form local storage and return it to a JavaScript variable
function getNoticeJSON() {
	if (typeof (Storage) !== "undefined") {

	    d = localStorage.getItem('PFINData');

	    console.log(typeof (d));
	}
	else {
	    console.log("This browser does not support web storage");
	}

}