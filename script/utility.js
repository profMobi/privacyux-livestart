/* This utilitly JavaScript library holds various utility functions */

//debugging function
var boolDebug = false;  //turn this off to disable all debugging
var strDebugState = "ALL";   //change this to turn on only certain sections of debugging

function debug(strDebugMessage, strDebugSection) {
	if (boolDebug) {
		if (!strDebugSection) { strDebugSection = "" }
		if (strDebugSection == strDebugState || strDebugState == "ALL") {
			console.log(strDebugMessage);
		}
	}
}


//Javascript test to see if an email address is valid or not
function emailIsValid (email) {
	return /\S+@\S+\.\S+/.test(email)
}
