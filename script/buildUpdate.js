//buildUpdate.js
//Update script for build.php
// - This library is used to update the JSON of the PFIN as it is built in the UI


function doUpdate(strForm) {

	//console.log("in doUpdate");

	//identify the command
	var cmd = strForm;

	//pull the form fields into an object
	objData = {}
	objForm = $("#" + strForm).find(':input');
	for (var x=0;x<objForm.length;x++) {
		
		//get the checked value of checkboxes
		if (objForm[x].className == "form-check-input") {
			objData[objForm[x].name] = objForm[x].checked;			
		}

		//get the selected value from radio buttons 
		else if (objForm[x].className == "form-radio-input") {
			if (objForm[x].checked) {
				objData[objForm[x].name] = objForm[x].value;
			}
				
		} else {
			//DEFAULT - get the value of inputs
			objData[objForm[x].name] = objForm[x].value;
		}	
		
	}
	
	//console.log(cmd);
	//console.log(objData);

	//update the PFIN appropriately
	if (cmd == "build0" || cmd=="buildAll") {

		//Add the general PFIN information

		//put the data in the JSON object for the Company Name & Legal Priacy Policy
		d.elements.forEach(function(elmnt,indx){
			if (elmnt.members[0].text == "Our company name") {
				d.elements[indx].members[1].text = objData.organization;				
			}
			if (elmnt.members[0].text == "Full privacy details") {
				d.elements[indx].members[1].text = "our legal privacy policy";
				d.elements[indx].members[1].link = objData.privacypolicyurl;
			}
		});			
		
		if (objData.collectdata == "true") {
			
			//change the text of the button to redirect to a step rather than the end
			$("#btnBuild1").html("Step 3");
			
			//test if the categories element has already been added
			boolCategoriesAdded = false;
			boolPurposesAdded = false;
			d.elements.forEach(function(elmnt,indx){
				if (elmnt.members[0].text == "Categories") {
					boolCategoriesAdded = true;
				}
				if (elmnt.members[0].text == "Purposes") {
					boolPurposesAdded = true;
				}
			});	


			if (boolCategoriesAdded == false) {
				d.elements.push({
				  "order": "1",
				  "elementid": "CATEGORIES",
				  "questionid": "",
				  "members": [
					{
					  "memberid": "",
					  "text": "Categories",
					  "format": "L",
					  "link": "",
					  "type": "fact"
					},
					{
					  "memberid": "",
					  "text": "of data we collect",
					  "format": "R",
					  "link": "",
					  "type": "info"
					}
				  ]
				});				
			}

			if (boolPurposesAdded == false) {
				d.elements.push(    {
				  "order": "2",
				  "elementid": "PURPOSES",
				  "questionid": "",
				  "members": [
					{
					  "memberid": "",
					  "text": "Purposes",
					  "format": "L",
					  "link": "",
					  "type": "fact"
					},
					{
					  "memberid": "",
					  "text": "for how we use your data",
					  "format": "R",
					  "link": "",
					  "type": "info"
					}
				  ]
				});
			}

		    //re-sort the array by the order
		    d.elements.sort(function(a,b){return a.order - b.order});


		}
	
		if (cmd == "build0") {
			//write the data object to the database
			storeData(userid,function(){
				window.location.href = "build.php?s=1&userid=" + userid;	
			});			
		}


		
	}

	if (cmd == "build1" || cmd=="buildAll") {

		//Add the organization data information

		//grab the organization's name from the data object
		var orgName = d.elements[0].members[1].text;

		//set the hyperlink on
		d.elements[0].members[1].link = "ORG";
		
		//fixed the url of the org
		if (!objData.orgurl) { objData.orgurl = ""};
		
		//test for other ORG details and remove them from the list of details...
		tempDetails = d.details;
		tempDetails.forEach(function(dtl,indx){
			console.log("detail link dtl.eidlink: " + dtl.eidlink);
			console.log("index: " + indx);
			if (dtl.eidlink == "ORG") {
				//delete that detail
				delete tempDetails[indx];
			}
		});	

		//...then replace the details that belong there
		d.details = [];
		tempDetails.forEach(function(dtl,indx){
			d.details.push(tempDetails[indx]);
		});
		
		jsonDetails = {
	      "eidlink": "ORG",
	      "head": "Our company name is " + orgName + ". " + objData.orgfunction,
	      "foot": orgName + " may change the provisions of this privacy notice at any time, and will always post the most up-to-date version on our website. Additional general information about us can be found on our corporate website.",
	      "detaillinktext": "Link to our corporate website",
	      "detaillinkurl": objData.orgurl,
	      "factors": []
    	}


		if (objData.orgemail != "") {
			jsonDetails.factors.push({
				"text":"Notify us by e-mail",
				"info": objData.orgemail,
				"factorlink":"",
				"order":"0"
			});
		}		

		if (objData.orglocation != "") {
			jsonDetails.factors.push({
				"text":"Notify us by post",
				"info": objData.orglocation,
				"factorlink":"",
				"order":"1"
			});
		}	
		
		if (objData.orgphone != "") {
			jsonDetails.factors.push({
				"text":"Notify us by phone",
				"info": objData.orgphone,
				"factorlink":"",
				"order":"2"
			});
		}
	
		//push the details again
		d.details.push(jsonDetails);		

			

		//test to see if personal data is collected - if not, just skip to the end
		if (cmd == "build1") {
			if (d.elements[1].members[0].text == "Categories") {

				//write the data object to local storage
				storeData(userid,function(){
					window.location.href = "build.php?s=2&userid=" + userid;
				});			
							
			} else {

				//write the data object to the database
				storeData(userid,function(){
					window.location.href = "build.php?s=4&userid=" + userid;	
				});	

			}
		}

	}

	if (cmd == "build2" || cmd=="buildAll") {

		//Add the categories of information data

		//add the link to the categories page
		d.elements[1].members[1].link = "CAT";
		
		var arrCategoryFactors = [];
		
		//clean up string "true" because of UI issues
		for (var x=0;x<arrCatChoices.length;x++)  {

			if (objData[arrCatChoices[x]] == true || objData[arrCatChoices[x]] == "true") {
				
				//force it to be the value true not the string
				objData[arrCatChoices[x]] = true;				
			}
			else {
				//force it to be the value false not the string
				objData[arrCatChoices[x]] = false;	
			}
		} 
		
		var iOrder = 0;

		//read the checkboxes and add factors to the data object
		if (objData.catidentifiers) {

			var objFactor = {
			  "text": "Identifiers",
			  "info": "Data that identifies you such as your name, address, or phone number.",
			  "factorlink": "",
			  "order": iOrder
			}
		
			arrCategoryFactors.push(objFactor);
			iOrder = iOrder+1;

		}
		if (objData.catcustomerrecords) {
			var objFactor = {
			  "text": "Information in Customer Records",
			  "info": "Records of your activity recorded by our organization.",
			  "factorlink": "",
			  "order": iOrder
			}
		
			arrCategoryFactors.push(objFactor);
			iOrder = iOrder+1;			
			
		}
		if (objData.catlegallyprotected) {
			var objFactor = {
			  "text": "Legally Protected Characteristics",
			  "info": "Under federal law, protected characteristics include race, color, national origin, religion, gender (including pregnancy), disability, age (if the employee is at least 40 years old), and citizenship status.",
			  "factorlink": "",
			  "order": iOrder
			}
		
			arrCategoryFactors.push(objFactor);
			iOrder = iOrder+1;			
			
		}
		if (objData.catcommercialpurchasing) {
			var objFactor = {
			  "text": "Commercial Purchasing Information",
			  "info": "Information related to doing business bewteen organizations.",
			  "factorlink": "",
			  "order": iOrder
			}
		
			arrCategoryFactors.push(objFactor);
			iOrder = iOrder+1;			
			
		}
		if (objData.catbiometric) {
			var objFactor = {
			  "text": "Biometric Information",
			  "info": "Your body's unique physical characteristics used to identify you such as fingerprints or facial recognition.",
			  "factorlink": "",
			  "order": iOrder
			}
		
			arrCategoryFactors.push(objFactor);
			iOrder = iOrder+1;			
			
		}
		if (objData.catnetworkactivity) {
			var objFactor = {
			  "text": "Internet or Network Activity",
			  "info": "A record of your online activity.",
			  "factorlink": "",
			  "order": iOrder
			}
		
			arrCategoryFactors.push(objFactor);
			iOrder = iOrder+1;			
			
		}
		if (objData.catgeolocation) {
			var objFactor = {
			  "text": "Geolocation",
			  "info": "Data that identifies or may be used to estimate your the real-world geographic location such as a radar source, mobile phone, or Internet-connected computer terminal.",
			  "factorlink": "",
			  "order": iOrder
			}
		
			arrCategoryFactors.push(objFactor);
			iOrder = iOrder+1;			
			
		}
		if (objData.catsenses) {
			var objFactor = {
			  "text": "Information Typically Detected by the Senses",
			  "info": "Any audio, electronic, visual, thermal, olfactory, or similar information that may be collected from you.",
			  "factorlink": "",
			  "order": iOrder
			}
		
			arrCategoryFactors.push(objFactor);
			iOrder = iOrder+1;			
			
		}
		if (objData.catemployment) {
			var objFactor = {
			  "text": "Employment Information",
			  "info": "Your work history or current state of employment.",
			  "factorlink": "",
			  "order": iOrder
			}
		
			arrCategoryFactors.push(objFactor);
			iOrder = iOrder+1;			
			
		}
		if (objData.cateducation) {
			var objFactor = {
			  "text": "Education Information",
			  "info": "Information about where and when you were educated, what you studied, or what marks and degrees were attained.",
			  "factorlink": "",
			  "order": iOrder
			}
		
			arrCategoryFactors.push(objFactor);
			iOrder = iOrder+1;			
			
		}
		if (objData.catinferences) {
			var objFactor = {
			  "text": "Data Inferences Used to Profile",
			  "info": "Any data that may be used in combination with other freely available information that may be used to make an educated guess about other personal information.",
			  "factorlink": "",
			  "order": iOrder
			}
		
			arrCategoryFactors.push(objFactor);
			iOrder = iOrder+1;			
			
		}
	
		var jsonObjectCategories =     {
		  "eidlink": "CAT",
		  "head": "These are the categories of data " + d.elements[0].members[1].text + " collects.",
		  "foot": "",
		  "detaillinktext": "",
		  "detaillinkurl": "",
		  "factors": []
		}
		
		jsonObjectCategories.factors = arrCategoryFactors;
		
		//test for other CAT details and remove them from the list of details...
		tempDetails = d.details;
		tempDetails.forEach(function(dtl,indx){
			console.log("detail link dtl.eidlink: " + dtl.eidlink);
			console.log("index: " + indx);
			if (dtl.eidlink == "CAT") {
				//delete that detail
				delete tempDetails[indx];
			}
		});	

		//...then replace the details that belong there
		d.details = [];
		tempDetails.forEach(function(dtl,indx){
			d.details.push(tempDetails[indx]);
		});		
				
		d.details.push(jsonObjectCategories);
		
		if (cmd == "build2") {
			//write the data object to the database
			storeData(userid,function(){
				window.location.href = "build.php?s=3&userid=" + userid;	
			});
		}
		
	}

	if (cmd == "build3" || cmd=="buildAll") {  
	
		//Add the purposes of information data

		// Information about the CCPA purposes
		//https://medium.com/golden-data/purposes-for-processing-under-ccpa-3e329ddd218

		//add the link to the categories page
		d.elements[2].members[1].link = "PUR";
		
		var arrPurposeFactors = [];
		
		//clean up string "true" because of UI issues
		for (var x=0;x<arrPurChoices.length;x++)  {
			
			if (objData[arrPurChoices[x]] == true || objData[arrPurChoices[x]] == "true") {
				
				//force it to be the value true not the string
				objData[arrPurChoices[x]] = true;				
			}
			else {
				//force it to be the value false not the string
				objData[arrPurChoices[x]] = false;	
			}			
		}  		
		
		var iOrder = 0;
		
		///"purbusiness","purcommercial", "purconsumer"

		//read the checkboxes and add factors to the data object
		if (objData.purbusiness) {

			var objFactor = {
			  "text": "For Business Purposes",
			  "info": "We process your personal data for operational purposes. The use of personal information in this way is necessary and proportionate to achieve the purposes for which the personal information is collected. Examples of business purposes for processing personal data are auditing, security, debugging, short-term transient use, performing services on behalf of a business or service provider, research, and maintaining the quality or safety of a product or service.",
			  "factorlink": "",
			  "order": iOrder
			}
		
			arrPurposeFactors.push(objFactor);
			iOrder = iOrder+1;

		}
		if (objData.purcommercial) {

			var objFactor = {
			  "text": "For Commercial Purposes",
			  "info": "We process your personal data for commercial purposes. For example to advanceyour commercial or economic interests, such as by inducing another person to buy, rent, lease, join, subscribe to, provide, or exchange products, goods, property, information, or services, or enabling or effecting, directly or indirectly, a commercial transaction. ",
			  "factorlink": "",
			  "order": iOrder
			}
		
			arrPurposeFactors.push(objFactor);
			iOrder = iOrder+1;

		}
		if (objData.purconsumer) {

			var objFactor = {
			  "text": "For Fulfilling Consumers' Requests",
			  "info": "If you request that we process your personal data, we will do so with certain limitations. You must act intentionally to give your consent for the processing of your data or on behalf of of a child and such consent for data processing doesn't necessarily extend to downstream processing.",
			  "factorlink": "",
			  "order": iOrder
			}
		
			arrPurposeFactors.push(objFactor);
			iOrder = iOrder+1;

		}

		var jsonObjectPurposes =     {
		  "eidlink": "PUR",
		  "head": "These are the purposes for which " + d.elements[0].members[1].text + " uses your data.",
		  "foot": "",
		  "detaillinktext": "",
		  "detaillinkurl": "",
		  "factors": []
		}
		
		//test for other PUR details and remove them from the list of details...
		tempDetails = d.details;
		tempDetails.forEach(function(dtl,indx){
			console.log("detail link dtl.eidlink: " + dtl.eidlink);
			console.log("index: " + indx);
			if (dtl.eidlink == "PUR") {
				//delete that detail
				delete tempDetails[indx];
			}
		});	

		//...then replace the details that belong there
		d.details = [];
		tempDetails.forEach(function(dtl,indx){
			d.details.push(tempDetails[indx]);
		});			
		
		jsonObjectPurposes.factors = arrPurposeFactors;
		d.details.push(jsonObjectPurposes);

				
		if (cmd == "build3") {
			//write the data object to the database
			storeData(userid,function(){
				window.location.href = "build.php?s=4&userid=" + userid;	
			});
		}
		
		
	}

	if (cmd == "build4" || cmd=="buildAll") {
		
		//start the loader spinning
		showLoader(true);
	
		//Get the email address and send an invitation to create an account
		var objPostData = {};	
		objPostData["email"] = objData.emailcustomer;
		objPostData["userid"] = userid;
		objPostData["cmd"] = "identifyuser";
		
		//console.log("about to identify the user");
		//console.log(objPostData);
	
		//identify the user by linking the email address and the userid
		$.post("tools.php",objPostData,function(data) {

			//turn the string data returned back into a JSON object
			var objReturnData = JSON.parse(data);
			
			//console.log(objReturnData);

			if (objReturnData.rtn != "ok") {
				
				//stop the loader
				showLoader(false);

				//error or failure creating the user
				modalShow("Error creating new account: " + objReturnData.rtnmsg,"Try again later","OK",function(){

					//!!!! WHAT SHOULD WE DO IN CASE OF A FAILURE OR ERROR? do nothing?

				});					


			} else {
					
				//console.log("about to store data");	
				
				//console.log(userid);
					
				//store the notice JSON		
				storeData(userid,function(){

					//still looking good, now send the email

					//create the post data object to send to the tools
					var objPostData = {};	
					objPostData["email"] = objReturnData.email;
					objPostData["userid"] = objReturnData.userid;
					objPostData["cmd"] = "sendinviteemail";
					
					//add the link to the notice
					$("#spnNoticeURL").html('<a href=\'https://www.xcheq.com/PrivacyUX/privacyNotice.php?uid=' + objReturnData.userid + '\' target=\'notice\'>https://www.xcheq.com/PrivacyUX/privacyNotice.php?uid=' + objReturnData.userid + '</a>');

					//trigger the email to the customer
					$.post("tools.php",objPostData,function(data) {		

						//turn the string data returned back into a JSON object
						var objReturnData = JSON.parse(data);
	
						if (objReturnData.rtn != "ok") {
													
							//stop the loader
							showLoader(false);
						
							//error or failure saving the JSON to database
							modalShow("Error sending welcome email: " + objReturnData.rtnmsg,"Try again later","OK",function(){

								//!!!! WHAT SHOULD WE DO IN CASE OF A FAILURE OR ERROR? do nothing?

							});	
						} else {

							//all three calls worked, move on
							window.location.href = "buildComplete.php?s=5&userid=" + userid;

						}

					});	 //end of sendemail call		

			
				});	//end of the storeData response

			} //end of else case of identify user

		}); //end of identify user call

	}

	//**** END OF THE BUILD CALLS HERE *****

}


function storeData(userid,callback) {
	//start the loader spinning
	showLoader(true);

	//create the post data object to send to the tools
	var objPostData = {};	
	objPostData["userid"] 		= userid;  
	objPostData["noticejson"] 	= JSON.stringify(d); 
	objPostData["cmd"] 			= "createnoticejson";
	
	//console.log(objPostData);

	//trigger create the notice by passing the JSON
	$.post("tools.php",objPostData,function(data) {			

		//turn the string data returned back into a JSON object
		var objReturnData = JSON.parse(data);
		
		//console.log(data);


		if (objReturnData.rtn != "ok") {

			//error or failure saving the JSON to database
			modalShow("Error saving JSON to database: " + objReturnData.rtnmsg,"Try again later","OK",function(){
				//!!! What to do here in case of an error?
			});	
		
			
		} else {
			
			//execute the callback
			callback();			
		}
		
	});
	
}

//store the PFIN JSON to local storage
function localStoreData() {
	if (typeof (Storage) !== "undefined") {
	    localStorage.setItem('PFINData', d);
		return true;
	}
	else {
	    console.log("This browser does not support web storage");
	}
	
	return false;
}

//get PFIN JSON data from local storage
function getLocalStoreData() {
	if (typeof (Storage) !== "undefined") {

	    d = localStorage.getItem('PFINData');

	    return true;
	}
	else {
	    console.log("This browser does not support web storage");
	}
	
	return false;
}
