/*
loadData.js

This library loads all the notice json data, and then constructs the notice

*/

var d = new Object();  //main data object

/* This function gets the JSON Object and puts it in a variable that is just the letter d */
function loadData(strDataURL) {

	//console.log("in loadData");

	//getting the data from the parent window (build.php)
	if (!jQuery.isEmptyObject(parent.d)) {

		//get the data from the parent window
		d = parent.d;

		//show the data coming from the parent window
		showData();

	} else { 

	
		//if no filename is passed, just get the default
		if (!strDataURL) {
			strDataURL = "jsonData.php";
		}

		//get the data from an AJAX call
		$.ajax({
			type: "GET",
			dataType:"json",
			url: strDataURL,
			success: function(data) {
				d = data;
				showData();
			},
			error: function(xhr, status, error) {
				
				//why is this an error?
				debug("error loading PFIN data",1);
				debug(error,1);
			}
			
		});

	}







	
}

function showData() {
	var outputHTML = "";
	
	outputHTML += getNoticeTitleBlock();
	outputHTML += drawElements();
	
	$("#facts").html(outputHTML);
	$("#facts").show();
	
	//draw the details invisibly after showing the visible stuff
	$("#dataDetailLoad").html(drawDetails());
	
	try {
		parent.doOnFrameResize();
	} catch(err) {}
	
}

/* This function draws the title, icon, and informational block at the top of the notice */
function getNoticeTitleBlock(data) {
	
	var outputHTML = "";
	
	if (!data) {
		data = d;
	}
	
	if (data.icon != "") {
		outputHTML += '<img id="PFIN_controllericon" src="assets/' + data.icon + '"/>';
	}	
	if (data.title != "") {
		outputHTML += '<div id="PFIN_title" class="facts_title" >' +  data.title + '</div>';
	}
	if (data.intro != "") {
		outputHTML += '<div id="PFIN_intro">' +  data.intro + '</div>';
	}
	        
	return outputHTML;	
} 


/* This function generates the elements in the notice */
function drawElements(data) {

	var outputHTML = "";
	var outputButtonHTML = "";
	
	if (!data) {
		data = d;
	}
	
	var iTrialCloseClasses = -1;
	var iVisibleTrialCloseClasses = -1;
	
	//start the first section 
	outputHTML += '<div class="facts_box_top">';
	/*
	outputHTML += '<div class="fact_row"><span class="fact_entry" style="float:right;">';
	outputHTML += 'Links';
	outputHTML += '</span><span class="fact_entry" style="text-align:left;">';
	outputHTML += 'Information';
	outputHTML += '</span></div><div class="facts_box">';
	*/

	//for each element
	for (var x=0; x<data.elements.length; x++) {
		
		//debug(data.elements[x],3);
		
		//don't show elements that don't have a type
		if (data.elements[x].members[0].type != "") {
			
			outputHTML += '<div class="';
			
			//add a special class for a button or action
			if ((data.elements[x].members[0].type == "action")||(data.elements[x].members[0].type == "button")) {
				outputHTML += ' fact_row_no_bottom_line';
				outputHTML += ' LVL' + iTrialCloseClasses;	//to hide the entire line
			} else if (data.elements[x].members[0].type == "frame"){ 
			
				//Kludgey!!! Danger!!!
			
				//a blank line entry
				outputHTML += ' facts_box';
				
			} else {
				//a regular fact row
				outputHTML += ' fact_row';
			}

			//add classes to hide below a trial close
			if (iTrialCloseClasses >=0) {
				outputHTML += ' TC' + iTrialCloseClasses;							
			}
					
			outputHTML += '">';
			
			//add each member to the element
			for (var z=0;z< data.elements[x].members.length;z++ ) {
				
				var member = data.elements[x].members[z];
			
				//BLANK LINE ENTRY
				if (member.type.toLowerCase() == "frame") {
				
					//ignore these, now dealing with these above
				
				//ACTION ENTRY
				} else if (member.type.toLowerCase() == "action") {

					outputButtonHTML += '<div class="fact_center';
					if (iTrialCloseClasses >=0) {
						outputButtonHTML += ' hidden';
					}
					outputButtonHTML += ' LVL' + iTrialCloseClasses;
					outputButtonHTML += '">' + member.text + "</div>";
					
				
				//BUTTON ENTRY
				} else if (member.type.toLowerCase() == "button") {

					//add the appropriate classes
					outputButtonHTML += '<button class="btn btnEmpty';
					if (iTrialCloseClasses >=0) {
						outputButtonHTML += ' hidden';
					}				
					outputButtonHTML += ' LVL' + iTrialCloseClasses;
					outputButtonHTML += '"';
					
					//if a color is passed in the format field, change the button color
					if (member.format.indexOf("#") != -1) {
						outputButtonHTML += ' style="';
						outputButtonHTML += 'background-color:#fff; border-color:' + member.format + ';';
						outputButtonHTML += 'color:' + member.format + ';'; //black text
						outputButtonHTML += '"'; 
					}
					
					//increment the trial close or link to "What Just Happened"
					if (member.link.indexOf("WJH") != -1) {
						//link to "What Just Happened" queued by the letters "WJH" and a number
						var strWJHLink = member.link.substr(member.link.indexOf("WJH"),4);
						outputButtonHTML += ' onclick="linkToWJH(\'' + strWJHLink + '\');"';
						
					} else if (member.link == "Continue PFIN") {
						iTrialCloseClasses = iTrialCloseClasses + 1;
						iVisibleTrialCloseClasses = iVisibleTrialCloseClasses + 1;
						//$(".TC" + iVisibleTrialCloseClasses).show();
						outputButtonHTML += ' onclick="$(\'.TC' + eval(iVisibleTrialCloseClasses) + '\').show(); $(\'.LVL' + eval(iVisibleTrialCloseClasses) + '\').show(); $(\'.LVL' + eval(iVisibleTrialCloseClasses-1) + '\').hide();"';						
					}

					outputButtonHTML +='>' + member.text + "</button>";
					
				//INFO / PROMPT
				} else if ((member.type.toLowerCase() == "info")||(member.type.toLowerCase() == "fact")) {
					
					//get the fact text to pass along - KLUDGE
					var factText = data.elements[x].members[0];
					
					//reverse the order
					for (var y=data.elements[x].members.length-1;y>=0;y--){
						member = data.elements[x].members[y];
						
						if (member.type.toLowerCase() == "info") {
							outputHTML += '<span class="fact_entry" ';
							/*
							if (member.format.toUpperCase().indexOf("L") != -1) {
								//format left
								outputHTML += 'style="text-align:left;" ';						
							}*/
							if (member.format.toUpperCase().indexOf("R") != -1) {
								//format right
								outputHTML += 'style="float:right;" ';						
							} /*
							if (member.format.toUpperCase().indexOf("C") != -1) {
								//format center
								outputHTML += 'style="text-align:center;" ';
							} */				
							outputHTML += '>';
							
							if (member.link != "") {
								if (member.link.toLowerCase().indexOf("http") == 0) {
									outputHTML += '<a href="' + member.link + '" target="_detail"  class="ajLink">';
								} else {
									//get the FACT to pass as well
									
									outputHTML += '<a href="javascript:onShow(\'#' + member.link + '\',\'' + factText.text.replace(/'/g, "\\'") + '\');"  class="ajLink">';	
								}
							} 
							
							outputHTML +=  member.text;

							if (member.link != "") {
								outputHTML += '</a>';
							} 
							outputHTML += '</span>';
						}			

						if (member.type.toLowerCase() == "fact") {
							
							outputHTML += '<span class="fact_prompt" '; 
							
							if (member.format.toUpperCase().indexOf("L") != -1) {
								outputHTML += 'style="text-align:left;"';
							} else if (member.format.toUpperCase().indexOf("R") != -1) {
								outputHTML += 'style="float:right;"';
							} else if (member.format.toUpperCase().indexOf("C") != -1) {
								outputHTML += 'style="text-align:center;"';
							}			
							outputHTML += '>' + member.text + '</span>';
						}					
						
					}
					
					//break out of the element
					z=data.elements[x].members.length;
				} 
				
			} 
			
			outputHTML += "</div>"; //end element
		
		}
	}

	//show only if the user hasn't paid yet
	if (data.level != "") {
		if (data.level != 1) {

			outputHTML += '<span class="fact_entry" style="float:right;"><a href="https://www.privacyux.com/livestart" target="PrivacyUX">Get Yours Here</a></span>';
			outputHTML += '<span class="fact_prompt" style="text-align:left;">This is a Free Demo</span>';			
			
		}
	}

	
	//end the last section
	outputHTML += '</div>';
	
	$("#buttons_container").html(outputButtonHTML);
	
	return outputHTML;
	
}	

function drawDetails(data) {

	var outputHTML = "";
	
	if (!data) {
		data = d;
	}


	//for each element
	for (var x=0; x<data.details.length; x++) {	
	
		//head
		outputHTML += '<div id="' + data.details[x].eidlink + '" class="collapse"><div class="detail_head">' + data.details[x].head + '</div>';
		
		//add the factors
		if (data.details[x].factors.length > 0) {
			if ( data.details[x].head != "") {
				outputHTML += "<hr/>";			
			}
			outputHTML += '<div><ul>';
			for (var z=0;z<data.details[x].factors.length;z++) {
				outputHTML += '<li class="factor_text">';
				
				if (data.details[x].factors[z].info.toLowerCase().indexOf("http") == 0) {
					//if the info of the factor looks like an URL, link to it					
					outputHTML += '<a href="' + data.details[x].factors[z].info + '">';				
					
				} else if (data.details[x].factors[z].info != "") {
					//otherwise, just show it when clicked
					outputHTML += '<a href="javascript:showMoreFactorData(\'' + data.details[x].eidlink + '\',' + z + ',' + data.details[x].factors.length  + ');">';	
				}

				outputHTML += data.details[x].factors[z].text;
				if (data.details[x].factors[z].info != "") {
					outputHTML += '</a>';
				}
				outputHTML += '</li><div id="' + data.details[x].eidlink + z + '" style="display:none;" class="factor_detail">';
				outputHTML += data.details[x].factors[z].info;
				outputHTML += '</div>';
				
			}
			outputHTML += '</ul></div>';
			
		}
		
		//horizontal rule
		if ((data.details[x].foot != "") || (data.details[x].detaillinktext != "")) {
			outputHTML += "<hr/>";		
		}
		
		//footer
		outputHTML += '<div class="detail_foot">';
		outputHTML += data.details[x].foot;

		
		//detail link text and url
		if (data.details[x].detaillinkurl != "") {
			outputHTML += '<p><a href="' + data.details[x].detaillinkurl + '" target="_new" class="detail_link">';		
		}
		
		if (data.details[x].detaillinktext != "") {
			outputHTML += data.details[x].detaillinktext;			
		}
		
		if (data.details[x].detaillinkurl != "") {
			outputHTML +=  '</a></p>';
		}
	
		outputHTML += '</div>';		
		outputHTML += '<br><br></div>'
	
	}
	
	
	return  outputHTML;
	
}

 function onShow(strIDToShow, strPassedDetailTitle) {
	//hide all
	$('.collapse').not(strIDToShow).collapse('hide');
	$('.factor_detail').hide();

	//console.log($(strIDToShow).is(':visible'));
	if (!$(strIDToShow).is(':visible')) {
		$(strIDToShow).collapse('show');		
	}
	
	//load a header from AJAX
	loadDetailHeader(strPassedDetailTitle);
	//scroll to the top to view the detail page
	window.scrollTo(0,0);
 }
 
 var boolHeaderLoaded = false;
function loadDetailHeader(strDetailTitle) {

	if (strDetailTitle) {
		$("#PFIN_detail_title").html(strDetailTitle);
	}
		
	if (!boolHeaderLoaded)	{

		$("#PFIN_detail_title").show();
	    boolHeaderLoaded = true;
	  
		//handle the back button
		window.history.pushState(null,null,'#');
	  
	  	$("#detail_container").show();
	  
	}

}

//This function is used to show info field included with the factors in the accordion popup
function showMoreFactorData(strEIDLink,iFactor,iTotalFactors) {
	
	//console.log(strEIDLink);
		
	//hide all other factors
	for (var f=0; f<iTotalFactors; f++) {
		$('#' + strEIDLink + f).hide();
	}

	//show the selected factor
	$('#' + strEIDLink + iFactor).show();		
		

	
} 

//this funciton links to the appropriate "What Just Happened" page
function linkToWJH(strWJHID) {

	debug(strWJHID);
		
	//get the data from an AJAX call
	$.ajax({
		type: "GET",
		url: "../" + "WJH1.html",
		success: function(wjhHTML) {
			
			//show the appropriate popup
			showModal("What Just Happened?",wjhHTML);
			
		},
		error: function(xhr, status, error) {
			
			//why is this an error?
			debug("error loading WJH html",1);
			debug(error,1);
		}
		
	});
		

	

	
}

function printWJH() {
	
	window.print();
}



	
