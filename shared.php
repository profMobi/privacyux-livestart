<?php


//A library of shared PHP functions


/* MISCELLANEOUS FUNCTIONS */

//downloads a file from the Internet
function download($file_source, $file_target) {
    $rh = fopen($file_source, 'rb');
    $wh = fopen($file_target, 'w+b');
    if (!$rh || !$wh) {
        return false;
    }

    while (!feof($rh)) {
        if (fwrite($wh, fread($rh, 4096)) === FALSE) {
            return false;
        }
        echo ' ';
        flush();
    }

    fclose($rh);
    fclose($wh);

    return true;
}


//cracks text out of a string between two substrings
function get_string_between($string, $start, $end){
    $string = ' ' . $string;
    $ini = strpos($string, $start);
    if ($ini == 0) return '';
    $ini += strlen($start);
    $len = strpos($string, $end, $ini) - $ini;
    return substr($string, $ini, $len);
}

//returns a random string of a certain length
function random_string($length) {
    $key = '';
    $keys = array_merge(range(0, 9), range('a', 'z'));

    for ($i = 0; $i < $length; $i++) {
        $key .= $keys[array_rand($keys)];
    }

    return $key;
}

//writes to the analytics table
function analytics($name = "", $userid = "") {
	
	if (isset($_SERVER['REMOTE_ADDR']))  {
		
		//get data from the client and save it to identify the visitor
		$ipaddress 	= $_SERVER['REMOTE_ADDR'];
		$referrer 	= "";
		if (isset($_SERVER['HTTP_REFERER']))  {
			$referrer	= $_SERVER['HTTP_REFERER'];
		}
		$agent		= "";
		if (isset($_SERVER['HTTP_USER_AGENT']))  {
			$agent		= $_SERVER['HTTP_USER_AGENT'];
		}
		
		if (isset($_SESSION['userid']) && $userid=="") {
			$userid = $_SESSION['userid'];
		} 
		//test to see if the query string with the userid
		if (isset($_GET['userid']) && $userid =="" ) {
			$userid = $_GET['userid'];
		} 	

		//test to see if we have a cookie with the userid
		if(isset($_COOKIE['userid']) && $userid == "") {
		   $userid = $_COOKIE['userid'];
		} 
	
		//write to the database 
		//Get the MySQL/ConsentCheq credentials
		include("cred.inc");

		//MySQL include library
		include("conn.inc");
		
		if ($name == "") {
			$name = "PAGE LOAD";
		}
		
		$userhash = hash('ripemd160',$ipaddress . $agent);

		//insert the analytics record
		$sql = "INSERT INTO `analytics` (`userid`,`userhash`, `referrer`, `name`) VALUES ('" . $userid . "','" . $userhash . "','" . $referrer . "','" . $name . "');";		

		//send the query
		$result = $connection->query($sql); 
		
		//get any SQL error
		$sqlError = mysqli_error($connection);
		
		$rtn = "";
		if ($sqlError != "") {
			
			//sql error
			$rtn = json_encode(array(
				"rtn"  => "error",  
				"rtnmsg" => "error inserting analytics entry: " . $sqlError,
				"email" => $email
				));
				   
		
			
		} else {

			$rtn = json_encode(array(
				"rtn"  => "ok",
				"rtnmsg" => "analytics milestone saved"
				)); 
			
		}

		//return the result
		return $rtn;
		die();		
		
		
	} else {
		
		return json_encode(array(
				"rtn"  => "fail",  
				"rtnmsg" => "not on an acutal server"
		));
	}
		
	
}




?>