<?php
//This is the javascript editor for PrivacyUX Livestart

//include functions to lighten the load
include ("functions/retrievedataFunctions.php");
include ("functions/helperFunctions.php");


//global variables
$pStatusMessage = "";

//Test to confirm that we have a valid login
session_start();

if (!isset($_SESSION['auth']) || !isset($_SESSION['userid'])) {

	//redirect to login if no auth session flag
	header("Location: " . SERVER_PATH . "/login.php");

} elseif ($_SESSION['auth'] != "1" || $_SESSION['userid'] == "") {

	//redirect to login if no auth session flag
	header("Location: " . SERVER_PATH . "/login.php");	
	
}

//get the userid
$userid = $_SESSION['userid'];

//set the level of the login	
if (isset($_SESSION['level'])) {
	
	//are they a new customer, a paying customer, or a timed out customer?
	$level = $_SESSION['level'];	
	
} else {
	$level = "";	
}

//get the user's most current notice
$objReturn = json_decode(getNoticeFromUserId($userid));

if ($objReturn -> rtn != "ok") {
	
	//in case of error or failure, display the error in the status message
	$pStatusMessage =  $objReturn -> rtnmsg;
	
} else {
	
	//get the current notice id
	$noticeid = $objReturn -> id;
	
}	


?><!DOCTYPE html>


<!--
<?php
echo "DEBUG BLOCK: " . $_SESSION['userid'] . PHP_EOL;
echo "AUTHORIZATION CODE:" . $_SESSION['auth'] . PHP_EOL;

?>
-->

<?php
include 'shared.php';
?>

<html lang="en">

<head>

<?php
readfile('html/head.html');
readfile('html/style.html');
?>

<style>
	textarea {
		width:100%;
		height:140px;
	}
	
	.divFactorBlock {
		padding:5px;
		margin:5px;
		border:2px solid black;
	}
	
	.divFactorsBlock {
		border:2px solid black;		
	}
</style>


</head>
<body onload="doLoad();">


<?php if ($_SESSION['auth'] == "1") {

readfile('html/navigation.html');

} else { 

readfile ('html/blankNavigation.html');

}  ?>


<!-- Page Content -->
<div class="container">


<br/><br/>

<h1 class="my-4">Notice at Collection Editor</h1>

<div class="row">
	<!-- Left Panel -->
	<div id="divLeftPanel" class="col-sm-6 col-md-6">
		
		<div id="divElements">
			Loading 
		</div>

		<div id="divElementEditor" style="display:none;">
		
			<button id="btnUpdate" name="btnUpdate" onclick="doUpdate();" type="submit" class="btn btn-primary">Update Notice</button> <button id="btnRemoveElement" name="btnRemoveElement" onclick="removeElement();" type="submit" class="btn btn-danger">Delete Element</button>	<br/><br/>
					
			<!-- Prompt -->
			<label for="txtPrompt">Prompt:</label><br/>
			<input id="txtPrompt" name="txtPrompt" type="text" class="form-control" required /><br/>
				
			<!-- Information -->	
			<label for="txtInfo">Information:</label><br/>
			<input id="txtInfo" name="txtInfo" type="text" class="form-control"  required /><br/>
				
			<!-- Type -->	
			<label for="selType">Element Type:</label><br/>
			<select id="selType" name="selType" class="form-control" onchange="doSelectElementType();">
				<option value="static">Static Text</option>
				<option value="xurl">External URL</option>
				<option value="popup">Popup Details</option>
			</select><br/>
			
			<!-- Link URL -->
			<div id="divLinkURL" style="display:none;">
				<label for="txtLinkURL">Link URL:</label><br/>
				<input id="txtLinkURL" name="txtLinkURL" type="text" class="form-control"  /><br/>		
			</div>
			
			<!-- Popup Details -->
			<div id="divPopupDetails" style="display:none;">
	
				<!-- Heading -->
				<label for="txtHeading">Heading:</label><br/>
				<textarea id="txtHeading" name="txtHeading" class="form-control" ></textarea><br/>
							
				<!-- Footer -->	
				<label for="txtFooter">Footer:</label><br/>
				<textarea id="txtFooter" name="txtFooter" class="form-control" ></textarea><br/>
					
				<!-- Footer Link Text -->	
				<label for="txtFooterLinkText">Footer Link Text:</label><br/>
				<input type="text" id="txtFooterLinkText" name="txtFooterLinkText" class="form-control" ></input><br/>
					
				<!-- Footer Link URL -->	
				<label for="txtFooterLinkURL">Footer Link URL:</label><br/>
				<input type="text" id="txtFooterLinkURL" name="txtFooterLinkURL" class="form-control" ></input><br/>
				
				<!-- Factors -->
				<div id="divFactors" name="divFactors" style="display:none;" class="divFactorsBlock"></div>
	
			</div>
			
			<br/>
			<button id="btnUpdate" name="btnUpdate" onclick="doUpdate();" type="submit" class="btn btn-primary">Update Notice</button> <button id="btnRemoveElement" name="btnRemoveElement" onclick="removeElement();" type="submit" class="btn btn-danger">Delete Element</button>	
					
		</div>

		
		<br/><br/>
				
	
	
	
	</div>


	<!-- Right Panel -->
	<div id="divRightPanel" class="col-sm-6 col-md-6">
	
		<!-- cell phone example -->
		<div id="phone" style="position:relative;display:none;">
			<div id="phoneSilhouette" class="phone"></div>
			<div id="phoneButtonLeft0" class="phone"></div>
			<div id="phoneButtonLeft1" class="phone"></div>	
			<div id="phoneButtonLeft2" class="phone"></div>	
			<div id="phoneButtonRight0" class="phone"></div>			
			<div id="phoneContent" class="phone"><iframe id="framedPFIN" src="privacyNotice.php?uid=<?php echo $_SESSION['userid'] ?>" style="width:100%;height:100%; border:none;"></iframe></div>
		</div>

	</div>
	
</div>
<!-- end of the row -->

</div>
<!-- end of the container -->	

<br/><br/>


<?php
readfile ('html/bootstrapCore.html');
readfile ('html/footer.html');
readfile ('html/modal.html');
readfile ('html/loader.html');
?>
	
	
</body>

<script>

//data object
var d;

//userid
var userid = "<?php echo $userid; ?>";

//selected element
var iSelectedElement;

//on page load, get the data object and share it with the PFIN
$( document ).ready(function() {
	
	//DEBUG
    //console.log( "page ready - get data" );
	
	//get the data from an AJAX call
	$.ajax({
		type: "GET",
		dataType:"json",
		url: "jsonData.php?noticeid=<?php echo $noticeid; ?>",
		success: function(data) {
			d = data;
			
			$("#divElements").html(returnElements());
			
			//update the frame
			document.getElementById("framedPFIN").contentWindow.location = "privacyNotice.php";			
			
		},
		error: function(xhr, status, error) {
			
			//why is this an error?
			debug("error loading PFIN data",1);
			debug(error,1);
		}
		
	});
	

});

function doUpdate() {

	//update the data object	
	console.log("in doUpdate");
	
	//create the replacement detail
	updateDetail = {}
	
	//test to see if there is a detail page or not
	if ($("#selType").val() == "popup") {

		//fill the new detail from the fields
		updateDetail.eidlink = "E"+iSelectedElement;
		
		//!!! DEBUG
		//console.log("detail EID " +  updateDetail.eidlink);
		
		updateDetail.head = $("#txtHeading").val();
		updateDetail.foot = $("#txtFooter").val();
		updateDetail.detaillinktext = $("#txtFooterLinkText").val();
		updateDetail.detaillinkurl = $("#txtFooterLinkURL").val();
		updateDetail.factors = [];
	
		//add the factors into the array by looking for factor DIV tags
		var v=0;
		
		//DEBUG
		//console.log("WHAT IS THIS?");
		//console.log($("#divFactor" + v).length);
		
		//if the div exists, then there is a factor there
		while ( $("#divFactor" + v).length > 0 ) {
			updateFactor = {};
			updateFactor.text = $("#txtFactorText" + v).val();
			updateFactor.info = $("#txtFactorDetail" + v).val();
			updateFactor.factorlink = "";
			updateFactor.order = v;
			updateDetail.factors.push(updateFactor);
			v = v+1;
		}
	
	}
	
	//Identify the matching detail
	if (getDetailByEID(d.elements[iSelectedElement].members[1].link) >= 0) {
		
		//determine whether we should erase the detail or overwrite it
		if ($("#selType").val() == "popup") {
			
			//!!! DEBUG
			//console.log("Element Link ID " + getDetailByEID(d.elements[iSelectedElement].members[1].link));
			
			//write the detail back to the JSON data object
			d.details[getDetailByEID(d.elements[iSelectedElement].members[1].link)] = updateDetail;
			
		} else {
			
			//!!! DEBUG
			//console.log("deleting element #: " + getDetailByEID(d.elements[iSelectedElement].members[1].link));
			
			//erase the existing detail
			d.details.splice(getDetailByEID(d.elements[iSelectedElement].members[1].link),1);
		}
		
	}
		
	//DEBUG -- what is the array position of the element that was changed?
	console.log("selected element array position: " + iSelectedElement);
	
	//fill the new element data from the fields
	updateElement = {};
	updateElement.order = iSelectedElement;
	updateElement.elementid = "";
	
	//put in the appropriate link for the member if it links to popup
	strMemberLink = "";
	if ($("#selType").val() == "popup") {
		strMemberLink = "E"+iSelectedElement;
	} 
	
	//put in the URL if the member links to an external link
	if ($("#selType").val() == "xurl") {
		strMemberLink = $("#txtLinkURL").val();
	} 
	
	updateElement.members = [{text:$("#txtPrompt").val(),format:"L",link:"",type:"fact"},{text:$("#txtInfo").val(),format:"R",link:strMemberLink,type:"info"}];
	
	//write the element back to the JSON data object
	d.elements[iSelectedElement] = updateElement;

	//update the frame, pulling from the JSON data object
	document.getElementById("framedPFIN").contentWindow.location.reload();
	
	//hide and return to the elements view
	$("#divElementEditor").hide();
	$("#divFactors").hide();
	$("#divElements").show();
	
	//write the JSON object back to the database
	doSaveToDatabase();

	
}

//This function gets HTML listing all the elements
function returnElements() {
	
	var returnHTML = "";
	
	//make sure there is a data object
	if (d) {
		
		//loop through the elements	
		for (x=0;x<d.elements.length;x++) {
			returnHTML = returnHTML + "<li><a href='#' onclick='elementUI(" + x  + ");'>" + d.elements[x].members[0].text + "</a></li>\r";
			
		}	
			
	}

	//button to add new element
	//returnHTML += '<br/><button id="btnAdd" name="btnAdd" onclick="addElement();" type="submit" class="btn btn-success">Add New Element</button>';	
	
	return returnHTML;
	
}

//when someone selects a different element type
function doSelectElementType() {
	console.log("doSelectElementType: " + $("#selType").val() );
	
	var val = $("#selType").val();
	if (val == "static") {
		//hide fields
		$("#divLinkURL").hide();
		$("#divPopupDetails").hide();
	}
	if (val == "xurl") {
		$("#divLinkURL").show();
		$("#divPopupDetails").hide();		
	}
	if (val == "popup") {
		$("#divLinkURL").hide();
		$("#divPopupDetails").show();
	}
	
}

//this function opens a UI for an element
function elementUI(iElement) {
	
	console.log("elementUI: " + iElement);
	
	//hold on to the array position of the selelcted element
	iSelectedElement = iElement;
	
	//fill the element fields
	$("#txtPrompt").val(d.elements[iElement].members[0].text);  
	$("#txtInfo").val(d.elements[iElement].members[1].text);  
	
	if (d.elements[iElement].members[1].link != "") {
		
		//text to see if the link URL is valid
		if (d.elements[iElement].members[1].link.toLowerCase().indexOf("http") == 0) {
			$("#txtLinkURL").val(d.elements[iElement].members[1].link);
			$("#selType").val("xurl");
			doSelectElementType();
			
		} else {
			//link to the detail layer
			$("#selType").val("popup");
			doSelectElementType();	

			//load the detail layer
			var eidLink = d.elements[iElement].members[1].link;
			var iDetail = getDetailByEID(eidLink);
			
			//make sure there are details in there first
			if (d.details[iDetail]) {
				$("#txtHeading").val(d.details[iDetail].head);
				$("#txtFooter").val(d.details[iDetail].foot);
				$("#txtFooterLinkText").val(d.details[iDetail].detaillinktext);
				$("#txtFooterLinkURL").val(d.details[iDetail].detaillinkurl);				

				//reload the factors
				factorsUI(iElement, iDetail);

			}
		}
	}
	
	//switch the views
	$("#divElements").hide();
	$("#divElementEditor").show();
	
}

//draw the factors for the selected element/detail page
function factorsUI(iElement, iDetail) {
				
	//empty the factors DIV
	$("#divFactors").html("");
	
	//DEBUG!!!
	console.log("factors: " + d.details[iDetail].factors.length);
	
	//load any factors if they are there		
	if (d.details[iDetail].factors.length > 0) {
		
		//!!!! TO DO watch out for quotes and aspotrophes in teh data!!11!!!
		for (var f=0;f<d.details[iDetail].factors.length;f++){
			var factorHTML = '<div id="divFactor' + f + '" name="divFactor' + f + '" class="divFactorBlock">';
			factorHTML += '<label for="txtFactorText' + f + '">Factor Text:</label>';
			factorHTML += '<input type="text" id="txtFactorText' + f + '" name="txtFactorText' + f + '" class="form-control" value="' + d.details[iDetail].factors[f].text + '" ></input><br/>';
			factorHTML += '<label for="txtFactorDetail' + f + '">Factor Detail:</label>';
			factorHTML += '<textarea type="text" id="txtFactorDetail' + f + '" name="txtFactorDetail' + f + '" class="form-control">' + d.details[iDetail].factors[f].info + '</textarea><br/>';
			
			//add buttons to delete and move factors
			factorHTML += '<button type="button" class="btn btn-danger" onclick="removeFactorFromDetail(' + iElement + ',' + iDetail + ',' + f + ');">Remove</button>';
			
			factorHTML += '</div>';
			
			//update the div
			$("#divFactors").html($("#divFactors").html() + factorHTML);
			
		}

		//insert the button to add a new factor
		factorHTML = '<button type="button" style="margin:5px;" class="btn btn-success" onclick="addNewFactor(' + iElement + ',' + iDetail + ');">Add New Factor</button>';
		
		//update the div
		$("#divFactors").html($("#divFactors").html() + factorHTML);
		
		//show the factors DIV
		$("#divFactors").show();
	
	}

	
}

//gets the index in the array of an existing detail by the EID 
function getDetailByEID(strEID) {
	
	for (var x=0;x<d.details.length;x++) {
		if (d.details[x].eidlink == strEID) {
			return x; 
		}
	}
	
	//return the index of a -1 if there is no match
	return -1;
}

//remove a particular factor from a particular detail and then refesh the element edit
function removeFactorFromDetail(iElement, iDetail, iFactor) {
	
	//DEBUG
	console.log("in removeFactorFromDetail");
	console.log(iDetail);
	console.log(iFactor);
	
	//erase the factor
	d.details[iDetail].factors.splice(iFactor,1);
	
	//reload
	factorsUI(iElement, iDetail);
	
	return false;
}

//add a new factor to the indicated detail and then refesh the element edit
function addNewFactor(iElement, iDetail) {
	
	//DEBUG
	console.log("in addNewFactor");
	console.log(iDetail);
	
	//drop in a blank factor for that detail
	var objBlankFactor 			= {};
	objBlankFactor.text 		= "";
	objBlankFactor.info 		= "";
	objBlankFactor.factorlink 	= "";
	objBlankFactor.order 		= "";
	d.details[iDetail].factors.push(objBlankFactor);
	
	//reload
	factorsUI(iElement, iDetail);
	
	return false;
}

//add a new element to the page
function addNewElement() {
	
}

//remove the element at a particular array position
function removeElement(iElement) {
	
	//DEBUG
	console.log("in removeElement");
	
	//show the thinking screen
	showLoader(true);	
	
	if (iElement == null) {
		iElement = iSelectedElement;
	}
	
	console.log(iElement);
	
	//erase any associated details
	iDetail = getDetailByEID(d.elements[iElement].members[1].link);
	if (iDetail >= 0) {
		//d.details.splice(iDetail,1);		
	}
	
	console.log(iDetail);
	
	//erase the element
	d.elements.splice(iElement,1);
	
	//reload the elements
	$("#divElements").html(returnElements());
	
	//update the frame, pulling from the JSON data object
	document.getElementById("framedPFIN").contentWindow.location.reload();
	
	//hide and return to the elements view
	$("#divElementEditor").hide();
	$("#divFactors").hide();
	$("#divElements").show();
	
	//update the data
	doSaveToDatabase();
	
	//hide the loader
	showLoader(false);	
	
	return false;
	
}

//write back to the database
function doSaveToDatabase() {
	
	//create the post data object to send to the tools
	var objPostData = {};	
	objPostData["userid"] 		= userid;  
	objPostData["noticejson"] 	= JSON.stringify(d); 
	objPostData["cmd"] 			= "createnoticejson";

	//trigger create the notice by passing the JSON
	$.post("tools.php",objPostData,function(data) {			

		//turn the string data returned back into a JSON object
		var objReturnData = JSON.parse(data);

		if (objReturnData.rtn != "ok") {
			
			//!!!what do we do in case of a failure?
			console.log("ERROR WRITING BACK TO DATABASE");
			console.log(objReturnData);

		} else {
			//written to the database successfully
			
			//!!! DEBUG
			console.log(objReturnData);
		}
		
	});	
	
}

//^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^6

//called when this page loads
function doLoad() {
	
	//hide the gradient background in the PFIN and resize it
	document.getElementById("framedPFIN").contentWindow.document.getElementsByTagName("body")[0].style.backgroundImage = "none";
	onResize();
	
}


//called when the PFIN resizes
function onResize() {
	
	var phoneWidth = 440;
	
	//console.log(window.innerHeight);
	var scaleRatio = (window.innerHeight * 0.8) / 1000;
	var maxWidth = document.getElementById("divLeftPanel").offsetWidth;	
	
	//do I scale based on height or width?
	if ((phoneWidth * scaleRatio) >= maxWidth) {
		scaleRatio = maxWidth/phoneWidth;
	}	

	document.getElementById("phone").style.transform 	= "scale(" + scaleRatio + ")";
	document.getElementById("phone").style.top 		=  "-" + (window.innerHeight * 0.1) + "px";
	document.getElementById("phone").style.display		="block";	
	
}

//set the window's resize event
window.onresize = onResize;

</script>
</html>