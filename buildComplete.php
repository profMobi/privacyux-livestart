<!DOCTYPE html><?php
//This page is used to BUILD the privacy dialog

//Identify variables
	$userid = "";
	$level = "";
	$iStep = 0;

//Test whether the user is logged in or not
session_start();
if (!isset($_SESSION['auth'])) {
	
	//clear the current session variables if we aren't authorized
	$_SESSION['auth'] = "0";
	$_SESSION['userid'] = "";
	$_SESSION['level'] = "";

} else {
	
	//we are in a session, get the level and userid
	if (isset($_SESSION['userid'])) {
		//get the userid from the session
		$userid = $_SESSION['userid'];
		
		//write the userid to a cookie just in case we get broken
		setcookie('userid', $userid);
		
		//get the level if it is available
		if (isset($_SESSION['level'])){
			$level = $_SESSION['level'];
		}		
		
	} 
}

//test to see if the query string with the userid
if (isset($_GET['userid']) && $userid =="" ) {
	$userid = $_GET['userid'];
} 	

//test to see if we have a cookie with the userid
if(isset($_COOKIE['userid']) && $userid == "") {
   $userid = $_COOKIE['userid'];
}

//test to see if we have a cookie with the userid
if(isset($_COOKIE['s']) ) {
	if (intval($_COOKIE['s']) > 0 && intval($_COOKIE['s']) < 6) {
		$iStep = intval($_COOKIE['s']);		
	}

}

//test to see if the query string holds a newer current step
if (isset($_GET['s'])) {
	if (intval($_GET['s']) > 0 && intval($_GET['s']) < 6) {
		$iStep = intval($_GET['s']);
	}
} 	

//global variables
$pStatusMessage = "";

echo "<!-- " . PHP_EOL;
echo "DEBUG BLOCK" . PHP_EOL;
echo "USERID:" . $userid . PHP_EOL;
echo "CURRENT BUILD STEP: " . $iStep . PHP_EOL;
echo " -->" . PHP_EOL;
 
include 'shared.php';
include 'functions/helperFunctions.php';
include 'functions/userFunctions.php';
include 'functions/qrFunctions.php';

//write analytics 
analytics("PAGE LOAD - BUILD.PHP");
?>
<html lang="en">
<head>

<?php
readfile('html/head.html');
readfile('html/style.html');
?>

<script>
<?php include 'buildProcessor.php'; ?>
</script>

</head>
<body onload="doLoad();">

<?php 
readfile ('html/blankNavigation.html');
readfile ('html/footerUnderflow.html');
?>

<!-- Page Content -->
<div class="container">

<!-- Single Row-->
<div class="row">

	<!-- Left Panel -->
	<div id="divLeftPanel" class="col-12 col-sm-12 col-md-5 col-lg-5 col-xl-5">
	
	<div class="divSpacerTop" ></div>

		<!-- cell phone example -->
		<div id="phone" style="position:relative;display:none;">
			<div id="phoneSilhouette" class="phone"></div>
			<div id="phoneButtonLeft0" class="phone"></div>
			<div id="phoneButtonLeft1" class="phone"></div>	
			<div id="phoneButtonLeft2" class="phone"></div>	
			<div id="phoneButtonRight0" class="phone"></div>			
			<div id="phoneContent" class="phone"><iframe id="framedPFIN" src="privacyNotice.php" style="width:100%;height:100%; border:none;"></iframe></div>
		</div>
	
	</div>
	<!-- End Left Panel -->

	<!-- Right Panel -->
	<div id="divRightPanel" class="col-12 col-sm-12 col-md-7 col-lg-7 col-xl-7">
	
		<!-- BUILD TUNNEL -->
	
		<div class="divSpacerTop" ></div>

<!-- Mobile Accordian -->	
<div class="accordion" id="mobileAccordion">
  <div class="card border-bottom">
	<div class="card-header" id="headingOne">
	  <h2 class="mb-0">
		Your custom CCPA Notice At Collection is Complete
	  </h2>
	</div>
	<div id="mobileStepSix" class="show" aria-labelledby="headingOne" data-parent="#mobileAccordion">
	  <div class="card-body">
	
		<?php if (true) { ?>

				<p>You can access your notice at:<br/><span id="spnNoticeURL"><a href="<?php echo SERVER_PATH . "/privacyNotice.php?uid=" . $userid; ?>" target="new"><?php echo SERVER_PATH . "/privacyNotice.php?uid=" . $userid; ?></a></span></p>

				<?php
				//add the QR code to this page
				$noticeURL = SERVER_PATH . "/privacyNotice.php?uid=" . $userid;
				$qrURL = generateQR($noticeURL);				
				if (false) {

					if ($qrURL != "") {
						
						echo "<img src='functions/" . $qrURL . "' />";
					}						
				}
				?>
		
			<p>Use the link to share your custom Notice at Collection with your colleagues to show them how PrivacyUX Livestart quickly delivers CCPA notice compliance for the next thirty (30) days.</p>
			<p>To continue improving your privacy notice using our editor, check your email to confirm your identity and choose a password.</p>	

		<?php } ?>		


		<?php if (false) { ?>		
		<p>
		After you confirm your identity by clicking on the email we sent and choose a password for your account, you'll be given a link to your custom Notice At Collection and an email that you can share with your colleagues to show them how PrivacyUX Livestart quickly delivers CCPA notice compliance. 
		</p><p> 
		This "Nutrition style" notice gives your company the same user trust building transparency that drove Apple to require nutrition style notices for each of the 2 million publishers in the App Store. The difference is that <b>your PrivacyUX Livestart notice at collection works at every user touch point</b>.  
		 </p><p> 
		PrivacyUX Livestart rapidly solves the first and most obvious CCPA compliance requirement but it is not a complete solution. PrivacyCheq offers a complete transparency and opt out management service for CCPA that handles the complexity of gathering user preferences (including opt out), operationally honoring those preferences, distributing the preferences to third, fourth, and fifth parties that you share or sell data to.  And if your company's audience includes users under the age of 17, PrivacyUX for CCPA handles the complexity of consent for children 13-16 and for parental consent for children under 13. 
		 </p>
		<?php } ?> 
		
		<p> 
		If your company wants to quickly and easily take leadership with user trust, we'd love to talk to you about our complete solution for CCPA.  Contact us at <a href="mailto:bizdev@privacycheq.com">bizdev@privacycheq.com</a> or via <a href="https://privacyux.com/#get">this form</a>.</p>
		
	  
	  </div>
	</div>
  </div>
</div>	
<!-- End Mobile Accordian -->	
		
		<!-- Step By Step -->
		<div id="divStepByStep">

			<!-- Step 6 -->
			<div id="divStep6">
		
				<!-- PROGRESS METER -->
				<span class="progressMeter" style="width:100%;">
					<div id="divProgress5" class="progress-bar btnText-privacyux progressMeterCreep" >&nbsp;&nbsp;Progress Complete</div>
				</span>


				<h1>Your custom CCPA Notice At Collection is Complete</h1>

				
				<?php if (true) { ?>

						<p>You can access your notice at:<br/><span id="spnNoticeURL"><a href="<?php echo SERVER_PATH . "/privacyNotice.php?uid=" . $userid; ?>" target="new"><?php echo SERVER_PATH . "/privacyNotice.php?uid=" . $userid; ?></a></span></p>

					<?php
						if (true) {
							echo "<!-- QRCODE DEBUG: ";
							echo $qrURL; 
							echo " -->";
							if ($qrURL != "") {
								
								echo "<img src='functions/" . $qrURL . "' style='float:right;' />";
							}						
						}
					?>	
				
				<p>Use the link to share your custom Notice at Collection with your colleagues to show them how PrivacyUX Livestart quickly delivers CCPA notice compliance for the next thirty (30) days.</p>
				<p>To continue improving your privacy notice using our editor, check your email to confirm your identity and choose a password.</p>	

				<?php } ?>		


				<?php if (false) { ?>		
				<p>
				After you confirm your identity by clicking on the email we sent and choose a password for your account, you'll be given a link to your custom Notice At Collection and an email that you can share with your colleagues to show them how PrivacyUX Livestart quickly delivers CCPA notice compliance. 
				</p><p> 
				This "Nutrition style" notice gives your company the same user trust building transparency that drove Apple to require nutrition style notices for each of the 2 million publishers in the App Store. The difference is that <b>your PrivacyUX Livestart notice at collection works at every user touch point</b>.  
				 </p><p> 
				PrivacyUX Livestart rapidly solves the first and most obvious CCPA compliance requirement but it is not a complete solution. PrivacyCheq offers a complete transparency and opt out management service for CCPA that handles the complexity of gathering user preferences (including opt out), operationally honoring those preferences, distributing the preferences to third, fourth, and fifth parties that you share or sell data to.  And if your company's audience includes users under the age of 17, PrivacyUX for CCPA handles the complexity of consent for children 13-16 and for parental consent for children under 13. 
				 </p>
				<?php } ?> 
				
				<p> 
				If your company wants to quickly and easily take leadership with user trust, we'd love to talk to you about our complete solution for CCPA.  Contact us at <a href="mailto:bizdev@privacycheq.com">bizdev@privacycheq.com</a> or via <a href="">this form</a>.</p>
				
			
			</div>
			<!-- End Step 6 -->			
		
		</div>
		<!-- End Step By Step -->

	
	</div>
	<!-- End Right Panel -->	

</div>
<!-- End Row -->

</div> 
<!-- End Page Content -->
	

<br/><br/>
	
<?php
readfile ('html/bootstrapCore.html');
readfile('html/modal.html');
readfile('html/loader.html');
?>
	
	
</body>


<script src="script/buildValidation.js"></script>
<script src="script/buildUpdate.js"></script>
<script src="script/localStorage.js"></script>


<script>

//identify the step we loaded into
var iStep = 5;
var iNextStep = iStep + 2;

//called when this page loads
function doLoad() {
	
	//hide the gradient background in the PFIN and resize it
	document.getElementById("framedPFIN").contentWindow.document.getElementsByTagName("body")[0].style.backgroundImage = "none";
	onResize();
	
}



//called when the PFIN resizes
function onResize() {
	
	var phoneWidth = 440;
	
	//console.log(window.innerHeight);
	var scaleRatio = window.innerHeight / 1000;
	var maxWidth = document.getElementById("divLeftPanel").offsetWidth;	
	
	//do I scale based on height or width?
	if ((phoneWidth * scaleRatio) >= maxWidth) {
		scaleRatio = maxWidth/phoneWidth;
	}	
	
	//shuffle the phone to the left a little bit
	var scootchLeftOnResize = (phoneWidth/2) * (scaleRatio - 1); 

	document.getElementById("phone").style.transform 	= "scale(" + scaleRatio + ")";
    document.getElementById("phone").style.left 		= scootchLeftOnResize + "px";
	document.getElementById("phone").style.display		= "block";	

}

//set the window's resize event
window.onresize = onResize;

</script>
<script src="script/utility.js"></script>


</html>













