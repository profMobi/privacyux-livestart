<!DOCTYPE html>
<?php
include 'shared.php';

//write analytics 
//analytics("PAGE LOAD - BUILD.PHP");
?>

<html lang="en">
<head>

<?php
readfile('head.html');
readfile('style.html');
?>

<!--
DEBUG: 
-->
<script>
<?php include 'buildProcessor.php'; ?>
</script>
</head>
<body onload="doLoad();">

<?php 

readfile ('blankNavigation.html');
readfile ('footerUnderflow.html');

?>

<!-- Page Content -->
<div class="container">

<!-- Single Row-->
<div class="row">

	<!-- Left Panel -->
	<div id="divLeftPanel" class="col-sm-5 col-md-5">
	
	<div class="divSpacerTop" ></div>

		<!-- cell phone example -->
		<div id="phone" style="position:relative;display:none;">
			<div id="phoneSilhouette" class="phone"></div>
			<div id="phoneButtonLeft0" class="phone"></div>
			<div id="phoneButtonLeft1" class="phone"></div>	
			<div id="phoneButtonLeft2" class="phone"></div>	
			<div id="phoneButtonRight0" class="phone"></div>			
			<div id="phoneContent" class="phone"><iframe id="framedPFIN" src="privacyNotice.php" style="width:100%;height:100%; border:none;"></iframe></div>
		</div>
	
	</div>
	<!-- End Left Panel -->

	<!-- Right Panel -->
	<div id="divRightPanel" class="col-sm-7 col-md-7">
	<div class="divSpacerTop" ></div>	
	
		<!-- BUILD TUNNEL -->


		<!-- STEP BUTTON -->
		<button class="btn btn-privacyux">Step 
		<span class="btnStepBubble" style=""><span class="btnStepBubbleText">2</span></span>
		</button>
			
		<!-- PROGRESS METER -->
		<span class="progressMeter" style="">
			<div id="divProgress2" class="progress-bar btnText-privacyux progressMeterCreep">&nbsp;&nbsp;Progress</div>
		</span>
		
		<h1>Let’s&nbsp;Build&nbsp;Your Custom CCPA Notice&nbsp;at&nbsp;Collection</h1>

		<form id="build1" class="" action="javascript:doUpdate('build1');" method="post" onsubmit="return(validateForm('build1'));">
					
					<label for="orgfunction">What does your company do? (in a sentence)</label> <sup><i onclick="$(this).tooltip('show');" class="fas fa-info-circle" data-toggle="tooltip" data-html="true" data-placement="top" title="Use this sentence to briefly describe to your customers what you need their personal data for."></i></sup><br/>			
					<input id="orgfunction" name="orgfunction" type="text" class="form-control" placeholder="We create widgets." required><br/>

					<label for="orglocation">What is your company's address?</label> <sup><i onclick="$(this).tooltip('show');" class="fas fa-info-circle" data-toggle="tooltip" data-html="true" data-placement="top" title="This information is used to identify your organization and give customers the ability to write your organization by post with any questions about their privacy."></i></sup><br/>		
					<input id="orglocation" name="orglocation" type="text" class="form-control"  placeholder="123 Elm St. Anytown US 00000" required><br/>

					<label for="orgphone">What is your customer support phone number? (optional)</label> <sup><i onclick="$(this).tooltip('show');" class="fas fa-info-circle" data-toggle="tooltip" data-html="true" data-placement="top" title="If your customers have a question about how your organization treats their personal data, what phone number could they use?"></i></sup><br/>	
					<input id="orgphone" name="orgphone" type="text" class="form-control"  placeholder="800.123.3333"><br/>

					<label for="orgemail">What is the email address for privacy concerns? (optional)</label> <sup><i onclick="$(this).tooltip('show');" class="fas fa-info-circle" data-toggle="tooltip" data-html="true" data-placement="top" title="If your customers have a question about how your organization treats their personal data, what email address should they use?"></i></sup><br/>	
					<input id="orgemail" name="orgemail" type="text" class="form-control"  placeholder="privacy@company.com" required><br/>

					<label for="orgurl">What is the URL of your main website?</label> <sup><i onclick="$(this).tooltip('show');" class="fas fa-info-circle" data-toggle="tooltip" data-html="true" data-placement="top" title="Enter the web address of your company's marketing website."></i></sup><br/>	
					<input id="orgurl" name="orgurl" type="text" class="form-control"  placeholder="http://www.company.com" required><br/>
					<button id="nofooterButton" class="btn btn-privacyux" onclick="submitNext();">Next</button>
					
		</form>


	</div>
	<!-- End Right Panel -->	

</div>
<!-- End Row -->

</div> 
<!-- End Page Content -->
	

<br/><br/>
	
<?php
readfile ('bootstrapCore.html');
readfile('modal.html');
readfile ('loader.html');
?>
	
	
</body>

<script src="script/buildValidation.js"></script>
<script src="script/buildUpdate.js"></script>


<script>
//called when this page loads
function doLoad() {

	document.getElementById("footerButton").innerHTML = "Move to Step " + 3 + " <span id='blackBubble'><span>&gt;</span></span>";	
	
	//hide the gradient background in the PFIN and resize it
	document.getElementById("framedPFIN").contentWindow.document.getElementsByTagName("body")[0].style.backgroundImage = "none";
	onResize();
	
}


//called when the PFIN resizes
function onResize() {
	
	var phoneWidth = 440;
	
	//console.log(window.innerHeight);
	var scaleRatio = window.innerHeight / 1000;
	var maxWidth = document.getElementById("divLeftPanel").offsetWidth;	
	
	//do I scale based on height or width?
	if ((phoneWidth * scaleRatio) >= maxWidth) {
		scaleRatio = maxWidth/phoneWidth;
	}	
	
	//shuffle the phone to the left a little bit
	var scootchLeftOnResize = (phoneWidth/2) * (scaleRatio - 1); 
	console.log(scootchLeftOnResize);
	

	document.getElementById("phone").style.transform 	= "scale(" + scaleRatio + ")";
    document.getElementById("phone").style.left 		=  scootchLeftOnResize + "px";
	document.getElementById("phone").style.display		="block";	

	
}

//set the window's resize event
window.onresize = onResize;

</script>
<script src="utility.js"></script>
</html>













