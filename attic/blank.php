<?php
//This is a blank page in PrivacyUX


//Test to confirm that we have a valid login
session_start();
if (!isset($_SESSION['auth'])) {
	$_SESSION['auth'] = "0";
	$_SESSION['user'] = "";
	$_SESSION['level'] = "";
} else {
	
	$level = "";
}

//set the level of the login	
if (isset($_SESSION['level'])) {
	$level = $_SESSION['level'];	
	$userid = $_SESSION['user'];
} else {
	$level = "";	
}


//global variables
$pStatusMessage = "";


?><!DOCTYPE html>


<!--
<?php
echo "DEBUG BLOCK:" . $userid . PHP_EOL;


?>
-->

<?php
include 'shared.php';
?>

<html lang="en">

<head>

<?php
readfile('head.html');
readfile('style.html');
?>


</head>
<body>


<?php if ($_SESSION['auth'] == "1") {

readfile('navigation.html');

} else { 

readfile ('blankNavigation.html');

}  ?>


<!-- Page Content -->
<div class="container">


<br/><br/>

<?php
	if ($_SESSION['auth'] == "1" ) {
		//if there IS a valid login, show the dashboard page	
?>

<h1 class="my-4">Valid Login</h1>

<?php
	} else {
		//show the login page if there is no login
?>

<h1 class="my-4">PrivacyUX BRIEF Login</h1>
<p>Use this page to log in and edit your privacy facts interactive notice.</p>
<p id="pStatusMessage" style="color:red;"><?php echo $pStatusMessage; ?></p>

<div class="row">
	<!-- Left Panel -->
	<div class="col-sm-6 col-md-4">




	</div>

	<!-- Right Panel -->
	<div class="col-sm-6 col-md-4">




	</div>
</div>	
	
<?php	
	}
?>

</div>

<br/><br/>
	
<?php
readfile('footer.html');
readfile('modal.html');
?>
	
	
</body>
</html>