<!DOCTYPE html>
<?php
include 'shared.php';

//write analytics 
//analytics("PAGE LOAD - BUILD.PHP");
?>

<html lang="en">
<head>

<?php
readfile('head.html');
readfile('style.html');
?>

</head>
<body onload="doLoad();">

<?php 

readfile ('blankNavigation.html');
readfile ('footerUnderflow.html');

?>

<!-- Page Content -->
<div class="container">

<!-- Single Row-->
<div class="row">

	<!-- Left Panel -->
	<div id="divLeftPanel" class="col-sm-5 col-md-5">
	
	<div class="divSpacerTop" ></div>

		<!-- cell phone example -->
		<div id="phone" style="position:relative;display:none;">
			<div id="phoneSilhouette" class="phone"></div>
			<div id="phoneButtonLeft0" class="phone"></div>
			<div id="phoneButtonLeft1" class="phone"></div>	
			<div id="phoneButtonLeft2" class="phone"></div>	
			<div id="phoneButtonRight0" class="phone"></div>			
			<div id="phoneContent" class="phone"><iframe id="framedPFIN" src="privacyNotice.php" style="width:100%;height:100%; border:none;"></iframe></div>
		</div>
	
	</div>
	<!-- End Left Panel -->

	<!-- Right Panel -->
	<div id="divRightPanel" class="col-sm-7 col-md-7">
	<div class="divSpacerTop" ></div>	
	
	
<!-- BUILD TUNNEL -->
<form id="build0" class="" action="javascript:doUpdate('build0');" method="post" onsubmit="return(validateForm('build0'));">

	<!-- STEP BUTTON -->
	<button class="btn btn-privacyux">Step 
	<span class="btnStepBubble" style=""><span class="btnStepBubbleText">1</span></span>
	</button>
	
	<!-- PROGRESS METER -->
	<span class="progressMeter" style="">
		<div id="divProgress1" class="progress-bar btnText-privacyux progressMeterCreep">&nbsp;&nbsp;Progress</div>
	</span>
	
	<h1>Your&nbsp;Custom&nbsp;CCPA&nbsp;Notice</h1>
	<label for="organization">What is the name of your company?</label><br/>			
	<input id="organization" name="organization" type="text" class="form-control" placeholder="Company Name" required><br/>
	
	<label for="collectdata">Do you collect any personal data? Y/N</label>
	<br/>
	<input id="collectdatatrue" name="radiocollectdata" type="radio" value="true" class="form-radio-input btnBigRadio" onclick="$('#collectdata').val('true');" checked> &nbsp;<span for="collectdatatrue" >Yes</span> &nbsp;&nbsp;&nbsp;
	<input id="collectdatafalse" name="radiocollectdata" type="radio" value="false" class="form-radio-input btnBigRadio" onclick="$('#collectdata').val('false');" > &nbsp;<span for="collectdatafalse" >No</span>
	
	<input type="hidden" name="collectdata" id="collectdata" value="true"></input>
	
		
	<br/><br/>		

	<label for="privacypolicyurl">What is the URL of your company's full privacy policy?</label><br/>		
	<input id="privacypolicyurl" name="privacypolicyurl" type="text" class="form-control"  placeholder="https://www.company.com/privacy" required><br/>
</form>

<form id="build1" class="" style="display:none;" action="javascript:doUpdate('build1');" method="post" onsubmit="return(validateForm('build1'));">

	<!-- STEP BUTTON -->
	<button class="btn btn-privacyux">Step 
	<span class="btnStepBubble" style=""><span class="btnStepBubbleText">2</span></span>
	</button>
		
	<!-- PROGRESS METER -->
	<span class="progressMeter" style="">
		<div id="divProgress2" class="progress-bar btnText-privacyux progressMeterCreep">&nbsp;&nbsp;Progress</div>
	</span>
	
	<h1>Your&nbsp;Custom&nbsp;CCPA&nbsp;Notice</h1>
			<label for="orgfunction">What does your company do? (in a sentence)</label><br/>			
			<input id="orgfunction" name="orgfunction" type="text" class="form-control" placeholder="We create widgets." required><br/>

			<label for="orglocation">What is your company's address?</label><br/>		
			<input id="orglocation" name="orglocation" type="text" class="form-control"  placeholder="123 Elm St. Anytown US 00000" required><br/>

			<label for="orgphone">What is your customer support phone number? (optional)</label><br/>	
			<input id="orgphone" name="orgphone" type="text" class="form-control"  placeholder="800.123.3333"><br/>

			<label for="orgemail">What is the email address for privacy concerns? (optional)</label><br/>	
			<input id="orgemail" name="orgemail" type="text" class="form-control"  placeholder="privacy@company.com" required><br/>

			<label for="orgurl">What is the URL to your website?</label><br/>	
			<input id="orgurl" name="orgurl" type="text" class="form-control"  placeholder="http://www.company.com" required><br/>
			
</form>

<form id="build2" class="" style="display:none;"   action="javascript:doUpdate('build2');" method="post" onsubmit="return(validateForm('build2'));">

	<!-- STEP BUTTON -->
	<button class="btn btn-privacyux">Step 
	<span class="btnStepBubble" style=""><span class="btnStepBubbleText">3</span></span>
	</button>
	
	<!-- PROGRESS METER -->
	<span class="progressMeter" style="">
		<div id="divProgress3" class="progress-bar btnText-privacyux progressMeterCreep">&nbsp;&nbsp;Progress</div>
	</span>

	<h1>Your&nbsp;Custom&nbsp;CCPA&nbsp;Notice</h1>
	
	<table><tr><td class="checkboxSpacer"></td>
	<td>
			<label for="">What categories of personal data does your company collect?</label><br/>			
			<input type="checkbox" class="form-check-input inpBigCheck" onchange=" $('#catidentifiers').val(this.checked);"> &nbsp;<span for="catidentifiers">Identifiers</span><br/>
			
			<input type="checkbox" class="form-check-input inpBigCheck" onchange=" $('#catcustomerrecords').val(this.checked);"> &nbsp;<span for="catcustomerrecords">Information in Customer Records</span><br/>
			
			<input type="checkbox" class="form-check-input inpBigCheck" onchange=" $('#catlegallyprotected').val(this.checked);"> &nbsp;<span for="catlegallyprotected">Legally Protected Characteristics</span><br/>
			
			<input type="checkbox" class="form-check-input inpBigCheck" onchange=" $('#catcommercialpurchasing').val(this.checked);"> &nbsp;<span for="catcommercialpurchasing">Commercial Purchasing Information</span><br/>
			
			<input type="checkbox" class="form-check-input inpBigCheck" onchange=" $('#catbiometric').val(this.checked);"> &nbsp;<span for="catbiometric">Biometric Information</span><br/>
			
			<input type="checkbox" class="form-check-input inpBigCheck" onchange=" $('#catnetworkactivity').val(this.checked);"> &nbsp;<span for="catnetworkactivity">Internet or Network Activity</span><br/>
			
			<input type="checkbox" class="form-check-input inpBigCheck" onchange=" $('#catgeolocation').val(this.checked);"> &nbsp;<span for="catgeolocation">Geolocation</span><br/>
			
			<input type="checkbox" class="form-check-input inpBigCheck" onchange=" $('#catsenses').val(this.checked);"> &nbsp;<span for="catsenses">Information Typically Detected by the Senses</span><br/>
			
			<input type="checkbox" class="form-check-input inpBigCheck" onchange=" $('#catemployment').val(this.checked);"> &nbsp;<span for="catemployment">Employment Information</span><br/>
			
			<input type="checkbox" class="form-check-input inpBigCheck" onchange=" $('#cateducation').val(this.checked);"> &nbsp;<span for="cateducation">Education Information</span><br/>
			
			<input type="checkbox" class="form-check-input inpBigCheck" onchange=" $('#catinferences').val(this.checked);"> &nbsp;<span for="catinferences">Data Inferences Used to Profile</span><br/>
	</td></tr></table>
	
			<input type="hidden" id="catidentifiers" name="catidentifiers" value="false"></input>
			<input type="hidden" id="catcustomerrecords" name="catcustomerrecords" value="false"></input>
			<input type="hidden" id="catlegallyprotected" name="catlegallyprotected" value="false"></input>
			<input type="hidden" id="catcommercialpurchasing" name="catcommercialpurchasing" value="false"></input>
			<input type="hidden" id="catbiometric" name="catbiometric" value="false"></input>
			<input type="hidden" id="catnetworkactivity" name="catnetworkactivity" value="false"></input>
			<input type="hidden" id="catgeolocation" name="catgeolocation" value="false"></input>
			<input type="hidden" id="catsenses" name="catsenses" value="false"></input>
			<input type="hidden" id="catemployment" name="catemployment" value="false"></input>
			<input type="hidden" id="cateducation" name="cateducation" value="false"></input>
			<input type="hidden" id="catinferences" name="catinferences" value="false"></input>
			<script>
			var arrCatChoices = ["catidentifiers","catcustomerrecords","catlegallyprotected","catcommercialpurchasing","catbiometric","catnetworkactivity","catgeolocation","catsenses","catemployment","cateducation","catinferences"];
			</script>	
	
</form>


<form id="build3" class=""  style="display:none;"  action="javascript:doUpdate('build3');" method="post" onsubmit="return(validateForm('build3'));">

	<!-- STEP BUTTON -->
	<button class="btn btn-privacyux">Step 
	<span class="btnStepBubble" style=""><span class="btnStepBubbleText">4</span></span>
	</button>	
	
	<!-- PROGRESS METER -->
	<span class="progressMeter" style="">
		<div id="divProgress4" class="progress-bar btnText-privacyux progressMeterCreep">&nbsp;&nbsp;Progress</div>
	</span>

	<h1>Your&nbsp;Custom&nbsp;CCPA&nbsp;Notice</h1>
			
			<label for="">What are the purposes for which your company collects this personal data?</label><br/>			
			<input type="checkbox" class="form-check-input inpBigCheck"  onchange=" $('#purbusiness').val(this.checked);"> &nbsp; <span for="purbusiness">For Business Purposes</span><br/>
			<input type="checkbox" class="form-check-input inpBigCheck" onchange=" $('#purcommercial').val(this.checked);"> &nbsp; <span for="purcommercial">For Commercial Purposes</span><br/>
			<input type="checkbox" class="form-check-input inpBigCheck" onchange=" $('#purconsumer').val(this.checked);"> &nbsp; <span for="purconsumer">For Fulfilling Consumers' Requests</span><br/>
			
			<input type="hidden" id="purbusiness" name="purbusiness" value="false"></input>
			<input type="hidden" id="purcommercial" name="purcommercial" value="false"></input>
			<input type="hidden" id="purconsumer" name="purconsumer" value="false"></input>			
			<script>
			var arrPurChoices = ["purbusiness","purcommercial","purconsumer"];
			</script>		
	
</form>

<form id="build4" class="" style="display:none;" action="javascript:doUpdate('build4');" method="post" onsubmit="return(validateForm('build4'));">

	<!-- STEP BUTTON -->
	<button class="btn btn-privacyux">Step 
	<span class="btnStepBubble" style=""><span class="btnStepBubbleText">5</span></span>
	</button>
	
	<!-- PROGRESS METER -->
	<span class="progressMeter" style="">
		<div id="divProgress5" class="progress-bar btnText-privacyux progressMeterCreep">&nbsp;&nbsp;Progress</div>
	</span>

	<h1>Your&nbsp;Custom&nbsp;CCPA&nbsp;Notice</h1>
			
<p>As you can see on the left, we've built a custom CCPA Notice At Collection using your inputs. This notice fully complies with CCPA, looks great no matter what device your users are using, and can easily be added at <b>every data ingestion point</b>, as required. </p>

<p>We'll email you a link to your fully functional custom notice and instructions on how to test it in all of the required CCPA scenarios. For 45 days, you can test and share with your colleagues. (Don't worry, PrivacyCheq only uses your email to communicate with you about this demonstration.)</p><br/>			

			<label for="emailcustomer">What is your email address?</label><br/>	
			<input id="emailcustomer" name="emailcustomer" type="text" class="form-control"  placeholder="youremail@yourorg.com" required><br/>

		
			<button type="submit" class="btn btn-privacyux">Deliver my CCPA Notice demonstration</button>		
</form>
<div id="build5" class="" style="display:none;">
	<button class="btn btn-privacyux" style="visible:false;">Complete</button>
	
	<!-- PROGRESS METER -->
	<span class="progressMeter" style="">
		<div id="divProgress5" class="progress-bar btnText-privacyux progressMeterCreep">&nbsp;&nbsp;Progress Complete</div>
	</span>

	<h1>Your&nbsp;Custom&nbsp;CCPA&nbsp;Notice</h1>


<p>You can access your notice at:<br/><span id="spnNoticeURL"></span></p>

<p>As you can see on the left, we've built a custom CCPA Notice At Collection using your inputs. This notice fully complies with CCPA, looks great no matter what device your users are using, and can easily be added at <b>every data ingestion point</b>, as required. </p>	
	
</div>	
	</div>
	<!-- End Right Panel -->	

</div>
<!-- End Row -->

</div> 
<!-- End Page Content -->
	

<br/><br/>
	
<?php
readfile ('bootstrapCore.html');
readfile('modal.html');
readfile ('loader.html');
?>
	
	
</body>

<script src="script/buildValidation.js"></script>
<script src="script/buildUpdate.js"></script>


<script>
//called when this page loads
function doLoad() {
	
	//hide the gradient background in the PFIN and resize it
	document.getElementById("framedPFIN").contentWindow.document.getElementsByTagName("body")[0].style.backgroundImage = "none";
	onResize();
	
}


//called when the PFIN resizes
function onResize() {
	
	var phoneWidth = 440;
	
	//console.log(window.innerHeight);
	var scaleRatio = window.innerHeight / 1000;
	var maxWidth = document.getElementById("divLeftPanel").offsetWidth;	
	
	//do I scale based on height or width?
	if ((phoneWidth * scaleRatio) >= maxWidth) {
		scaleRatio = maxWidth/phoneWidth;
	}	
	
	//shuffle the phone to the left a little bit
	var scootchLeftOnResize = (phoneWidth/2) * (scaleRatio - 1); 
	console.log(scootchLeftOnResize);
	

	document.getElementById("phone").style.transform 	= "scale(" + scaleRatio + ")";
    document.getElementById("phone").style.left 		=  scootchLeftOnResize + "px";
	document.getElementById("phone").style.display		="block";	

	
}

//set the window's resize event
window.onresize = onResize;

</script>
<script src="utility.js"></script>
</html>













