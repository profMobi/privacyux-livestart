<!DOCTYPE html>
<?php
include 'shared.php';

//write analytics 
//analytics("PAGE LOAD - BUILD.PHP");
?>

<html lang="en">
<head>

<?php
readfile('head.html');
readfile('style.html');
?>

<!--
DEBUG: 
-->
<script>
<?php include 'buildProcessor.php'; ?>
</script>

</head>
<body onload="doLoad();">

<?php 

readfile ('blankNavigation.html');
readfile ('footerUnderflow.html');

?>

<!-- Page Content -->
<div class="container">

<!-- Single Row-->
<div class="row">

	<!-- Left Panel -->
	<div id="divLeftPanel" class="col-sm-5 col-md-5">
	
	<div class="divSpacerTop" ></div>

		<!-- cell phone example -->
		<div id="phone" style="position:relative;display:none;">
			<div id="phoneSilhouette" class="phone"></div>
			<div id="phoneButtonLeft0" class="phone"></div>
			<div id="phoneButtonLeft1" class="phone"></div>	
			<div id="phoneButtonLeft2" class="phone"></div>	
			<div id="phoneButtonRight0" class="phone"></div>			
			<div id="phoneContent" class="phone"><iframe id="framedPFIN" src="privacyNotice.php" style="width:100%;height:100%; border:none;"></iframe></div>
		</div>
	
	</div>
	<!-- End Left Panel -->

	<!-- Right Panel -->
	<div id="divRightPanel" class="col-sm-7 col-md-7">
	<div class="divSpacerTop" ></div>	
		
		<!-- BUILD TUNNEL -->

		<!-- STEP BUTTON -->
		<button class="btn btn-privacyux">Step 
		<span class="btnStepBubble" style=""><span class="btnStepBubbleText">3</span></span>
		</button>
		
		<!-- PROGRESS METER -->
		<span class="progressMeter" style="">
			<div id="divProgress3" class="progress-bar btnText-privacyux progressMeterCreep">&nbsp;&nbsp;Progress</div>
		</span>

		<h1>Let’s&nbsp;Build&nbsp;Your Custom CCPA Notice&nbsp;at&nbsp;Collection</h1>
			
		<form id="build2"  action="javascript:doUpdate('build2');" method="post" onsubmit="return(validateForm('build2'));">
			
			<table><tr><td class="checkboxSpacer"></td>
			<td>
					<label for="">What categories of personal data does your company collect?</label> <sup><i onclick="$(this).tooltip('show');" class="fas fa-info-circle" data-toggle="tooltip" data-html="true" data-placement="top" title="The CCPA identifies these as the possible categories of personal data that might be collected. This law requires that organizations that do business in California share which of these categories of data are collected."></i></sup><br/>			
					<input type="checkbox" class="form-check-input inpBigCheck" onchange=" $('#catidentifiers').val(this.checked);"> &nbsp;<span for="catidentifiers" class="forcheck">Identifiers</span><br/>
					
					<input type="checkbox" class="form-check-input inpBigCheck" onchange=" $('#catcustomerrecords').val(this.checked);"> &nbsp;<span for="catcustomerrecords" class="forcheck">Information in Customer Records</span><br/>
					
					<input type="checkbox" class="form-check-input inpBigCheck" onchange=" $('#catlegallyprotected').val(this.checked);"> &nbsp;<span for="catlegallyprotected" class="forcheck">Legally Protected Characteristics</span><br/>
					
					<input type="checkbox" class="form-check-input inpBigCheck" onchange=" $('#catcommercialpurchasing').val(this.checked);"> &nbsp;<span for="catcommercialpurchasing" class="forcheck">Commercial Purchasing Information</span><br/>
					
					<input type="checkbox" class="form-check-input inpBigCheck" onchange=" $('#catbiometric').val(this.checked);"> &nbsp;<span for="catbiometric" class="forcheck">Biometric Information</span><br/>
					
					<input type="checkbox" class="form-check-input inpBigCheck" onchange=" $('#catnetworkactivity').val(this.checked);"> &nbsp;<span for="catnetworkactivity" class="forcheck">Internet or Network Activity</span><br/>
					
					<input type="checkbox" class="form-check-input inpBigCheck" onchange=" $('#catgeolocation').val(this.checked);"> &nbsp;<span for="catgeolocation" class="forcheck">Geolocation</span><br/>
					
					<input type="checkbox" class="form-check-input inpBigCheck" onchange=" $('#catsenses').val(this.checked);"> &nbsp;<span for="catsenses" class="forcheck">Information Typically Detected by the Senses</span><br/>
					
					<input type="checkbox" class="form-check-input inpBigCheck" onchange=" $('#catemployment').val(this.checked);"> &nbsp;<span for="catemployment" class="forcheck">Employment Information</span><br/>
					
					<input type="checkbox" class="form-check-input inpBigCheck" onchange=" $('#cateducation').val(this.checked);"> &nbsp;<span for="cateducation" class="forcheck">Education Information</span><br/>
					
					<input type="checkbox" class="form-check-input inpBigCheck" onchange=" $('#catinferences').val(this.checked);"> &nbsp;<span for="catinferences" class="forcheck">Data Inferences Used to Profile</span><br/>
			</td></tr></table>
			
					<input type="hidden" id="catidentifiers" name="catidentifiers" value="false"></input>
					<input type="hidden" id="catcustomerrecords" name="catcustomerrecords" value="false"></input>
					<input type="hidden" id="catlegallyprotected" name="catlegallyprotected" value="false"></input>
					<input type="hidden" id="catcommercialpurchasing" name="catcommercialpurchasing" value="false"></input>
					<input type="hidden" id="catbiometric" name="catbiometric" value="false"></input>
					<input type="hidden" id="catnetworkactivity" name="catnetworkactivity" value="false"></input>
					<input type="hidden" id="catgeolocation" name="catgeolocation" value="false"></input>
					<input type="hidden" id="catsenses" name="catsenses" value="false"></input>
					<input type="hidden" id="catemployment" name="catemployment" value="false"></input>
					<input type="hidden" id="cateducation" name="cateducation" value="false"></input>
					<input type="hidden" id="catinferences" name="catinferences" value="false"></input>
					<script>
					var arrCatChoices = ["catidentifiers","catcustomerrecords","catlegallyprotected","catcommercialpurchasing","catbiometric","catnetworkactivity","catgeolocation","catsenses","catemployment","cateducation","catinferences"];
					</script>
				<button id="nofooterButton" class="btn btn-privacyux" onclick="submitNext();">Next</button>			
			
		</form>

	</div>
	<!-- End Right Panel -->	

</div>
<!-- End Row -->

</div> 
<!-- End Page Content -->
	

<br/><br/>
	
<?php
readfile ('bootstrapCore.html');
readfile('modal.html');
readfile ('loader.html');
?>
	
	
</body>

<script src="script/buildValidation.js"></script>
<script src="script/buildUpdate.js"></script>


<script>
//called when this page loads
function doLoad() {

	document.getElementById("footerButton").innerHTML = "Move to Step " + 4 + " <span id='blackBubble'><span>&gt;</span></span>";	
	
	//hide the gradient background in the PFIN and resize it
	document.getElementById("framedPFIN").contentWindow.document.getElementsByTagName("body")[0].style.backgroundImage = "none";
	onResize();
	
}


//called when the PFIN resizes
function onResize() {
	
	var phoneWidth = 440;
	
	//console.log(window.innerHeight);
	var scaleRatio = window.innerHeight / 1000;
	var maxWidth = document.getElementById("divLeftPanel").offsetWidth;	
	
	//do I scale based on height or width?
	if ((phoneWidth * scaleRatio) >= maxWidth) {
		scaleRatio = maxWidth/phoneWidth;
	}	
	
	//shuffle the phone to the left a little bit
	var scootchLeftOnResize = (phoneWidth/2) * (scaleRatio - 1); 
	console.log(scootchLeftOnResize);
	

	document.getElementById("phone").style.transform 	= "scale(" + scaleRatio + ")";
    document.getElementById("phone").style.left 		=  scootchLeftOnResize + "px";
	document.getElementById("phone").style.display		="block";	

	
}

//set the window's resize event
window.onresize = onResize;

</script>
<script src="utility.js"></script>
</html>













