<?php
//This page is used to BUILD the privacy dialog


//Test to confirm that we have a valid login
session_start();
if (!isset($_SESSION['auth'])) {
	$_SESSION['auth'] = "0";
	$_SESSION['user'] = "";
	$_SESSION['level'] = "";
} else {
	
	//set the level of the login	
	if (isset($_SESSION['level'])) {
		$level = $_SESSION['level'];	
		//$userid = $_SESSION['user'];
	} else {
		$level = "";	
	}
}

//global variables
$pStatusMessage = "";


?><!DOCTYPE html>


<!--
<?php
echo "DEBUG BLOCK:" . $userid . PHP_EOL;
?>
-->

<?php
include 'shared.php';

//write analytics 
analytics("PAGE LOAD - BUILD.PHP");
?>

<html lang="en">
<head>

<?php
readfile('head.html');
readfile('style.html');
?>

</head>
<body onload="doLoad();">

<?php 

readfile ('blankNavigation.html');
readfile ('footerUnderflow.html');

?>

<!-- Page Content -->
<div class="container">

<!-- Single Row-->
<div class="row">

	<!-- Left Panel -->
	<div id="divLeftPanel" class="col-sm-5 col-md-5">
	
	<div class="divSpacerTop" ></div>

		<!-- cell phone example -->
		<div id="phone" style="position:relative;display:none;">
			<div id="phoneSilhouette" class="phone"></div>
			<div id="phoneButtonLeft0" class="phone"></div>
			<div id="phoneButtonLeft1" class="phone"></div>	
			<div id="phoneButtonLeft2" class="phone"></div>	
			<div id="phoneButtonRight0" class="phone"></div>			
			<div id="phoneContent" class="phone"><iframe id="framedPFIN" src="privacyNotice.php" style="width:100%;height:100%; border:none;"></iframe></div>
		</div>
	
	</div>
	<!-- End Left Panel -->

	<!-- Right Panel -->
	<div id="divRightPanel" class="col-sm-7 col-md-7">
	<div class="divSpacerTop" ></div>	
	
	
<!-- BUILD TUNNEL -->
<form id="build0" class="" action="javascript:doUpdate('build0');" method="post" onsubmit="return(validateForm('build0'));">

	<!-- STEP BUTTON -->
	<button class="btn btn-privacyux">Step 
	<span class="btnStepBubble" style=""><span class="btnStepBubbleText">1</span></span>
	</button>
	
	<!-- PROGRESS METER -->
	<span class="progressMeter" style="">
		<div id="divProgress1" class="progress-bar btnText-privacyux progressMeterCreep">&nbsp;&nbsp;Progress</div>
	</span>
	
	<h1>Your&nbsp;Custom&nbsp;CCPA&nbsp;Notice</h1>
	<label for="organization">What is the name of your company?</label><br/>			
	<input id="organization" name="organization" type="text" class="form-control" placeholder="Company Name" required><br/>
	
	<label for="collectdata">Do you collect any personal data? Y/N</label>
	<br/>
	<input id="collectdatatrue" name="collectdata" type="radio" value="true" class="form-radio-input btnBigRadio" required></input> &nbsp;<span for="collectdata">Yes</span> &nbsp;&nbsp;&nbsp;
	<input id="collectdatafalse" name="collectdata" type="radio" value="false" class="form-radio-input btnBigRadio" required></input> &nbsp;<span for="collectdata">No</span>
	
		
	<br/><br/>		

	<label for="privacypolicyurl">What is the URL of your company's full privacy policy?</label><br/>		
	<input id="privacypolicyurl" name="privacypolicyurl" type="text" class="form-control"  placeholder="https://www.company.com/privacy" required><br/>
</form>

<form id="build1" class="" style="display:none;" action="javascript:doUpdate('build1');" method="post" onsubmit="return(validateForm('build1'));">

	<!-- STEP BUTTON -->
	<button class="btn btn-privacyux">Step 
	<span class="btnStepBubble" style=""><span class="btnStepBubbleText">2</span></span>
	</button>
		
	<!-- PROGRESS METER -->
	<span class="progressMeter" style="">
		<div id="divProgress2" class="progress-bar btnText-privacyux progressMeterCreep">&nbsp;&nbsp;Progress</div>
	</span>
	
	<h1>Your&nbsp;Custom&nbsp;CCPA&nbsp;Notice</h1>
			<label for="orgfunction">What does your company do? (in a sentence)</label><br/>			
			<input id="orgfunction" name="orgfunction" type="text" class="form-control" placeholder="We create widgets." required><br/>

			<label for="orglocation">What is your company's address?</label><br/>		
			<input id="orglocation" name="orglocation" type="text" class="form-control"  placeholder="123 Elm St. Anytown US 00000" required><br/>

			<label for="orgphone">What is your customer support phone number? (optional)</label><br/>	
			<input id="orgphone" name="orgphone" type="text" class="form-control"  placeholder="800.123.3333"><br/>

			<label for="orgemail">What is the email address for privacy concerns? (optional)</label><br/>	
			<input id="orgemail" name="orgemail" type="text" class="form-control"  placeholder="privacy@company.com" required><br/>

			<label for="orgurl">What is the URL to your website?</label><br/>	
			<input id="orgurl" name="orgurl" type="text" class="form-control"  placeholder="http://www.company.com" required><br/>
</form>

<form id="build2" class="" style="display:none;"   action="javascript:doUpdate('build2');" method="post" onsubmit="return(validateForm('build2'));">

	<!-- STEP BUTTON -->
	<button class="btn btn-privacyux">Step 
	<span class="btnStepBubble" style=""><span class="btnStepBubbleText">3</span></span>
	</button>
	
	<!-- PROGRESS METER -->
	<span class="progressMeter" style="">
		<div id="divProgress3" class="progress-bar btnText-privacyux progressMeterCreep">&nbsp;&nbsp;Progress</div>
	</span>

	<h1>Your&nbsp;Custom&nbsp;CCPA&nbsp;Notice</h1>
	
	<table><tr><td class="checkboxSpacer"></td>
	<td>
			<label for="">What categories of personal data does your company collect?</label><br/>			
			<input id="catidentifiers" name="catidentifiers" type="checkbox" class="form-check-input inpBigCheck"> &nbsp;<span for="catidentifiers">Identifiers</span><br/>
			
			<input id="catcustomerrecords" name="catcustomerrecords" type="checkbox" class="form-check-input inpBigCheck"> &nbsp;<span for="catcustomerrecords">Information in Customer Records</span><br/>
			
			<input id="catlegallyprotected" name="catlegallyprotected" type="checkbox" class="form-check-input inpBigCheck"> &nbsp;<span for="catlegallyprotected">Legally Protected Characteristics</span><br/>
			
			<input id="catcommercialpurchasing" name="catcommercialpurchasing" type="checkbox" class="form-check-input inpBigCheck"> &nbsp;<span for="catcommercialpurchasing">Commercial Purchasing Information</span><br/>
			
			<input id="catbiometric" name="catbiometric" type="checkbox" class="form-check-input  inpBigCheck"> &nbsp;<span for="catbiometric">Biometric Information</span><br/>
			
			<input id="catnetworkactivity" name="catnetworkactivity" type="checkbox" class="form-check-input inpBigCheck"> &nbsp;<span for="catnetworkactivity">Internet or Network Activity</span><br/>
			
			<input id="catgeolocation" name="catgeolocation" type="checkbox" class="form-check-input inpBigCheck"> &nbsp;<span for="catgeolocation">Geolocation</span><br/>
			
			<input id="catsenses" name="catsenses" type="checkbox" class="form-check-input inpBigCheck"> &nbsp;<span for="catsenses">Information Typically Detected by the Senses</span><br/>
			
			<input id="catemployment" name="catemployment" type="checkbox" class="form-check-input inpBigCheck"> &nbsp;<span for="catemployment">Employment Information</span><br/>
			
			<input id="cateducation" name="cateducation" type="checkbox" class="form-check-input inpBigCheck"> &nbsp;<span for="cateducation">Education Information</span><br/>
			
			<input id="catinferences" name="catinferences" type="checkbox" class="form-check-input inpBigCheck"> &nbsp;<span for="catinferences">Data Inferences Used to Profile</span><br/>
	</td></tr></table>
</form>


<form id="build3" class=""  style="display:none;"  action="javascript:doUpdate('build3');" method="post" onsubmit="return(validateForm('build3'));">

	<!-- STEP BUTTON -->
	<button class="btn btn-privacyux">Step 
	<span class="btnStepBubble" style=""><span class="btnStepBubbleText">4</span></span>
	</button>	
	
	<!-- PROGRESS METER -->
	<span class="progressMeter" style="">
		<div id="divProgress4" class="progress-bar btnText-privacyux progressMeterCreep">&nbsp;&nbsp;Progress</div>
	</span>

	<h1>Your&nbsp;Custom&nbsp;CCPA&nbsp;Notice</h1>
			
			<label for="">What are the purposes for which your company collects this personal data?</label><br/>			
			<input id="purbusiness" name="purbusiness" type="checkbox" class="form-check-input inpBigCheck"> &nbsp; <span for="purbusiness">For Business Purposes</span><br/>
			<input id="purcommercial" name="purcommercial" type="checkbox" class="form-check-input inpBigCheck"> &nbsp; <span for="purcommercial">For Commercial Purposes</span><br/>
			<input id="purconsumer" name="purconsumer" type="checkbox" class="form-check-input inpBigCheck"> &nbsp; <span for="purconsumer">For Fulfilling Consumers' Requests</span><br/>
	
	
</form>

<form id="build4" class="" style="display:none;" action="javascript:doUpdate('build4');" method="post" onsubmit="return(validateForm('build4'));">

	<!-- STEP BUTTON -->
	<button class="btn btn-privacyux">Step 
	<span class="btnStepBubble" style=""><span class="btnStepBubbleText">5</span></span>
	</button>
	
	<!-- PROGRESS METER -->
	<span class="progressMeter" style="">
		<div id="divProgress5" class="progress-bar btnText-privacyux progressMeterCreep">&nbsp;&nbsp;Progress</div>
	</span>

	<h1>Your&nbsp;Custom&nbsp;CCPA&nbsp;Notice</h1>
			
<p>As you can see on the left, we've built a custom CCPA Notice At Collection using your inputs. This notice fully complies with CCPA, looks great no matter what device your users are using, and can easily be added at <b>every data ingestion point</b>, as required. </p>

<p>We'll email you a link to your fully functional custom notice and instructions on how to test it in all of the required CCPA scenarios. For 45 days, you can test and share with your colleagues. (Don't worry, PrivacyCheq only uses your email to communicate with you about this demonstration.)</p><br/>			

			<label for="emailcustomer">What is your email address?</label><br/>	
			<input id="emailcustomer" name="emailcustomer" type="text" class="form-control"  placeholder="youremail@yourorg.com" required><br/>

		
			<button type="submit" class="btn btn-privacyux">Deliver my CCPA Notice demonstration</button>		
</form>
	
	</div>
	<!-- End Right Panel -->	

</div>
<!-- End Row -->

</div> 
<!-- End Page Content -->
	

<br/><br/>
	
<?php
readfile ('bootstrapCore.html');
readfile('modal.html');
?>
	
	
</body>

<script>
	
//validate the form
function validateForm(strForm) {
	
	//identify the command
	var cmd = strForm;

	//pull the form fields into an object
	objData = {}
	objForm = $("#" + strForm).find(':input');
	for (var x=0;x<objForm.length;x++) {

		//get the checked value of checkboxes
		if (objForm[x].className == "form-check-input") {
			objData[objForm[x].name] = objForm[x].checked;			
		}

		//get the selected value from radio buttons 
		else if (objForm[x].className == "form-radio-input") {
			if (objForm[x].checked) {
				objData[objForm[x].name] = objForm[x].value;
			}
				
		} else {
			//DEFAULT - get the value of inputs
			objData[objForm[x].name] = objForm[x].value;
		}
		
	}	

	//return false if validation fails
	boolReturn = false;
	
	//validate the input
	if (cmd == "build0") {
		
		if (objData.organization == "") {
			modalShow("No Organization","Please enter the name of your organization","OK");
			return false;
		}
		else if (objData.collectdata == "") {
			modalShow("No Data Choice","Please indicate whether you collect personal information or not","OK");
			return false;			
		}

		boolReturn = true;
	}
	else if (cmd == "build1") {
		//validate the organization information 
		if (objData.orgfunction == "") {
			modalShow("No Description","Please describe in under 200 characters what your organizaiton does","OK");
			return false;
		}

		else if (objData.orgfunction.slice(-1) != "." && objData.orgfunction.slice(-1) != "!" && objData.orgfunction.slice(-1) != "?" && objData.orgfunction.slice(-1) != " ") {
			modalShow("Use Complete Sentences","Please use complete sentences to describe what your organization does","OK");
			return false;			
		}

		else if (objData.orglocation == "") {
			modalShow("No Organization Location","Please give an address for your organization's headquarters","OK");
			return false;			
		}
		else if (objData.orgemail == "") {
			modalShow("No DPO Email","Please provide an email address for your chief data privacy officer","OK");
			return false;			
		}
		else if (!emailIsValid(objData.orgemail)) {
			modalShow("Invalid Email Syntax","Please provide a valid email address","OK");
			return false;			
		}
		else if (objData.orgurl == "") {
			modalShow("No Website URL","Please provide the web address of your organizatin's corporate website","OK");
			return false;			
		}

		boolReturn = true;
		
	}

	else if (cmd == "build2") {
		//validate the categories of data collected information

		//there must be at least one true
		boolReturn = false;
		for (var x=0;x<arrCatChoices.length;x++)  {

			if (objData[arrCatChoices[x]] == true) {

				boolReturn = true;
				x = arrCatChoices.length;			
			}
		} 

		if (boolReturn == false) {
			modalShow("No Categories Chosen","If you were incorrect earlier, and you actually do not collect any personal data, please reload the page to start over.","OK");
		}
		
	}

	else if (cmd == "build3") {
		//validate the purposes of data collected information

		//there must be at least one true
		boolReturn = false;
		for (var x=0;x<arrPurChoices.length;x++)  {

			if (objData[arrPurChoices[x]] == true) {

				boolReturn = true;
				x = arrPurChoices.length;			
			}
		} 

		if (boolReturn == false) {
			modalShow("No Purposes Chosen","If you were incorrect earlier, and you actually do not collect any personal data, please reload the page to start over.","OK");
		}
		
	}
	else if (cmd == "build4") {

		//validate the user's email address
		if (objData.emailcustomer == "") {
			modalShow("No Email Entered","Please provide an email address to store your privacy notice","OK");
			return false;			
		}
		else if (!emailIsValid(objData.emailcustomer)) {
			modalShow("Invalid Email Syntax","Please provide a valid email address","OK");
			return false;			
		}


		boolReturn = true;
	}

	return boolReturn;
	
}


function doUpdate(strForm) {

	console.log("in doUpdate");

	//identify the command
	var cmd = strForm;

	//pull the form fields into an object
	objData = {}
	objForm = $("#" + strForm).find(':input');
	for (var x=0;x<objForm.length;x++) {
		
		//get the checked value of checkboxes
		if (objForm[x].className == "form-check-input") {
			objData[objForm[x].name] = objForm[x].checked;			
		}

		//get the selected value from radio buttons 
		else if (objForm[x].className == "form-radio-input") {
			if (objForm[x].checked) {
				objData[objForm[x].name] = objForm[x].value;
			}
				
		} else {
			//DEFAULT - get the value of inputs
			objData[objForm[x].name] = objForm[x].value;
		}	
		
	}

	//update the PFIN appropriately
	if (cmd == "build0") {

		//Add the general PFIN information

		//put the data in the JSON object
		d.elements[0].members[1].text = objData.organization;
		d.elements[1].members[1].text = objData.privacypolicyurl;
		d.elements[1].members[1].link = objData.privacypolicyurl;
		
		console.log(2222222);
		console.log(objData.collectdata);

		if (objData.collectdata == "true") {
			
			//change the text of the button to redirect to a step rather than the end
			$("#btnBuild1").html("Step 3");

			d.elements.push({
		      "order": "1",
		      "elementid": "CATEGORIES",
		      "questionid": "",
		      "members": [
		        {
		          "memberid": "",
		          "text": "Categories",
		          "format": "L",
		          "link": "",
		          "type": "fact"
		        },
		        {
		          "memberid": "",
		          "text": "of data we collect",
		          "format": "R",
		          "link": "",
		          "type": "info"
		        }
		      ]
		    });

		    d.elements.push(    {
		      "order": "2",
		      "elementid": "PURPOSES",
		      "questionid": "",
		      "members": [
		        {
		          "memberid": "",
		          "text": "Purposes",
		          "format": "L",
		          "link": "",
		          "type": "fact"
		        },
		        {
		          "memberid": "",
		          "text": "for how we use your data",
		          "format": "R",
		          "link": "",
		          "type": "info"
		        }
		      ]
		    });

		    //re-sort the array by the order
		    d.elements.sort(function(a,b){return a.order - b.order});


		}
		console.log(objData);

		//update the form by hiding the previous and showing the new one
		$("#build0").hide();
		$("#build1").show();

	}

	else if (cmd == "build1") {

		//Add the organization data information

		//grab the organization's name from the data object
		var orgName = d.elements[0].members[1].text;

		//set the hyperlink on
		d.elements[0].members[1].link = "ORG";

		//objData.orgfunction
		//objData.orglocation 
		//objData.orgemail 
		//objData.orglocation
		//objData.orgphone
		//objData.orgurl
		
		//!!! why is this URL not passed? Maybe I need to validate and force a URL to be passed?
		if (!objData.url) { objData.url = ""};

		jsonDetails = {
	      "eidlink": "ORG",
	      "head": "Our company name is " + orgName + ". " + objData.orgfunction,
	      "foot": orgName + " may change the provisions of this privacy notice at any time, and will always post the most up-to-date version on our website. Additional general information about us can be found on our corporate website.",
	      "detaillinktext": "Link to our corporate website",
	      "detaillinkurl": objData.url,
	      "factors": [
	        {
	          "text": "Notify us by e-mail",
	          "info": objData.orgemail ,
	          "factorlink": "",
	          "order": "0"
	        },
	        {
	          "text": "Notify us by post",
	          "info": objData.orglocation,
	          "factorlink": "",
	          "order": "1"
	        }
	      ]
    	}

		if (objData.orgphone != "") {
			jsonDetails.factors.push({
				"text":"Notify us by phone",
				"info": objData.orgphone,
				"factorlink":"",
				"order":"2"
			});
		}


		d.details.push(jsonDetails);


		//update the form by hiding the previous and showing the new one
		$("#build1").hide();

		console.log(1111111111111111111111111111);
		console.log(d.elements[1].members[1].text);
		//test to see if personal data is collected - if not, just skip to the end
		if (d.elements[1].members[0].text == "Categories") {
			$("#build2").show();
		} else {
			//change the data to change the title to "Privacy Facts"
			d.title = "Privacy Facts";
			$("#build4").show();
		}

	}

	else if (cmd == "build2") {

		//Add the categories of information data

		//add the link to the categories page
		d.elements[1].members[1].link = "CAT";
		
		var arrCategoryFactors = [];
		
		console.log(d);
		
		var iOrder = 0;

		//read the checkboxes and add factors to the data object
		if (objData.catidentifiers) {

			var objFactor = {
			  "text": "Identifiers",
			  "info": "Data that identifies you such as your name, address, or phone number.",
			  "factorlink": "",
			  "order": iOrder
			}
		
			arrCategoryFactors.push(objFactor);
			iOrder = iOrder+1;

		}
		if (objData.catcustomerrecords) {
			var objFactor = {
			  "text": "Information in Customer Records",
			  "info": "Records of your activity recorded by our organization.",
			  "factorlink": "",
			  "order": iOrder
			}
		
			arrCategoryFactors.push(objFactor);
			iOrder = iOrder+1;			
			
		}
		if (objData.catlegallyprotected) {
			var objFactor = {
			  "text": "Legally Protected Characteristics",
			  "info": "Under federal law, protected characteristics include race, color, national origin, religion, gender (including pregnancy), disability, age (if the employee is at least 40 years old), and citizenship status.",
			  "factorlink": "",
			  "order": iOrder
			}
		
			arrCategoryFactors.push(objFactor);
			iOrder = iOrder+1;			
			
		}
		if (objData.catcommercialpurchasing) {
			var objFactor = {
			  "text": "Commercial Purchasing Information",
			  "info": "Information related to doing business bewteen organizations.",
			  "factorlink": "",
			  "order": iOrder
			}
		
			arrCategoryFactors.push(objFactor);
			iOrder = iOrder+1;			
			
		}
		if (objData.catbiometric) {
			var objFactor = {
			  "text": "Biometric Information",
			  "info": "Your body's unique physical characteristics used to identify you such as fingerprints or facial recognition.",
			  "factorlink": "",
			  "order": iOrder
			}
		
			arrCategoryFactors.push(objFactor);
			iOrder = iOrder+1;			
			
		}
		if (objData.catnetworkactivity) {
			var objFactor = {
			  "text": "Internet or Network Activity",
			  "info": "A record of your online activity.",
			  "factorlink": "",
			  "order": iOrder
			}
		
			arrCategoryFactors.push(objFactor);
			iOrder = iOrder+1;			
			
		}
		if (objData.catgeolocation) {
			var objFactor = {
			  "text": "Geolocation",
			  "info": "Data that identifies or may be used to estimate your the real-world geographic location such as a radar source, mobile phone, or Internet-connected computer terminal.",
			  "factorlink": "",
			  "order": iOrder
			}
		
			arrCategoryFactors.push(objFactor);
			iOrder = iOrder+1;			
			
		}
		if (objData.catsenses) {
			var objFactor = {
			  "text": "Information Typically Detected by the Senses",
			  "info": "Any audio, electronic, visual, thermal, olfactory, or similar information that may be collected from you.",
			  "factorlink": "",
			  "order": iOrder
			}
		
			arrCategoryFactors.push(objFactor);
			iOrder = iOrder+1;			
			
		}
		if (objData.catemployment) {
			var objFactor = {
			  "text": "Employment Information",
			  "info": "Your work history or current state of employment.",
			  "factorlink": "",
			  "order": iOrder
			}
		
			arrCategoryFactors.push(objFactor);
			iOrder = iOrder+1;			
			
		}
		if (objData.cateducation) {
			var objFactor = {
			  "text": "Education Information",
			  "info": "Information about where and when you were educated, what you studied, or what marks and degrees were attained.",
			  "factorlink": "",
			  "order": iOrder
			}
		
			arrCategoryFactors.push(objFactor);
			iOrder = iOrder+1;			
			
		}
		if (objData.catinferences) {
			var objFactor = {
			  "text": "Data Inferences Used to Profile",
			  "info": "Any data that may be used in combination with other freely available information that may be used to make an educated guess about other personal information.",
			  "factorlink": "",
			  "order": iOrder
			}
		
			arrCategoryFactors.push(objFactor);
			iOrder = iOrder+1;			
			
		}
	
		var jsonObjectCategories =     {
		  "eidlink": "CAT",
		  "head": "These are the categories of data " + d.elements[0].members[1].text + " collects.",
		  "foot": "",
		  "detaillinktext": "",
		  "detaillinkurl": "",
		  "factors": []
		}
		
		jsonObjectCategories.factors = arrCategoryFactors;
				
		d.details.push(jsonObjectCategories);

		//update the form by hiding the previous and showing the new one
		$("#build2").hide();
		$("#build3").show();
	}

	else if (cmd == "build3") {  
	
		//Add the purposes of information data

		// Information about the CCPA purposes
		//https://medium.com/golden-data/purposes-for-processing-under-ccpa-3e329ddd218

		//add the link to the categories page
		d.elements[2].members[1].link = "PUR";
		
		var arrPurposeFactors = [];
		
		var iOrder = 0;
		
		///"purbusiness","purcommercial", "purconsumer"

		//read the checkboxes and add factors to the data object
		if (objData.purbusiness) {

			var objFactor = {
			  "text": "For Business Purposes",
			  "info": "We process your personal data for operational purposes. The use of personal information in this way is necessary and proportionate to achieve the purposes for which the personal information is collected. Examples of business purposes for processing personal data are auditing, security, debugging, short-term transient use, performing services on behalf of a business or service provider, research, and maintaining the quality or safety of a product or service.",
			  "factorlink": "",
			  "order": iOrder
			}
		
			arrPurposeFactors.push(objFactor);
			iOrder = iOrder+1;

		}
		if (objData.purcommercial) {

			var objFactor = {
			  "text": "For Commercial Purposes",
			  "info": "We process your personal data for commercial purposes. For example to advanceyour commercial or economic interests, such as by inducing another person to buy, rent, lease, join, subscribe to, provide, or exchange products, goods, property, information, or services, or enabling or effecting, directly or indirectly, a commercial transaction. ",
			  "factorlink": "",
			  "order": iOrder
			}
		
			arrPurposeFactors.push(objFactor);
			iOrder = iOrder+1;

		}
		if (objData.purconsumer) {

			var objFactor = {
			  "text": "For Fulfilling Consumers' Requests",
			  "info": "If you request that we process your personal data, we will do so with certain limitations. You must act intentionally to give your consent for the processing of your data or on behalf of of a child and such consent for data processing doesn't necessarily extend to downstream processing.",
			  "factorlink": "",
			  "order": iOrder
			}
		
			arrPurposeFactors.push(objFactor);
			iOrder = iOrder+1;

		}

		var jsonObjectPurposes =     {
		  "eidlink": "PUR",
		  "head": "These are the purposes for which " + d.elements[0].members[1].text + " uses your data.",
		  "foot": "",
		  "detaillinktext": "",
		  "detaillinkurl": "",
		  "factors": []
		}
		
		jsonObjectPurposes.factors = arrPurposeFactors;
		d.details.push(jsonObjectPurposes);

		//update the form by hiding the previous and showing the new one
		$("#build3").hide();
		$("#build4").show();

		
	}

	else if (cmd == "build4") {

		//get the user id 
		var newUserID = "";

		//Get the email address and send an invitation to create an account
		var objPostData = {};	
		objPostData["email"] = objData.emailcustomer;
		objPostData["cmd"] = "newuser";


		//create a new user based on the email, returning a user id
		$.post("tools.php",objPostData,function(data) {

			//turn the string data returned back into a JSON object
			var objReturnData = JSON.parse(data);

			if (objReturnData.rtn != "ok") {

				//error or failure creating the user
				modalShow("Error creating new account: " + objReturnData.rtnmsg,"Try again later","OK",function(){

					//!!!! WHAT SHOULD WE DO IN CASE OF A FAILURE OR ERROR? do nothing?

				});					


			} else {

				//created the new account successfully
				var newUserID = objReturnData.id;

				//create the post data object to send to the tools
				var objPostData = {};	
				objPostData["userid"] 		= objReturnData.id;  
				objPostData["email"] 		= objReturnData.email;
				objPostData["noticejson"] 	= JSON.stringify(d); 
				objPostData["cmd"] 			= "createnoticejson";

				//trigger create the notice by passing the JSON
				$.post("tools.php",objPostData,function(data) {			

					//turn the string data returned back into a JSON object
					var objReturnData = JSON.parse(data);

					if (objReturnData.rtn != "ok") {

						//error or failure saving the JSON to database
						modalShow("Error saving JSON to database: " + objReturnData.rtnmsg,"Try again later","OK",function(){

							//!!!! WHAT SHOULD WE DO IN CASE OF A FAILURE OR ERROR? do nothing?

						});	
					} else {

						//still looking good, now send the email

						//create the post data object to send to the tools
						var objPostData = {};	
						objPostData["email"] = objReturnData.email;
						objPostData["noticeid"] = objReturnData.noticeid;
						objPostData["userid"] = objReturnData.userid;
						objPostData["noticename"] = objReturnData.name;
						objPostData["cmd"] = "sendinviteemail";

						//trigger the email to the customer
						$.post("tools.php",objPostData,function(data) {		

							//turn the string data returned back into a JSON object
							var objReturnData = JSON.parse(data);

							if (objReturnData.rtn != "ok") {

								//error or failure saving the JSON to database
								modalShow("Error sending welcome email: " + objReturnData.rtnmsg,"Try again later","OK",function(){

									//!!!! WHAT SHOULD WE DO IN CASE OF A FAILURE OR ERROR? do nothing?

								});	
							} else {

								//all three calls worked, move on

								//display instructions on what to do next that sends the email with the new user id
								$("#build4").hide();
								$("#build5").show();
							}

						});	 //end of sendemail call

					}

				});	 //end of the createnoticejson call

			}
		
		}); //end of newuser call

	}

	//**** END OF THE BUILD CALLS HERE *****


	//update the frame
	document.getElementById("framedPFIN").contentWindow.location.reload();


}


//Maintain the data object on the device

var d = {
  "id": "0",
  "name": "PFIN Soft Creation",
  "title": "CCPA Notice at Collection",
  "icon": "",
  "intro": "Click <span style='color:#007bff;'>blue</span> facts for more detail.",
  "elements": [
    {
      "order": "0",
      "elementid": "",
      "questionid": "",
      "members": [
        {
          "memberid": "",
          "text": "Our company name",
          "format": "L",
          "link": "",
          "type": "fact"
        },
        {
          "memberid": "",
          "text": "Your organization's name",
          "format": "R",
          "link": "",
          "type": "info"
        }
      ]
    },
    {
      "order": "3",
      "elementid": "",
      "questionid": "",
      "members": [
        {
          "memberid": "",
          "text": "Full privacy details",
          "format": "L",
          "link": "",
          "type": "fact"
        },
        {
          "memberid": "",
          "text": "our privacy policy",
          "format": "R",
          "link": "",
          "type": "info"
        }
      ]
    }
  ],
  "details": []
};


function storeData() {
	if (typeof (Storage) !== "undefined") {
	    localStorage.setItem('PFINData', d);
	}
	else {
	    console.log("This browser does not support web storage");
	}

}

function getStoredData() {
	if (typeof (Storage) !== "undefined") {

	    d = localStorage.getItem('PFINData');

	    console.log(typeof (d));
	}
	else {
	    console.log("This browser does not support web storage");
	}

}

function emailIsValid (email) {
	return /\S+@\S+\.\S+/.test(email)
}

function goBack(iStep) {
	
	//todo: ROLL BACK CHANGES THAT ARE MADE!!1!
	
	$("#build0").hide();	
	$("#build1").hide();	
	$("#build2").hide();	
	$("#build3").hide();	
	$("#build4").hide();
	$("#build" + iStep).show();
	
	return false;
}
</script>


<script>
function doLoad() {
	
	//hide the gradient background
	document.getElementById("framedPFIN").contentWindow.document.getElementsByTagName("body")[0].style.backgroundImage = "none";
	onResize();
	
}


//when the PFIN resizes
function onResize() {
	
	var phoneWidth = 440;
	
	//console.log(window.innerHeight);
	var scaleRatio = window.innerHeight / 1000;
	var maxWidth = document.getElementById("divLeftPanel").offsetWidth;	
	
	//do I scale based on height or width?
	if ((phoneWidth * scaleRatio) >= maxWidth) {
		scaleRatio = maxWidth/phoneWidth;
	}	
	
	//shuffle the phone to the left a little bit
	var scootchLeftOnResize = (phoneWidth/2) * (scaleRatio - 1); 
	console.log(scootchLeftOnResize);
	

	document.getElementById("phone").style.transform 	= "scale(" + scaleRatio + ")";
    document.getElementById("phone").style.left 		=  scootchLeftOnResize + "px";
	document.getElementById("phone").style.display		="block";	

	
}

//track the steps and move to the next
var iStep = 0;
function showNext() {

	var form = document.getElementById("build"+iStep);
	iStep = iStep +1;
	form.submit();

	//!!! Move this stuff into validation
	/*
	if (iStep >= 5) { 
		document.getElementById("footerButton").style.display = "none";
	
	} else {
		var strStep = iStep/1 +1;
		document.getElementById("footerButton").innerHTML = "Move to Step " + strStep + " <span id='blackBubble'><span>&gt;</span></span>";			
	}	
	*/

}

window.onresize = onResize;

</script>

</html>













