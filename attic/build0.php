<!DOCTYPE html>
<?php
include 'shared.php';

//write analytics 
analytics("PAGE LOAD - BUILD.PHP");
?>

<html lang="en">
<head>

<?php
readfile('head.html');
readfile('style.html');
?>

<!--
DEBUG: 
<?php


?>
-->

<script>
<?php include 'buildProcessor.php'; ?>
</script>

</head>
<body onload="doLoad();">

<?php 

readfile ('blankNavigation.html');
readfile ('footerUnderflow.html');

?>

<!-- Page Content -->
<div class="container">

<!-- Single Row-->
<div class="row">

	<!-- Left Panel -->
	<div id="divLeftPanel" class="col-sm-5 col-md-5">
	
	<div class="divSpacerTop" ></div>

		<!-- cell phone example -->
		<div id="phone" style="position:relative;display:none;">
			<div id="phoneSilhouette" class="phone"></div>
			<div id="phoneButtonLeft0" class="phone"></div>
			<div id="phoneButtonLeft1" class="phone"></div>	
			<div id="phoneButtonLeft2" class="phone"></div>	
			<div id="phoneButtonRight0" class="phone"></div>			
			<div id="phoneContent" class="phone"><iframe id="framedPFIN" src="privacyNotice.php" style="width:100%;height:100%; border:none;"></iframe></div>
		</div>
	
	</div>
	<!-- End Left Panel -->

	<!-- Right Panel -->
	<div id="divRightPanel" class="col-sm-7 col-md-7">
	<div class="divSpacerTop" ></div>	
	
		<!-- BUILD TUNNEL -->

		<!-- STEP BUTTON -->
		<button class="btn btn-privacyux">Step 
		<span class="btnStepBubble" style=""><span class="btnStepBubbleText">1</span></span>
		</button>
		
		<!-- PROGRESS METER -->
		<span class="progressMeter" style="">
			<div id="divProgress1" class="progress-bar btnText-privacyux progressMeterCreep">&nbsp;&nbsp;Progress</div>
		</span>
		
		<h1>Let’s&nbsp;Build&nbsp;Your Custom CCPA Notice&nbsp;at&nbsp;Collection</h1>
			
		<form id="build0" class="" action="javascript:doUpdate('build0');" method="post" onsubmit="return(validateForm('build0'));">
			
			
			<label for="organization">What is the name of your company?</label>
			<sup><i onclick="$(this).tooltip('show');" class="fas fa-info-circle" data-toggle="tooltip" data-html="true" data-placement="top" title="Enter the legal name of your company as it appears in your privacy policy."></i></sup><br/>	
			<input id="organization" name="organization" type="text" class="form-control" placeholder="Company Name" required><br/>
			
			<label><u><a href="https://oag.ca.gov/privacy/ccpa#ccpafaq" target="new">Here’s how CCPA defines personal information</a></u></label><br/>
			<label for="collectdata">Do you collect any personal data? Y/N</label>
			<sup><i onclick="$(this).tooltip('show');" class="fas fa-info-circle" data-toggle="tooltip" data-html="true" data-placement="top" title="This key fact determines how your notice will be structured. If your company collects personal information, you’ll need to disclose the details in the next screens."></i></sup>
			<br/>
			
			<input id="collectdatatrue" name="radiocollectdata" type="radio" value="true" class="form-radio-input btnBigRadio" onclick="$('#collectdata').val('true');" checked> &nbsp;<span for="collectdatatrue" >Yes</span> &nbsp;&nbsp;&nbsp;
			<input id="collectdatafalse" name="radiocollectdata" type="radio" value="false" class="form-radio-input btnBigRadio" onclick="$('#collectdata').val('false');" > &nbsp;<span for="collectdatafalse" >No</span>
			
			<input type="hidden" name="collectdata" id="collectdata" value="true"></input>		
			<br/><br/>		

			<label for="privacypolicyurl">What is the URL of your company's full privacy policy?</label> 
			<sup><i onclick="$(this).tooltip('show');" class="fas fa-info-circle" data-toggle="tooltip" data-html="true" data-placement="top" title="Enter the web address of your company’s legal  privacy policy."></i></sup><br/>		
			<input id="privacypolicyurl" name="privacypolicyurl" type="text" class="form-control"  placeholder="https://www.company.com/privacy" required><br/>
			
			<button id="nofooterButton" class="btn btn-privacyux" onclick="submitNext();">Next</button>
		</form>

	</div>
	<!-- End Right Panel -->	

</div>
<!-- End Row -->

</div> 
<!-- End Page Content -->
	

<br/><br/>
	
<?php
readfile ('bootstrapCore.html');
readfile ('modal.html');
readfile ('loader.html');
?>
	
	
</body>

<script src="script/buildValidation.js"></script>
<script src="script/buildUpdate.js"></script>


<script>
//called when this page loads
function doLoad() {
	
	document.getElementById("footerButton").innerHTML = "Move to Step " + 2 + " <span id='blackBubble'><span>&gt;</span></span>";		
	
	//hide the gradient background in the PFIN and resize it
	document.getElementById("framedPFIN").contentWindow.document.getElementsByTagName("body")[0].style.backgroundImage = "none";
	onResize();
	
}


//called when the PFIN resizes
function onResize() {
	
	var phoneWidth = 440;
	
	//console.log(window.innerHeight);
	var scaleRatio = window.innerHeight / 1000;
	var maxWidth = document.getElementById("divLeftPanel").offsetWidth;	
	
	//do I scale based on height or width?
	if ((phoneWidth * scaleRatio) >= maxWidth) {
		scaleRatio = maxWidth/phoneWidth;
	}	
	
	//shuffle the phone to the left a little bit
	var scootchLeftOnResize = (phoneWidth/2) * (scaleRatio - 1); 
	console.log(scootchLeftOnResize);

	document.getElementById("phone").style.transform 	= "scale(" + scaleRatio + ")";
    document.getElementById("phone").style.left 		=  scootchLeftOnResize + "px";
	document.getElementById("phone").style.display		="block";	

}

//set the window's resize event
window.onresize = onResize;

</script>
<script src="utility.js"></script>
</html>













