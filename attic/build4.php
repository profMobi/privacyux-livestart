<!DOCTYPE html>
<?php
include 'shared.php';

//write analytics 
//analytics("PAGE LOAD - BUILD.PHP");
?>

<html lang="en">
<head>

<?php
readfile('head.html');
readfile('style.html');
?>

<!--
DEBUG: 
-->
<script>
<?php include 'buildProcessor.php'; ?>
</script>

</head>
<body onload="doLoad();">

<?php 

readfile ('blankNavigation.html');
readfile ('footerUnderflow.html');

?>

<!-- Page Content -->
<div class="container">

<!-- Single Row-->
<div class="row">

	<!-- Left Panel -->
	<div id="divLeftPanel" class="col-sm-5 col-md-5">
	
	<div class="divSpacerTop" ></div>

		<!-- cell phone example -->
		<div id="phone" style="position:relative;display:none;">
			<div id="phoneSilhouette" class="phone"></div>
			<div id="phoneButtonLeft0" class="phone"></div>
			<div id="phoneButtonLeft1" class="phone"></div>	
			<div id="phoneButtonLeft2" class="phone"></div>	
			<div id="phoneButtonRight0" class="phone"></div>			
			<div id="phoneContent" class="phone"><iframe id="framedPFIN" src="privacyNotice.php" style="width:100%;height:100%; border:none;"></iframe></div>
		</div>
	
	</div>
	<!-- End Left Panel -->

	<!-- Right Panel -->
	<div id="divRightPanel" class="col-sm-7 col-md-7">
	<div class="divSpacerTop" ></div>	
	
		<!-- BUILD TUNNEL -->

		<!-- STEP BUTTON -->
		<button class="btn btn-privacyux">Step 
		<span class="btnStepBubble" style=""><span class="btnStepBubbleText">5</span></span>
		</button>
		
		<!-- PROGRESS METER -->
		<span class="progressMeter" style="">
			<div id="divProgress5" class="progress-bar btnText-privacyux progressMeterCreep">&nbsp;&nbsp;Progress</div>
		</span>

		<h1>Let’s&nbsp;Build&nbsp;Your Custom CCPA Notice&nbsp;at&nbsp;Collection</h1>
			
		<form id="build4" action="javascript:doUpdate('build4');" method="post" onsubmit="return(validateForm('build4'));">
						
		<p class="description">As you can see on the left, we've built a custom CCPA Notice at Collection using your inputs. This notice fully complies with CCPA, looks great no matter what device your consumers are using, and can easily be added wherever <a href="https://oag.ca.gov/privacy/ccpa#ccpafaq" target="new">personal information</a> is ingested.</p>

		<p class="description">We'll email you a link to your fully functional custom notice and instructions on how to test it in all of the required CCPA scenarios. For thirty (30) days, you can test and share with your colleagues. Don't worry, PrivacyCheq only uses your email to communicate with you about this demonstration.</p><br/>			

					<label for="emailcustomer">What is your email address?</label> <sup><i onclick="$(this).tooltip('show');"  class="fas fa-info-circle" data-toggle="tooltip" data-html="true" data-placement="top" title="PrivacyUX will use your email address to message you about your new privacy notice and to identify you when you maintain your notice in the future."></i></sup><br/>	
					<input id="emailcustomer" name="emailcustomer" type="text" class="form-control"  placeholder="youremail@yourorg.com" required><br/>

				
					<button type="submit" class="btn btn-privacyux">Deliver my CCPA Notice at Collection demonstration</button>		
		</form>

	</div>
	<!-- End Right Panel -->	

</div>
<!-- End Row -->

</div> 
<!-- End Page Content -->
	

<br/><br/>
	
<?php
readfile ('bootstrapCore.html');
readfile('modal.html');
readfile ('loader.html');
?>
	
	
</body>

<script src="script/buildValidation.js"></script>
<script src="script/buildUpdate.js"></script>


<script>
//called when this page loads
function doLoad() {

	//hide the button
	$("#footerButton").hide();

	//hide the gradient background in the PFIN and resize it
	document.getElementById("framedPFIN").contentWindow.document.getElementsByTagName("body")[0].style.backgroundImage = "none";
	onResize();
	
}


//called when the PFIN resizes
function onResize() {
	
	var phoneWidth = 440;
	
	//console.log(window.innerHeight);
	var scaleRatio = window.innerHeight / 1000;
	var maxWidth = document.getElementById("divLeftPanel").offsetWidth;	
	
	//do I scale based on height or width?
	if ((phoneWidth * scaleRatio) >= maxWidth) {
		scaleRatio = maxWidth/phoneWidth;
	}	
	
	//shuffle the phone to the left a little bit
	var scootchLeftOnResize = (phoneWidth/2) * (scaleRatio - 1); 
	console.log(scootchLeftOnResize);
	

	document.getElementById("phone").style.transform 	= "scale(" + scaleRatio + ")";
    document.getElementById("phone").style.left 		=  scootchLeftOnResize + "px";
	document.getElementById("phone").style.display		="block";	

	
}

//set the window's resize event
window.onresize = onResize;

</script>
<script src="utility.js"></script>
</html>













