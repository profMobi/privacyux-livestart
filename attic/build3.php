<!DOCTYPE html>
<?php
include 'shared.php';

//write analytics 
//analytics("PAGE LOAD - BUILD.PHP");
?>

<html lang="en">
<head>

<?php
readfile('head.html');
readfile('style.html');
?>

<!--
DEBUG: 
-->
<script>
<?php include 'buildProcessor.php'; ?>
</script>

</head>
<body onload="doLoad();">

<?php 

readfile ('blankNavigation.html');
readfile ('footerUnderflow.html');

?>

<!-- Page Content -->
<div class="container">

<!-- Single Row-->
<div class="row">

	<!-- Left Panel -->
	<div id="divLeftPanel" class="col-sm-5 col-md-5">
	
	<div class="divSpacerTop" ></div>

		<!-- cell phone example -->
		<div id="phone" style="position:relative;display:none;">
			<div id="phoneSilhouette" class="phone"></div>
			<div id="phoneButtonLeft0" class="phone"></div>
			<div id="phoneButtonLeft1" class="phone"></div>	
			<div id="phoneButtonLeft2" class="phone"></div>	
			<div id="phoneButtonRight0" class="phone"></div>			
			<div id="phoneContent" class="phone"><iframe id="framedPFIN" src="privacyNotice.php" style="width:100%;height:100%; border:none;"></iframe></div>
		</div>
	
	</div>
	<!-- End Left Panel -->

	<!-- Right Panel -->
	<div id="divRightPanel" class="col-sm-7 col-md-7">
	<div class="divSpacerTop" ></div>	
	
		<!-- BUILD TUNNEL -->

		<!-- STEP BUTTON -->
		<button class="btn btn-privacyux">Step 
		<span class="btnStepBubble" style=""><span class="btnStepBubbleText">4</span></span>
		</button>	
		
		<!-- PROGRESS METER -->
		<span class="progressMeter" style="">
			<div id="divProgress4" class="progress-bar btnText-privacyux progressMeterCreep">&nbsp;&nbsp;Progress</div>
		</span>

		<h1>Let’s&nbsp;Build&nbsp;Your Custom CCPA Notice&nbsp;at&nbsp;Collection</h1>
		
		<form id="build3" action="javascript:doUpdate('build3');" method="post" onsubmit="return(validateForm('build3'));">
			
			<label>This personal information is collected for which purpose(s)? <sup><i  onclick="$(this).tooltip('show');" class="fas fa-info-circle" style="color:black;" data-toggle="tooltip" data-html="true" data-placement="top" title="The CCPA identifies these as the possible purposes for collecting customers' personal data. This law requires that organizations that do business in California share which purposes personal data is collected for."></i></sup></label> <br/>			
			<input type="checkbox" class="form-check-input inpBigCheck"  onchange=" $('#purbusiness').val(this.checked);"> &nbsp; <span for="purbusiness">For Business Purposes</span><br/>
			<input type="checkbox" class="form-check-input inpBigCheck" onchange=" $('#purcommercial').val(this.checked);"> &nbsp; <span for="purcommercial">For Commercial Purposes</span><br/>
			<input type="checkbox" class="form-check-input inpBigCheck" onchange=" $('#purconsumer').val(this.checked);"> &nbsp; <span for="purconsumer">For Fulfilling Consumers' Requests</span><br/>
			
			<input type="hidden" id="purbusiness" name="purbusiness" value="false"></input>
			<input type="hidden" id="purcommercial" name="purcommercial" value="false"></input>
			<input type="hidden" id="purconsumer" name="purconsumer" value="false"></input>			
			<script>
			var arrPurChoices = ["purbusiness","purcommercial","purconsumer"];
			</script>		
			<button id="nofooterButton" class="btn btn-privacyux" onclick="submitNext();">Next</button>	
				
		</form>


	</div>
	<!-- End Right Panel -->	

</div>
<!-- End Row -->

</div> 
<!-- End Page Content -->
	

<br/><br/>
	
<?php
readfile ('bootstrapCore.html');
readfile('modal.html');
readfile ('loader.html');
?>
	
	
</body>

<script src="script/buildValidation.js"></script>
<script src="script/buildUpdate.js"></script>


<script>
//called when this page loads
function doLoad() {
	
	document.getElementById("footerButton").innerHTML = "Move to Step " + 5 + " <span id='blackBubble'><span>&gt;</span></span>";	
	
	//hide the gradient background in the PFIN and resize it
	document.getElementById("framedPFIN").contentWindow.document.getElementsByTagName("body")[0].style.backgroundImage = "none";
	onResize();
	
}


//called when the PFIN resizes
function onResize() {
	
	var phoneWidth = 440;
	
	//console.log(window.innerHeight);
	var scaleRatio = window.innerHeight / 1000;
	var maxWidth = document.getElementById("divLeftPanel").offsetWidth;	
	
	//do I scale based on height or width?
	if ((phoneWidth * scaleRatio) >= maxWidth) {
		scaleRatio = maxWidth/phoneWidth;
	}	
	
	//shuffle the phone to the left a little bit
	var scootchLeftOnResize = (phoneWidth/2) * (scaleRatio - 1); 
	console.log(scootchLeftOnResize);
	

	document.getElementById("phone").style.transform 	= "scale(" + scaleRatio + ")";
    document.getElementById("phone").style.left 		=  scootchLeftOnResize + "px";
	document.getElementById("phone").style.display		="block";	

	
}

//set the window's resize event
window.onresize = onResize;

</script>
<script src="utility.js"></script>
</html>













