<?php
include 'shared.php';
include 'qrFunctions.php';

//write a cookie in case we come back
$cookie_name = 'privacyux_uid';
$cookie_value = $_GET["userid"];
setcookie($cookie_name, $cookie_value, time() + (86400 * 365), '/','xcheq.com',true); // 86400 = 1 day


//write analytics 
//analytics("PAGE LOAD - BUILD.PHP");
?><!DOCTYPE html>
<html lang="en">
<head>

<?php
readfile('head.html');
readfile('style.html');
?>

<!--
DEBUG: 
-->
<script>
<?php include 'buildProcessor.php'; ?>
</script>

</head>
<body onload="doLoad();">

<?php 

readfile ('blankNavigation.html');
readfile ('footerUnderflow.html');

?>

<!-- Page Content -->
<div class="container">

<!-- Single Row-->
<div class="row">

	<!-- Left Panel -->
	<div id="divLeftPanel" class="col-sm-5 col-md-5">
	
	<div class="divSpacerTop" ></div>

		<!-- cell phone example -->
		<div id="phone" style="position:relative;display:none;">
			<div id="phoneSilhouette" class="phone"></div>
			<div id="phoneButtonLeft0" class="phone"></div>
			<div id="phoneButtonLeft1" class="phone"></div>	
			<div id="phoneButtonLeft2" class="phone"></div>	
			<div id="phoneButtonRight0" class="phone"></div>			
			<div id="phoneContent" class="phone"><iframe id="framedPFIN" src="privacyNotice.php" style="width:100%;height:100%; border:none;"></iframe></div>
		</div>
	
	</div>
	<!-- End Left Panel -->

	<!-- Right Panel -->
	<div id="divRightPanel" class="col-sm-7 col-md-7">
	<div class="divSpacerTop" ></div>	
		
		<!-- BUILD TUNNEL -->
		<div id="build5">
		<!--
			<button class="btn btn-privacyux" style="visible:false;">Complete</button>
		-->	
			<!-- PROGRESS METER -->
			<span class="progressMeter" style="width:100%;">
				<div id="divProgress5" class="progress-bar btnText-privacyux progressMeterCreep" >&nbsp;&nbsp;Progress Complete</div>
			</span>

		<h1>Your custom CCPA Notice At Collection is Complete</h1>

		
<?php if (true) { ?>
		<!-- This section is remarked out in PHP-land for later -->	
		<p>You can access your notice at:<br/><span id="spnNoticeURL"></span></p>

<?php
	//add the QR code to this page
	$userid = $_GET["userid"];
	$noticeURL = SERVER_PATH . "/privacyNotice.php?uid=" . $userid;
	$qrURL = generateQR($noticeURL);
	if ($qrURL != "") {
		
		echo "<img src='" . $qrURL . "' style='float:right;' />";
	}
	

?>
		
		<p>Use the link or QR code to share your custom Notice At Collection with your colleagues to show them how PrivacyUX Livestart quickly delivers CCPA notice compliance for the next thirty (30) days. Check your email to confirm your identity by clicking on the email we sent and choose a password for your account.</p>	

<?php } ?>		


<?php if (false) { ?>		
<p>
After you confirm your identity by clicking on the email we sent and choose a password for your account, you'll be given a link to your custom Notice At Collection and an email that you can share with your colleagues to show them how PrivacyUX Livestart quickly delivers CCPA notice compliance. 
</p><p> 
This "Nutrition style" notice gives your company the same user trust building transparency that drove Apple to require nutrition style notices for each of the 2 million publishers in the App Store. The difference is that <b>your PrivacyUX Livestart notice at collection works at every user touch point</b>.  
 </p><p> 
PrivacyUX Livestart rapidly solves the first and most obvious CCPA compliance requirement but it is not a complete solution. PrivacyCheq offers a complete transparency and opt out management service for CCPA that handles the complexity of gathering user preferences (including opt out), operationally honoring those preferences, distributing the preferences to third, fourth, and fifth parties that you share or sell data to.  And if your company's audience includes users under the age of 17, PrivacyUX for CCPA handles the complexity of consent for children 13-16 and for parental consent for children under 13. 
 </p>
<?php } ?> 
 <p> 
If your company wants to quickly and easily take leadership with user trust, we'd love to talk to you about our complete solution for CCPA.  Contact us at <a href="mailto:bizdev@privacycheq.com">bizdev@privacycheq.com</a> or via <a href="">this form</a>. 
</p>
		
			
			

		
		</div>	

	</div>
	<!-- End Right Panel -->	

</div>
<!-- End Row -->

</div> 
<!-- End Page Content -->
	

<br/><br/>
	
<?php
readfile ('bootstrapCore.html');
readfile('modal.html');
readfile ('loader.html');
?>
	
	
</body>

<script src="script/buildValidation.js"></script>
<script src="script/buildUpdate.js"></script>


<script>
var pageURL = "<?php echo SERVER_PATH;  ?>"

//called when this page loads
function doLoad() {

	//hide the button
	$("#footerButton").hide();
	
	//hide the gradient background in the PFIN and resize it
	document.getElementById("framedPFIN").contentWindow.document.getElementsByTagName("body")[0].style.backgroundImage = "none";
	onResize();
	
	//link to the notice
	$("#spnNoticeURL").html("<a target='new' href='" + pageURL +  "/privacyNotice.php?uid=" + userid + "'>" + pageURL +  "/privacyNotice.php?uid=" + userid + "</a>");
	
}


//called when the PFIN resizes
function onResize() {
	
	var phoneWidth = 440;
	
	//console.log(window.innerHeight);
	var scaleRatio = window.innerHeight / 1000;
	var maxWidth = document.getElementById("divLeftPanel").offsetWidth;	
	
	//do I scale based on height or width?
	if ((phoneWidth * scaleRatio) >= maxWidth) {
		scaleRatio = maxWidth/phoneWidth;
	}	
	
	//shuffle the phone to the left a little bit
	var scootchLeftOnResize = (phoneWidth/2) * (scaleRatio - 1); 
	console.log(scootchLeftOnResize);
	

	document.getElementById("phone").style.transform 	= "scale(" + scaleRatio + ")";
    document.getElementById("phone").style.left 		=  scootchLeftOnResize + "px";
	document.getElementById("phone").style.display		="block";	

}

//set the window's resize event
window.onresize = onResize;

</script>
<script src="utility.js"></script>
</html>













