<!DOCTYPE html><?php
//This page is used to BUILD the privacy dialog

//Identify variables
	$userid = "";
	$level = "";
	$iStep = 0;

//Test whether the user is logged in or not
session_start();
if (!isset($_SESSION['auth'])) {
	
	//clear the current session variables if we aren't authorized
	$_SESSION['auth'] = "0";
	$_SESSION['userid'] = "";
	$_SESSION['level'] = "";

} else {
	
	//we are in a session, get the level and userid
	if (isset($_SESSION['userid'])) {
		//get the userid from the session
		$userid = $_SESSION['userid'];
		
		//write the userid to a cookie just in case we get broken
		setcookie('userid', $userid);
		
		//get the level if it is available
		if (isset($_SESSION['level'])){
			$level = $_SESSION['level'];
		}		
		
	} 
}

//test to see if the query string with the userid
if (isset($_GET['userid']) && $userid =="" ) {
	$userid = $_GET['userid'];
} 	

//test to see if we have a cookie with the userid
if(isset($_COOKIE['userid']) && $userid == "") {
   $userid = $_COOKIE['userid'];
}

//test to see if we have a cookie with the userid
if(isset($_COOKIE['s']) ) {
	if (intval($_COOKIE['s']) > 0 && intval($_COOKIE['s']) < 6) {
		$iStep = intval($_COOKIE['s']);		
	}

}

//test to see if the query string holds a newer current step
if (isset($_GET['s'])) {
	if (intval($_GET['s']) > 0 && intval($_GET['s']) < 6) {
		$iStep = intval($_GET['s']);
	}
} 	

//global variables
$pStatusMessage = "";

echo "<!-- " . PHP_EOL;
echo "DEBUG BLOCK" . PHP_EOL;
echo "USERID:" . $userid . PHP_EOL;
echo "CURRENT BUILD STEP: " . $iStep . PHP_EOL;
echo " -->" . PHP_EOL;
 
include 'shared.php';
include 'functions/helperFunctions.php';
include 'functions/userFunctions.php';
include 'functions/qrFunctions.php';

//write analytics 
analytics("PAGE LOAD - BUILD.PHP");
?>
<html lang="en">
<head>

<?php
readfile('html/head.html');
readfile('html/style.html');
?>

<script>
<?php include 'buildProcessor.php'; ?>
</script>

</head>
<body onload="doLoad();">

<?php 
readfile ('html/blankNavigation.html');
readfile ('html/footerUnderflow.html');
?>

<!-- Page Content -->
<div class="container">

<!-- Single Row-->
<div class="row">

	<!-- Left Panel -->
	<div id="divLeftPanel" class="col-12 col-sm-12 col-md-5 col-lg-5 col-xl-5">
	
	<div class="divSpacerTop" ></div>

		<!-- cell phone example -->
		<div id="phone" style="position:relative;display:none;">
			<div id="phoneSilhouette" class="phone"></div>
			<div id="phoneButtonLeft0" class="phone"></div>
			<div id="phoneButtonLeft1" class="phone"></div>	
			<div id="phoneButtonLeft2" class="phone"></div>	
			<div id="phoneButtonRight0" class="phone"></div>			
			<div id="phoneContent" class="phone"><iframe id="framedPFIN" src="privacyNotice.php" style="width:100%;height:100%; border:none;"></iframe></div>
		</div>
	
	</div>
	<!-- End Left Panel -->

	<!-- Right Panel -->
	<div id="divRightPanel" class="col-12 col-sm-12 col-md-7 col-lg-7 col-xl-7">
	
		<!-- BUILD TUNNEL -->
	
		<div class="divSpacerTop" ></div>

<?php
readfile ('html/mobileAccordian.html');
?>		
		
		<!-- Step By Step -->
		<div id="divStepByStep">

			<!-- Step 1 -->
			<div id="divStep1" style="display:none;">

				<!-- STEP BUTTON -->
				<div class="step">
				<p class="pStepSpacer"></p>
				<span class="spnStepText" style="padding-top:5px;">Step 
				<span class="btnStepBubble"><span class="stepBubbleText">1</span></span>
				</span>
				</div>
				
				<!-- PROGRESS METER -->
				<span class="progressMeter" style="">
					<div id="divProgress1" class="progress-bar btnText-privacyux progressMeterCreep">&nbsp;&nbsp;Progress</div>
				</span>
				
				<h1>Let’s&nbsp;Build&nbsp;Your Custom CCPA Notice&nbsp;at&nbsp;Collection</h1>
				<div class="divSpacerNewline" ></div>		
				
				<form id="build0" class="" action="javascript:doUpdate('build0');" method="post" onsubmit="return(validateForm('build0'));">
					
					
					<label for="organization">What is the name of your company?&nbsp;<sup><i onclick="$(this).tooltip('show');" class="fas fa-info-circle" data-toggle="tooltip" data-html="true" data-placement="top" title="Enter the legal name of your company as it appears in your privacy policy."></i></sup></label>
					<div class="divSpacerNewline" ></div>	
					<input name="organization" type="text" class="form-control" placeholder="Company Name" required><br/>
					
					<label><u><a href="https://oag.ca.gov/privacy/ccpa#ccpafaq" target="new">Here’s how CCPA defines personal information</a></u></label><br/>
					<label for="collectdata">Do you collect any personal data? Y/N&nbsp;<sup><i onclick="$(this).tooltip('show');" class="fas fa-info-circle" data-toggle="tooltip" data-html="true" data-placement="top" title="This key fact determines how your notice will be structured. If your company collects personal information, you’ll need to disclose the details in the next screens."></i></sup></label>
					
					<div class="divSpacerNewline" ></div>
					
					<input id="collectdatatrue" name="radiocollectdata" type="radio" value="true" class="form-radio-input btnBigRadio" onclick="$('#collectdataHidden').val('true');" checked> &nbsp;<span for="collectdatatrue" >Yes</span> &nbsp;&nbsp;&nbsp;
					<input id="collectdatafalse" name="radiocollectdata" type="radio" value="false" class="form-radio-input btnBigRadio" onclick="$('#collectdataHidden').val('false');" > &nbsp;<span for="collectdatafalse" >No</span>
					
					<input type="hidden" name="collectdata" id="collectdataHidden" value="true"></input>		
					<div class="divSpacerNewline" ></div><div class="divSpacerNewline" ></div>		

					<label for="privacypolicyurl">What is the URL of your company's full privacy policy?&nbsp;<sup><i onclick="$(this).tooltip('show');" class="fas fa-info-circle" data-toggle="tooltip" data-html="true" data-placement="top" title="Enter the web address of your company’s legal  privacy policy."></i></sup></label> 
					<div class="divSpacerNewline" ></div>		
					<input name="privacypolicyurl" type="text" class="form-control"  placeholder="https://www.company.com/privacy" required>

				</form>
		
			</div>
			<!-- End Step 1 -->

		
			<!-- Step 2 -->
			<div id="divStep2" style="display:none;">
			
				<!-- STEP BUTTON -->
				<div class="step">
				<p class="pStepSpacer"></p>
				<span class="spnStepText" style="padding-top:5px;">Step 
				<span class="btnStepBubble"><span class="stepBubbleText">2</span></span>
				</span>
				</div>
				
				<!-- PROGRESS METER -->
				<span class="progressMeter" style="">
					<div id="divProgress2" class="progress-bar btnText-privacyux progressMeterCreep">&nbsp;&nbsp;Progress</div>
				</span>
				
				<h1>Let’s&nbsp;Build&nbsp;Your Custom CCPA Notice&nbsp;at&nbsp;Collection</h1>
				<div class="divSpacerNewline" ></div>
				
				<form id="build1" class="" action="javascript:doUpdate('build1');" method="post" onsubmit="return(validateForm('build1'));">
							
							<label for="orgfunction">What does your company do? (in a sentence)&nbsp;<sup><i onclick="$(this).tooltip('show');" class="fas fa-info-circle" data-toggle="tooltip" data-html="true" data-placement="top" title="Use this sentence to briefly describe to your customers what you need their personal data for."></i></sup></label> <div class="divSpacerNewline" ></div>		
							<input name="orgfunction" type="text" class="form-control" placeholder="We create widgets." required><div class="divSpacerNewline" ></div>

							<label for="orglocation">What is your company's address?&nbsp;<sup><i onclick="$(this).tooltip('show');" class="fas fa-info-circle" data-toggle="tooltip" data-html="true" data-placement="top" title="This information is used to identify your organization and give customers the ability to write your organization by post with any questions about their privacy."></i></sup></label><div class="divSpacerNewline" ></div>		
							<input name="orglocation" type="text" class="form-control"  placeholder="123 Elm St. Anytown US 00000" required><div class="divSpacerNewline" ></div>

							<label for="orgphone">What is your customer support phone number? (optional)&nbsp;<sup><i onclick="$(this).tooltip('show');" class="fas fa-info-circle" data-toggle="tooltip" data-html="true" data-placement="top" title="If your customers have a question about how your organization treats their personal data, what phone number could they use?"></i></sup></label><div class="divSpacerNewline" ></div>
							<input name="orgphone" type="text" class="form-control"  placeholder="800.123.3333"><div class="divSpacerNewline" ></div>

							<label for="orgemail">What is the email address for privacy concerns? (optional)&nbsp;<sup><i onclick="$(this).tooltip('show');" class="fas fa-info-circle" data-toggle="tooltip" data-html="true" data-placement="top" title="If your customers have a question about how your organization treats their personal data, what email address should they use?"></i></sup></label><div class="divSpacerNewline" ></div>
							<input name="orgemail" type="text" class="form-control"  placeholder="privacy@company.com" required><div class="divSpacerNewline" ></div>

							<label for="orgurl">What is the URL of your main website?&nbsp;<sup><i onclick="$(this).tooltip('show');" class="fas fa-info-circle" data-toggle="tooltip" data-html="true" data-placement="top" title="Enter the web address of your company's marketing website."></i></sup></label> <div class="divSpacerNewline" ></div>	
							<input name="orgurl" type="text" class="form-control"  placeholder="http://www.company.com" required><div class="divSpacerNewline" ></div>

							
				</form>			
			
			</div>
			<!-- End Step 2 -->

			<!-- Step 3 -->
			<div id="divStep3" style="display:none;">

				<!-- STEP BUTTON -->
				<div class="step">
				<p class="pStepSpacer"></p>
				<span class="spnStepText" style="padding-top:5px;">Step 
				<span class="btnStepBubble"><span class="stepBubbleText">3</span></span>
				</span>
				</div>
				
				<!-- PROGRESS METER -->
				<span class="progressMeter" style="">
					<div id="divProgress3" class="progress-bar btnText-privacyux progressMeterCreep">&nbsp;&nbsp;Progress</div>
				</span>

				<h1>Let’s&nbsp;Build&nbsp;Your Custom CCPA Notice&nbsp;at&nbsp;Collection</h1>
					
				<form id="build2"  action="javascript:doUpdate('build2');" method="post" onsubmit="return(validateForm('build2'));">
					
					<table><tr><td class="checkboxSpacer"></td>
					<td>
							<label for="">What categories of personal data does your company collect?&nbsp;<sup><i onclick="$(this).tooltip('show');" class="fas fa-info-circle" data-toggle="tooltip" data-html="true" data-placement="top" title="The CCPA identifies these as the possible categories of personal data that might be collected. This law requires that organizations that do business in California share which of these categories of data are collected."></i></sup></label><div class="divSpacerNewline" ></div>			
							<input type="checkbox" class="form-check-input inpBigCheck" onchange=" $('#catidentifiers').val(this.checked);"> &nbsp;<span for="catidentifiers" class="forcheck!">Identifiers</span><div class="divSpacerNewline" ></div>
							
							<input type="checkbox" class="form-check-input inpBigCheck" onchange=" $('#catcustomerrecords').val(this.checked);"> &nbsp;<span for="catcustomerrecords" class="forcheck">Information in Customer Records</span><div class="divSpacerNewline" ></div>
							
							<input type="checkbox" class="form-check-input inpBigCheck" onchange=" $('#catlegallyprotected').val(this.checked);"> &nbsp;<span for="catlegallyprotected" class="forcheck">Legally Protected Characteristics</span><div class="divSpacerNewline" ></div>
							
							<input type="checkbox" class="form-check-input inpBigCheck" onchange=" $('#catcommercialpurchasing').val(this.checked);"> &nbsp;<span for="catcommercialpurchasing" class="forcheck">Commercial Purchasing Information</span><div class="divSpacerNewline" ></div>
							
							<input type="checkbox" class="form-check-input inpBigCheck" onchange=" $('#catbiometric').val(this.checked);"> &nbsp;<span for="catbiometric" class="forcheck">Biometric Information</span><div class="divSpacerNewline" ></div>
							
							<input type="checkbox" class="form-check-input inpBigCheck" onchange=" $('#catnetworkactivity').val(this.checked);"> &nbsp;<span for="catnetworkactivity" class="forcheck">Internet or Network Activity</span><div class="divSpacerNewline" ></div>
							
							<input type="checkbox" class="form-check-input inpBigCheck" onchange=" $('#catgeolocation').val(this.checked);"> &nbsp;<span for="catgeolocation" class="forcheck">Geolocation</span><div class="divSpacerNewline" ></div>
							
							<input type="checkbox" class="form-check-input inpBigCheck" onchange=" $('#catsenses').val(this.checked);"> &nbsp;<span for="catsenses" class="forcheck">Information Typically Detected by the Senses</span><div class="divSpacerNewline" ></div>
							
							<input type="checkbox" class="form-check-input inpBigCheck" onchange=" $('#catemployment').val(this.checked);"> &nbsp;<span for="catemployment" class="forcheck">Employment Information</span><div class="divSpacerNewline" ></div>
							
							<input type="checkbox" class="form-check-input inpBigCheck" onchange=" $('#cateducation').val(this.checked);"> &nbsp;<span for="cateducation" class="forcheck">Education Information</span><div class="divSpacerNewline" ></div>
							
							<input type="checkbox" class="form-check-input inpBigCheck" onchange=" $('#catinferences').val(this.checked);"> &nbsp;<span for="catinferences" class="forcheck">Data Inferences Used to Profile</span><div class="divSpacerNewline" ></div>
					</td></tr></table>
					
							<input type="hidden" id="catidentifiers" name="catidentifiers" value="false"></input>
							<input type="hidden" id="catcustomerrecords" name="catcustomerrecords" value="false"></input>
							<input type="hidden" id="catlegallyprotected" name="catlegallyprotected" value="false"></input>
							<input type="hidden" id="catcommercialpurchasing" name="catcommercialpurchasing" value="false"></input>
							<input type="hidden" id="catbiometric" name="catbiometric" value="false"></input>
							<input type="hidden" id="catnetworkactivity" name="catnetworkactivity" value="false"></input>
							<input type="hidden" id="catgeolocation" name="catgeolocation" value="false"></input>
							<input type="hidden" id="catsenses" name="catsenses" value="false"></input>
							<input type="hidden" id="catemployment" name="catemployment" value="false"></input>
							<input type="hidden" id="cateducation" name="cateducation" value="false"></input>
							<input type="hidden" id="catinferences" name="catinferences" value="false"></input>
							<script>
							var arrCatChoices = ["catidentifiers","catcustomerrecords","catlegallyprotected","catcommercialpurchasing","catbiometric","catnetworkactivity","catgeolocation","catsenses","catemployment","cateducation","catinferences"];
							</script>
			
					
				</form>

			</div>
			<!-- End Step 3 -->

			<!-- Step 4 -->
			<div id="divStep4" style="display:none;">

				<!-- STEP BUTTON -->
				<div class="step">
				<p class="pStepSpacer"></p>
				<span class="spnStepText" style="padding-top:5px;">Step 
				<span class="btnStepBubble"><span class="stepBubbleText">4</span></span>
				</span>
				</div>
				
				<!-- PROGRESS METER -->
				<span class="progressMeter" style="">
					<div id="divProgress4" class="progress-bar btnText-privacyux progressMeterCreep">&nbsp;&nbsp;Progress</div>
				</span>

				<h1>Let’s&nbsp;Build&nbsp;Your Custom CCPA Notice&nbsp;at&nbsp;Collection</h1>
				
				<form id="build3" action="javascript:doUpdate('build3');" method="post" onsubmit="return(validateForm('build3'));">
					
					<label>This personal information is collected for which purpose(s)?&nbsp;<sup><i  onclick="$(this).tooltip('show');" class="fas fa-info-circle" data-toggle="tooltip" data-html="true" data-placement="top" title="The CCPA identifies these as the possible purposes for collecting customers' personal data. This law requires that organizations that do business in California share which purposes personal data is collected for."></i></sup></label> <div class="divSpacerNewline" ></div>			
					<input type="checkbox" class="form-check-input inpBigCheck"  onchange=" $('#purbusiness').val(this.checked);"> &nbsp; <span for="purbusiness">For Business Purposes&nbsp;<sup><i  onclick="$(this).tooltip('show');" class="fas fa-question-circle" data-toggle="tooltip" data-html="true" data-placement="top" title='A "Business purpose" is defined in the CCPA as "the use of personal information for the business&apos;s or a service provider&apos;s operational purposes, or other notified purposes, provided that the use of personal information shall be reasonably necessary and proportionate to achieve the operational purpose for which the personal information was collected or processed or for another operational purpose that is compatible with the context in which the personal information was collected."'></i></sup></span><div class="divSpacerNewline" ></div>
					<input type="checkbox" class="form-check-input inpBigCheck" onchange=" $('#purcommercial').val(this.checked);"> &nbsp; <span for="purcommercial">For Commercial Purposes&nbsp;<sup><i  onclick="$(this).tooltip('show');" class="fas fa-question-circle" data-toggle="tooltip" data-html="true" data-placement="top" title='A Commercial purpose" is defined in the CCPA as a means "to advance a person&apos;s commercial or economic interests, such as by inducing another person to buy, rent, lease, join, subscribe to, provide, or exchange products, goods, property, information, or services, or enabling or effecting, directly or indirectly, a commercial transaction." Commercial purposes do "not include for the purpose of engaging in speech that state or federal courts have recognized as noncommercial speech, including political speech and journalism."'></i></sup></span><div class="divSpacerNewline" ></div>
					<input type="checkbox" class="form-check-input inpBigCheck" onchange=" $('#purconsumer').val(this.checked);"> &nbsp; <span for="purconsumer">For Fulfilling Consumers' Requests</span><div class="divSpacerNewline" ></div>
					
					<input type="hidden" id="purbusiness" name="purbusiness" value="false"></input>
					<input type="hidden" id="purcommercial" name="purcommercial" value="false"></input>
					<input type="hidden" id="purconsumer" name="purconsumer" value="false"></input>			
					<script>
					var arrPurChoices = ["purbusiness","purcommercial","purconsumer"];
					</script>		
						
				</form>

			</div>
			<!-- End Step 4 -->

			<!-- Step 5 -->
			<div id="divStep5" style="display:none;">

				<!-- STEP BUTTON -->
				<div class="step">
				<p class="pStepSpacer"></p>
				<span class="spnStepText" style="padding-top:5px;">Step 
				<span class="btnStepBubble"><span class="stepBubbleText">5</span></span>
				</span>
				</div>
				
				<!-- PROGRESS METER -->
				<span class="progressMeter" style="">
					<div id="divProgress5" class="progress-bar btnText-privacyux progressMeterCreep">&nbsp;&nbsp;Progress</div>
				</span>

				<h1>Let’s&nbsp;Build&nbsp;Your Custom CCPA Notice&nbsp;at&nbsp;Collection</h1>
					
				<form id="build4" action="javascript:doUpdate('build4');" method="post" onsubmit="return(validateForm('build4'));">
								
				<p class="description">As you can see on the left, we've built a custom CCPA Notice at Collection using your inputs. This notice fully complies with CCPA, looks great no matter what device your consumers are using, and can easily be added wherever <a href="https://oag.ca.gov/privacy/ccpa#ccpafaq" target="new">personal information</a> is ingested.</p>

				<p class="description">We'll email you a link to your fully functional custom notice and instructions on how to test it in all of the required CCPA scenarios. For thirty (30) days, you can test and share with your colleagues. Don't worry, PrivacyCheq only uses your email to communicate with you about this demonstration.</p><div class="divSpacerNewline" ></div>			

				<label for="emailcustomer">What is your email address?</label> <sup><i onclick="$(this).tooltip('show');"  class="fas fa-info-circle" data-toggle="tooltip" data-html="true" data-placement="top" title="PrivacyUX will use your email address to message you about your new privacy notice and to identify you when you maintain your notice in the future."></i></sup><br/>	
				<input name="emailcustomer" type="text" class="form-control"  placeholder="youremail@yourorg.com" required><br/>

				Deliver my CCPA Notice at Collection demonstration
				<button type="submit" class="btn btn-privacyux">Let's Go</button>		
				</form>

			</div>
			<!-- End Step 5 -->

			<!-- Step 6 -->
			<div id="divStep6" style="display:none;">
		
				<!-- PROGRESS METER -->
				<span class="progressMeter" style="width:100%;">
					<div id="divProgress5" class="progress-bar btnText-privacyux progressMeterCreep" >&nbsp;&nbsp;Progress Complete</div>
				</span>


				<h1>Your custom CCPA Notice At Collection is Complete</h1>

				
				<?php if (true) { ?>

						<p>You can access your notice at:<br/><span id="spnNoticeURL"></span></p>

						<?php
						if ($iStep == 5) {
							//add the QR code to this page
							$noticeURL = SERVER_PATH . "/privacyNotice.php?uid=" . $userid;
							$qrURL = generateQR($noticeURL);
							if ($qrURL != "") {
								
								echo "<img src='" . $qrURL . "' style='float:right;' />";
							}						
						}
						?>
				
					<p>Use the link or QR code to share your custom Notice At Collection with your colleagues to show them how PrivacyUX Livestart quickly delivers CCPA notice compliance for the next thirty (30) days. Check your email to confirm your identity by clicking on the email we sent and choose a password for your account.</p>	

				<?php } ?>		


				<?php if (false) { ?>		
				<p>
				After you confirm your identity by clicking on the email we sent and choose a password for your account, you'll be given a link to your custom Notice At Collection and an email that you can share with your colleagues to show them how PrivacyUX Livestart quickly delivers CCPA notice compliance. 
				</p><p> 
				This "Nutrition style" notice gives your company the same user trust building transparency that drove Apple to require nutrition style notices for each of the 2 million publishers in the App Store. The difference is that <b>your PrivacyUX Livestart notice at collection works at every user touch point</b>.  
				 </p><p> 
				PrivacyUX Livestart rapidly solves the first and most obvious CCPA compliance requirement but it is not a complete solution. PrivacyCheq offers a complete transparency and opt out management service for CCPA that handles the complexity of gathering user preferences (including opt out), operationally honoring those preferences, distributing the preferences to third, fourth, and fifth parties that you share or sell data to.  And if your company's audience includes users under the age of 17, PrivacyUX for CCPA handles the complexity of consent for children 13-16 and for parental consent for children under 13. 
				 </p>
				<?php } ?> 
				
				<p> 
				If your company wants to quickly and easily take leadership with user trust, we'd love to talk to you about our complete solution for CCPA.  Contact us at <a href="mailto:bizdev@privacycheq.com">bizdev@privacycheq.com</a> or via <a href="">this form</a>.</p>
				
			
			</div>
			<!-- End Step 6 -->			
		
		</div>
		<!-- End Step By Step -->

	
	</div>
	<!-- End Right Panel -->	

</div>
<!-- End Row -->

</div> 
<!-- End Page Content -->
	

<br/><br/>
	
<?php
readfile ('html/bootstrapCore.html');
readfile('html/modal.html');
readfile('html/loader.html');
?>
	
	
</body>


<script src="script/buildValidation.js"></script>
<script src="script/buildUpdate.js"></script>
<script src="script/localStorage.js"></script>


<script>

//identify the step we loaded into
var iStep = <?php echo $iStep; ?>;
var iNextStep = iStep + 2;

//called when this page loads
function doLoad() {
	
	//show the appropriate step and button on load
	showStep(iStep);
	
	//hide the gradient background in the PFIN and resize it
	document.getElementById("framedPFIN").contentWindow.document.getElementsByTagName("body")[0].style.backgroundImage = "none";
	onResize();
	
}

//!!! TODO: Fix the submit action

//submit the form
function submitNext(iPassedStep) {
	
	if (iPassedStep != null) {
	
		//submit the form		
		$("#build" + iPassedStep).submit();
	
	} else {
		
		//submit all steps at once
		if (validateForm("buildAll")) {
			//if true on validation, then send 
			doUpdate("buildAll");
		}
		

	}


}



function showStep(iShowStep) {
	//update the step if it is passed, otherwise just use the iStep
	if (iShowStep) {
		iStep = iShowStep;
	}
	
	if (iStep >=0 && iStep<4) {
		
		//$("#footerButton").html("Move to Step " + iNextStep + " <span id='blackBubble'><span>&gt;</span></span>");	
		
		$("#footerButton").html("Move to Step " + iNextStep + " <img src='assets/buttons/icon_arrow.png' style='height:46px;' />");
		
		$("#footerButton").show();
	}
	
	//hide all steps
	$("#divStep1").hide();
	$("#divStep2").hide();
	$("#divStep3").hide();
	$("#divStep4").hide();
	$("#divStep5").hide();
	$("#divStep6").hide();
	
	strActualStep = iStep+1;
	$("#divStep" + strActualStep).show();	
	
	
}

//called when the PFIN resizes
function onResize() {
	
	var phoneWidth = 440;
	
	//console.log(window.innerHeight);
	var scaleRatio = window.innerHeight / 1000;
	var maxWidth = document.getElementById("divLeftPanel").offsetWidth;	
	
	//do I scale based on height or width?
	if ((phoneWidth * scaleRatio) >= maxWidth) {
		scaleRatio = maxWidth/phoneWidth;
	}	
	
	//shuffle the phone to the left a little bit
	var scootchLeftOnResize = (phoneWidth/2) * (scaleRatio - 1); 

	document.getElementById("phone").style.transform 	= "scale(" + scaleRatio + ")";
    document.getElementById("phone").style.left 		= scootchLeftOnResize + "px";
	document.getElementById("phone").style.display		= "block";	

}

//set the window's resize event
window.onresize = onResize;

</script>
<script src="script/utility.js"></script>


</html>













