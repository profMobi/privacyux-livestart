<!DOCTYPE html>
<?php
include 'shared.php';
?>

<html lang="en">
<head>

<?php
readfile('html/head.html');
readfile('html/style.html');
?>

<style>
.testPanel {
	display:none;
}
</style>

<script>
//run when the unit test page loads
function doLoad() {
	
	
}

//show only the selected test
function testChange() {
	$(".testPanel").hide();
	$("#" + $("#tests").val()).show();
	
}
</script>

</head>
<body onload="doLoad();">

<?php 

readfile ('html/blankNavigation.html');

?>

<!-- Page Content -->
<div class="container">

<h1>Unit Test</h1>
TEST THIS:<br/>
<li/>include ("functions/helperFunctions.php");
<li/>include ("functions/emailFunctions.php");
<li/>include ("functions/databaseFunctions.php");
<li/>include ("functions/savedataFunctions.php");
<li/>include ("functions/retrievedataFunctions.php");
<li/>include ("functions/qrFunctions.php");<br/><br/>
This page holds a list of unit tests of the PrivacyUX project<br/>
<select class="form-control"  name="tests" id="tests" onchange="testChange(this);">
  <option value="">Select a Test</option>
  <option value="newuser">Create a new user</option>
  <option value="identifyuser">Identify a user with an email address</option>
  <option value="createnoticejson">Save JSON to database</option>
  <option value="getnoticeidfromuserid">Get notice from user id</option>
  <option value="createnotice">Create a new notice</option>
  <option value="sendinviteemail">Send invite email</option>
</select>
<hr/>

<!-- Single Row-->
<div class="row">

	<!-- Left Panel -->
	<div id="divLeftPanel" class="col-sm-6 col-md-6">
	
		<div id="newuser" class="testPanel">
			<h2>New User - tools.php</h2>
			<label>Data Object</label><br/><textarea class="form-control" id="datanewuser" type="string" style="width:100%;" rows="5">{"email":"","secret":""}</textarea><br/>
			<button class="btn btn-primary" onclick="test_newuser($('#datanewuser').val());">Test newuser</button>
		</div>


	
		<div id="identifyuser" class="testPanel">
			<h2>Identify user - tools.php</h2>
			<label>Data Object</label><br/><textarea class="form-control" id="dataidentifyuser" type="string" style="width:100%;" rows="5">{"email":"","userid":""}
			</textarea><br/>
			<button class="btn btn-primary" onclick="test_identifyuser($('#dataidentifyuser').val());">Test identifyuser</button>
		</div>	
		
		<div id="createnoticejson" class="testPanel">
			<h2>Create Notice from JSON - tools.php</h2>
			<label>Data Object</label><br/><textarea class="form-control" id="datacreatenoticejson" type="string" style="width:100%;" rows="5">{"email":"","userid":"","noticejson":""}</textarea><br/>
			<button class="btn btn-primary" onclick="test_createnoticejson($('#datacreatenoticejson').val());">Test createnoticejson</button>
		</div>
	
		<div id="getnoticeidfromuserid" class="testPanel">
			<h2>Get Notice ID from User ID - tools.php</h2>
			<label>Data Object</label><br/><textarea class="form-control" id="datagetnoticeidfromuserid" type="string" style="width:100%;" rows="5">{"userid":""}</textarea><br/>
			<button class="btn btn-primary" onclick="test_getnoticeidfromuserid($('#datagetnoticeidfromuserid').val());">Test getnoticeidfromuserid</button>
		</div>		
		
	
		<div id="createnotice" class="testPanel">
			<h2>Create a new notice - tools.php</h2>
			<label>Data Object</label><br/><textarea class="form-control" id="datacreatenotice" type="string" style="width:100%;" rows="5">{"userid":"","email":"","name":"","title":"","icon":"","intro":""}
			</textarea><br/>
			<button class="btn btn-primary" onclick="test_createnotice($('#datacreatenotice').val());">Test getnoticeidfromuserid</button>
		</div>			

	
		<div id="sendinviteemail" class="testPanel">
			<h2>Send invite email - tools.php</h2>
			<label>Data Object</label><br/><textarea class="form-control" id="datasendinviteemail" type="string" style="width:100%;" rows="5">{"email":"","noticeid":"","userid":"","noticename":""}
			</textarea><br/>
			<button class="btn btn-primary" onclick="test_sendinviteemail($('#datasendinviteemail').val());">Test sendinviteemail</button>
		</div>		
		
		
		
	</div>
	<!-- End Left Panel -->

	<!-- Right Panel -->
	<div id="divRightPanel" class="col-sm-6 col-md-6">	
		<textarea id="txtRightPanel" class="form-control" rows="15"></textarea>

	</div>
	<!-- End Right Panel -->	

</div>
<!-- End Row -->

</div> 
<!-- End Page Content -->
	

<br/><br/>
	
<?php

readfile ('html/footer.html');
readfile ('html/bootstrapCore.html');
readfile ('html/modal.html');
readfile ('html/loader.html');
?>
	
	
</body>

<script>
// &&&& Here are the functions that execute the tests &&&&//

function test_sendinviteemail(objPostData) {

	if (!objPostData) {
		objPostData = {};	

		objPostData["userid"] 		= "5B45A714-745B-49F2-A0F6-D19F94E60007";	
		objPostData["email"] 		= "";
		objPostData["noticeid"] 	= "";
		objPostData["noticename"] 	= "";	

		objPostData["cmd"] = "sendinviteemail";	
	} else {
		//make sure it is an OBJECT rather than a string
		objPostData = JSON.parse(objPostData);
		objPostData["cmd"] = "sendinviteemail";
	}	
					
	//post to tools
	$.post("tools.php",objPostData,function(data) {		

		$("#txtRightPanel").html(data);

	});	 	
	
}

function test_createnotice(objPostData) {
	if (!objPostData) {
		objPostData = {};	

		objPostData["userid"] 	= "5B45A714-745B-49F2-A0F6-D19F94E60007";	
		objPostData["email"] 	= "";
	
		objPostData["name"] 	= "";
		objPostData["title"] 	= "";	//if blank, set to "Privacy Facts"
		objPostData["icon"] 	= "";	//can be blank	
		objPostData["intro"] 	= "";  //if blank set to default		

		objPostData["cmd"] = "createnotice";	
	} else {
		//make sure it is an OBJECT rather than a string
		objPostData = JSON.parse(objPostData);
		objPostData["cmd"] = "createnotice";
	}	
					
	//post to tools
	$.post("tools.php",objPostData,function(data) {		

		$("#txtRightPanel").html(data);

	});	 	
}

function test_getnoticeidfromuserid(objPostData) {
	
	if (!objPostData) {
		objPostData = {};	
		objPostData["id"] 		= "5B45A714-745B-49F2-A0F6-D19F94E60007";
		objPostData["cmd"] = "getnoticeidfromuserid";	
	} else {
		//make sure it is an OBJECT rather than a string
		objPostData = JSON.parse(objPostData);
		objPostData["cmd"] = "getnoticeidfromuserid";
	}	
					
	//post to tools
	$.post("tools.php",objPostData,function(data) {		

		$("#txtRightPanel").html(data);

	});	 	
	
}

function test_createnoticejson(objPostData) {
	
	if (!objPostData) {
		objPostData = {};	
		objPostData["email"] 	= "ahs+testme@privacycheq.com";
		objPostData["userid"] 		= null;
		objPostData["cmd"] = "createnoticejson";	
	} else {
		//make sure it is an OBJECT rather than a string
		objPostData = JSON.parse(objPostData);
		objPostData["cmd"] = "createnoticejson";
	}
	
					
	//post to tools
	$.post("tools.php",objPostData,function(data) {		

		$("#txtRightPanel").html(data);
	
		//turn the string data returned back into a JSON object
		var objReturnData = JSON.parse(data);
		
		

		if (objReturnData.rtn != "ok") {


		} else {



		}

	});	 
	
} 

function test_identifyuser (objPostData) {
	
	objPostData = JSON.parse(objPostData);
	objPostData["cmd"] = "identifyuser";


						
	//post to tools
	$.post("tools.php",objPostData,function(data) {		

		$("#txtRightPanel").html(data);
	
		//turn the string data returned back into a JSON object
		var objReturnData = JSON.parse(data);
		
		

		if (objReturnData.rtn != "ok") {


		} else {



		}

	});	 
	
}

function test_newuser(objPostData) {
	
	if (!objPostData) {
		objPostData = {};	
		objPostData["email"] 	= "ahs+testme4@privacycheq.com";
		objPostData["id"] 		= null;
		objPostData["password"] = "this is a password?";
		objPostData["cmd"] = "newuser";	
	} else {
		//make sure it is an OBJECT rather than a string
		objPostData = JSON.parse(objPostData);
		objPostData["cmd"] = "newuser";
	}
	
	

						
	//post to tools
	$.post("tools.php",objPostData,function(data) {		

		$("#txtRightPanel").html(data);
	
		//turn the string data returned back into a JSON object
		var objReturnData = JSON.parse(data);
		
		

		if (objReturnData.rtn != "ok") {


		} else {



		}

	});	 
	
}
</script>

<script src="utility.js"></script>
</html>	