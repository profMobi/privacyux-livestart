<?php
//This is the landing page for new users coming from their email validation

$userid = "";
if(isset($_GET['uid'])) {
    $userid = strtolower($_GET['uid']);

} else {
	//if no uid passed something has gone wrong with onboarding.php

   	//redirect to marketing page 
	header ("Location: https://www.privacyux.com");
	exit();	
	
}




//global variables
$pStatusMessage = "";


?><!DOCTYPE html>


<!--
<?php
echo "DEBUG BLOCK:" . $userid . PHP_EOL;


?>
-->

<?php
include 'shared.php';

//write analytics 
analytics("PAGE LOAD - NEWUSER.PHP",$userid);
?>

<html lang="en">

<head>

<?php
readfile('html/head.html');
readfile('html/style.html');
?>


</head>
<body>


<?php 

readfile ('html/blankNavigation.html');

?>


<!-- Page Content -->
<div class="container">


<br/><br/>

<?php
	if ($userid == "" ) {
		//if there IS a valid login, show the dashboard page	
?>

<h1 class="my-4">No Key Passed</h1>	

<?php
	} else {
?>		
	
		
<h1 class="my-4">Create Your Account</h1>

<p>Please choose a password to identify yourself on our system. You will log in using a combination of your email address and the password you choose below. </p>
<p id="pStatusMessage" style="color:red;"><?php echo $pStatusMessage; ?></p>



<div class="row">

	<div class="col-sm-4 col-md-4">
		<!-- First column -->
		
		<form id="npass" class="" action="javascript:doSubmit('npass');" method="post" onsubmit="return(validateForm('npass'));">	
			
			<label for="newPassword">Choose Password</label><br/>			
			<input id="newpassword" name="newpassword" type="password" class="form-control" placeholder="New Password" required><br/>
			
			<label for="newpasswordconfirm">Confirm Password</label><br/>			
			<input id="newpasswordconfirm" name="newpasswordconfirm" type="password" class="form-control"  placeholder="Confirm Password" required><br/>
			
			<input id="userid" name="userid" type="hidden" value="<?php
				echo $userid;
			?>"/>

			<button type="submit" class="btn btn-primary">Save Your Password</button>		

		</form>	
		<br/><br/>   
		

		
	</div>
	
	<div class="col-sm-4 col-md-4">
		<!-- Second column -->
		 
		
	</div>	
	
	<div class="col-sm-4 col-md-4">
		<!-- Third column -->
		
		
	</div>	
</div>	
	
<?php	
	}
?>

</div>

<br/><br/>
	
<?php
readfile('html/footer.html');
readfile('html/modal.html');
readfile('html/bootstrapCore.html');
?>
	
	
</body>


<script>
//validate the tool forms
function validateForm(strForm) {
	
	//identify the command
	var cmd = strForm;

	//pull the form fields into an object
	objData = {}
	objForm = $("#" + strForm).find(':input');
	for (var x=0;x<objForm.length;x++) {
		
		//get the value of inputs
		objData[objForm[x].name] = objForm[x].value;
		
		//get the checked value of checkboxes
		if (objForm[x].className == "form-check-input") {
			objData[objForm[x].name] = objForm[x].checked;			
		}
		
	}	

	//return false if validation fails
	boolReturn = false;
	
	//validate the new user tool
	if (cmd == "nuser") {
		
		if (objData.newUser == "") {
			modalShow("No Username","Please choose a username","OK");
			return false;
		}
		else if (objData.newpassword != objData.newpasswordconfirm) {
			modalShow("Please reconfirm the password","Password Matching","OK");
			return false;			
		}
		boolReturn = true;
	}
	
	//validate the user access tool
	if (cmd == "uaccess") {

		//make sure we pick a user
		if (objData.manageUser == "") {
			modalShow("Please choose a user to manage","No User","OK");
			return false;
		}

		boolReturn = true;

	}
	
	//validate the new password tool
	if (cmd == "npass") {

		if (objData.newpassword != objData.newpasswordconfirm) {
			modalShow("Your two passwords don't match. Please reconfirm the password you chose.","Password Matching","OK");
			return false;			
		}
		else if (objData.newpassword.length < 8) {
			modalShow("Please choose a password at least eight characters long","Password Length","OK");
			return false;			
		}		
		boolReturn = true;

	}

	//validate the new dialog tool
	if (cmd == "ndialog") {

		if (objData.newDialogID == "") {
			modalShow("No Dialog ID","Please enter a valid dialog id","OK");
			return false;
		}
		else if (objData.newDialogTitle == "") {
			modalShow("Please enter a title","No Title","OK");
			return false;			
		}
		
		boolReturn = true;

	}	
	
	return boolReturn;
	
}

//submit the tool forms to the PHP page
function doSubmit(strForm) {
	console.log("doSubmit");
	
	//identify the command
	var cmd = strForm;
	
	//pull the form fields into an object
	objData = {"cmd":strForm};
	objForm = $("#" + strForm).find(':input');
	for (var x=0;x<objForm.length;x++) {
		
		//get the value of inputs
		if (cmd == "uaccess") {
			//for dialog access, make sure all the checkboxes are lowercased
			objData[objForm[x].name] = objForm[x].value.toLowerCase();
		} else {
			objData[objForm[x].name] = objForm[x].value;			
		}

		
		//get the checked value of checkboxes
		if (objForm[x].className == "form-check-input") {
			objData[objForm[x].name] = objForm[x].checked;			
		}
	}	

	//debug
	console.log(objData);
	
	//post the information to the tools
	$.post("usertools.php",objData,function(data) {
		
		//console.log(data);
		
		//console.log(JSON.parse(data).id);
		
		//!!! TODO: I should probably do some error checking here
		
		destinationUserID = JSON.parse(data).id;
		destinationUserEmail = JSON.parse(data).email;
		
		//send viral email
		$.post("tools.php",{"cmd":"sendviralemail","email":destinationUserEmail,"userid":destinationUserID},function(data) {
			
			//console.log(data);
			
			//!!! TODO: More error checking here as well

			modalShow("We've saved your password. Use it along with your email address to log on in to PrivacyUX.","Password Saved","OK",function(){
				//logout after successfully changing the password
				window.location.replace("login.php");
				
			});	

		});
		

	}); 
	
}



</script>

</html>