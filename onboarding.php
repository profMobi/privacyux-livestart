<?php
//This page validates the user's account and redirects it appropriately

include 'shared.php';

//Get the MySQL/ConsentCheq credentials
include("cred.inc");

//include functions to lighten the load
include ("functions/helperFunctions.php");

//Test to confirm that we have a valid login
session_start();

//Set the max timeout at 5 minutes
ini_set('max_execution_time', 300);

$userid = "";
if(isset($_GET['uid'])) {
    $userid = strtolower($_GET['uid']);
	
	//write analytics 
	analytics("PAGE LOAD - ONBOARDING.PHP",$userid);

	
	//Test to see if this is a valid user, and that they haven't chosen a password yet
	
	//MySQL 
	include("conn.inc");
	
	//check for valid credentials
	$sql = "SELECT * FROM `users` WHERE `id` = '" . $userid . "'";
	
	//send the query
	$result = $connection->query($sql);
		
	//We'll get a number of rows of credentials
	if ($result->num_rows > 0) {
		
		// output data of each row
		$userid = "";
		$pass = "";
		$email = "";
		$level = "";

		while($row = $result->fetch_assoc()) {
			$userid =  $row["id"];
			$email = $row["email"];
			$pass = $row["secret"];
			$level = $row["level"];
		}

		
		//test against the hashed password
		if (password_verify("********", $pass)) {
		
			//redirect to the new user page
			header ("Location: newuser.php?uid=" . $userid);
			exit();

		} else {
			
			//This user already has a password, redirect to the login directly
			header ("Location: login.php?email=" . $email);
		}		
	} else {
		
		//redirect to marketing page - the userid is bad
    	header ("Location: https://www.privacyux.com");
    	exit();
	}
	
	mysqli_close($connection);

	


} else {
	
	//no uid passed redirect to our sales page redirect to marketing page - the link is bad and has no uid
    header ("Location: https://www.privacyux.com");
    exit();
	
}






?>