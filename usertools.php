<?php

//This is a page to maintain the functions that REQUIRE a user login to execute

//It is called in separate post/get calls
//USE THESE COMMANDS FOR MySQL ACCESS 
//Use these commands for async calls

//There is no UI to this page - It echoes a JSON string with at least the following elements:
//  - rtn  : ok on success, error or fail in other cases
//  - rtnmsg : A plain-ish language explanation

//Get the MySQL/ConsentCheq credentials
include("cred.inc");

//include functions to lighten the load
include ("functions/helperFunctions.php");
include ("functions/emailFunctions.php");
include ("functions/databaseFunctions.php");
include ("functions/userFunctions.php");
//include ("functions/savedataFunctions.php");



//Test to confirm that we have a valid login
session_start();
if (!isset($_SESSION['auth'])) {

	$rtn = json_encode(array(
		"rtn"  => "fail",
		"rtnmsg" => "no user logged in"
		));
	       
	echo $rtn;	
	exit();
}


//Set the max timeout at 5 minutes
ini_set('max_execution_time', 300);


// *** COMMANDS *********************

// Get the Command
$command = strtolower(getPassedData("cmd"));	

	//---------------------------
	// TEST "HELLO WORLD" COMMAND
	//---------------------------
	if ($command=="test") {

		$rtn = json_encode(array(
		"rtn"  => "ok",
		"rtnmsg" => "test command successful"
		));
		       
		echo $rtn;	

	}
		
	
	//---------------------------
	// CREATE NEW USER / UPDATE USER LEVEL
	//---------------------------
	elseif ($command=="newuser") {

		//get passed data
		$email = getPassedData("email");
		$id = getPassedData("id");

		//get the password, or set it to the word "password"
		$secret = getPassedData("secret");
		if ($secret == "") {
			$secret = "password";
		} else {
			//??? validate the password!!!
		}

		$userLevel = getPassedData("userlevel");
		if ($userLevel == null) { $userLevel = 0; }

		//validate the user email
		if ($email == "") {
			$email = $id;
		} else {
			if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
				$rtn = json_encode(array(
					"rtn"  => "fail",
					"rtnmsg" => "invalid email address",
					"email" => $email
					));
				       
				echo $rtn;	
				die();
			}
		}		
		

		//call the function to create database entry
		$objReturn = json_decode(createUser($email,$secret,$userLevel));
			
		//pass the return value from the funciton through
		echo json_encode($objReturn);

	}


    //-----------------------------
    // UPDATE PASSWORD
    //-----------------------------		
	elseif ($command=="npass") {
		
		//get passed data
		$id = getPassedData("userid");
		$email = getPassedData("email");
		$updatePassword = 	getPassedData("newPassword");
		$updatePasswordConfirm = getPassedData("newPasswordConfirm");
		
		//validate input - we need either an email or id as well as matching non-empty password entries
		if ( $updatePassword == "" || $updatePasswordConfirm == "") {

			$rtn = json_encode(array(
				"rtn"  => "fail",
				"rtnmsg" => "Please pass the appropriate parameters for this command",
				"email" => $email,
				"id" => $id,
				"password" => $updatePassword,
				"confirm" => $updatePasswordConfirm
				));
			       
			echo $rtn;	
			die();
			
		}
		
		if ($updatePassword != $updatePasswordConfirm) {

			$rtn = json_encode(array(
				"rtn"  => "fail",
				"rtnmsg" => "Please check that the password confirmation matches",
				"email" => $email
				));
			       
			echo $rtn;	
			die();		
			
		}
		
		if ($email == "") {
			$email = JSON_decode(getEmailFromUserId($id)) -> email;
		}
		
		//hash the password
		$updatePassword = password_hash($updatePassword, PASSWORD_DEFAULT);
		
		//MySQL include library
		include("conn.inc");

		$sql = "";
		if ($id == "") {
			//update the user's password by their email
			$sql = "UPDATE users SET secret = '" . $updatePassword . "' WHERE email = '" . $email . "';";
		} else {
			//update the user's password by their userid
			$sql = "UPDATE users SET secret = '" . $updatePassword . "' WHERE id = '" . $id . "';";
		}

		//send the query
		$result = $connection->query($sql);

		//get the number of affected rows
		$iRows = mysqli_affected_rows($connection);
 
			
		//return the result
		if ($result == "1" && $iRows > 0) {

			$rtn = json_encode(array(
				"rtn"  => "ok",
				"rtnmsg" => "password updated successfully",
				"email" => $email,
				"id" => $id
				));
			       
			echo $rtn;	

		} else {

			$rtn = json_encode(array(
				"rtn"  => "fail",
				"rtnmsg" => "unable to update the user record",
				"email" => $email,
				"affectedRows" => $iRows 
				));
			       
			echo $rtn;	
		}

		mysqli_close($connection);
		
		//log in afterwards
		$_SESSION['auth'] = "1";	
		$_SESSION['userid'] = $id;	
		//$_SESSION['level'] = "0";			
    
	} 

	//-----------------------------
	// CREATE NOTICE COMMAND
	//-----------------------------
	elseif ($command == "createnotice") {

		//The ID of the new notice created returned by this command
		$iNoticeID = null;

		//get passed data
		$userid = getPassedData("userid");
		$email = getPassedData("email"); //this call requires either the email address or userid but userid is more efficient
		$name = getPassedData("name"); //can be blank
		$title = getPassedData("title"); //if blank, set to "Privacy Facts"
		$icon = getPassedData("icon"); //can be blank
		$intro = getPassedData("intro"); //if blank set to default
		
		if ($title == "") {
			$title = "Privacy Facts";
		}

		if ($intro == "") {
			$intro = "Click <span style=''color:#007bff;''>blue</span> facts for more detail.";
		}

		//data validation 
		if ($userid == "" && $email == "") {
			$rtn = json_encode(array(
				"rtn"  => "fail",
				"rtnmsg" => "This command requires a valid email address or user id"
				));
			       
			echo $rtn;	
			die();			
		}

		//call the function to create the notice
		$objReturn = json_decode(createNotice($name, $title, $icon, $intro, $email, $userid));
		


		if ($objReturn -> rtn != "ok") {
			//in case of error or failure, pass the error along
			echo json_encode($objReturn);
		} else {

			//success
			$rtn = json_encode(array(
			"rtn"  => "ok",
			"rtnmsg" => "notice created in database",
			"userid" => $userid,
			"email" => $email,
			"name" => $objReturn -> name,
			"noticeid" => $objReturn -> id
			
			));
			       
			echo $rtn;			
		}	

	}


	//------------------------
	// INSERT ELEMENT
	//------------------------
	elseif ($command =="addelement") {

		//get passed data
		$noticeid = getPassedData("noticeid");
		$order = getPassedData("order");
		
		if ($order == "") {
			
		}

		//data validation 
		if ($noticeid == "" ) {
			$rtn = json_encode(array(
				"rtn"  => "fail",
				"rtnmsg" => "This command requires a valid notice id"
				));
			       
			echo $rtn;	
			die();			
		}

		//call the function to insert the element
		$objReturn = json_decode(insertElement ($noticeid, $order)); 
		
		//pass the return value from the funciton through
		echo json_encode($objReturn);	
		



	}

	// ------------------------
	// INSERT MEMBER
	// ------------------------
	elseif ($command == "addmember") {

		//get passed data
		$elementid = getPassedData("elementid");
		$text = getPassedData("text");
		$format = getPassedData("format");   //format left/right center or a HEX COLOR for a button - can be blank
		$link = getPassedData("link");   //link to a URL or a detail layer
		$type = getPassedData("type");  //Possible types include "fact", "info","button","text", or "frame"  
		


		//data validation 
		if ($elementid == "" ) {
			$rtn = json_encode(array(
				"rtn"  => "fail",
				"rtnmsg" => "This command requires a valid element id to associate the member with"
				));
			       
			echo $rtn;	
			die();			
		}

		//data validation
		if ($type == "") {
			$rtn = json_encode(array(
				"rtn"  => "fail",
				"rtnmsg" => "This command requires a valid type: fact, info, button, text, or frame"
				));
			       
			echo $rtn;	
			die();					
		}

		//call the function to insert the member of the element 
		$objReturn = json_decode(insertMember ($elementid, $text, $format, $link, $type)); 
		
		//pass the return value from the funciton through
		echo json_encode($objReturn);	

	}

	// -----------------------------
	//  INSERT DETAIL LAYER
	// -----------------------------
	elseif ($command == "adddetail") {

		//get passed data
		$linkcode = getPassedData("link");  //the code to link from an element (one to many association)
		$noticeid = getPassedData("noticeid");  //the notice id to associate the detail with
		$head = getPassedData("head");
		$foot = getPassedData("foot");
		$footlinktext = getPassedData("linktext");   
		$linkurl = getPassedData("linkurl");   


		//data validation 
		if ($noticeid == "" ) {
			$rtn = json_encode(array(
				"rtn"  => "fail",
				"rtnmsg" => "This command requires a valid notice id to associate the detail layer with"
				));
			       
			echo $rtn;	
			die();			
		}

		//data validation 
		if ($linkcode == "" ) {
			$rtn = json_encode(array(
				"rtn"  => "fail",
				"rtnmsg" => "this command requires a valid link code to identify the new detail layer with"
				));
			       
			echo $rtn;	
			die();			
		}

		//data validation
		if ($head == "") {
			$rtn = json_encode(array(
				"rtn"  => "fail",
				"rtnmsg" => "This command requires at least some text in the heading"
				));
			       
			echo $rtn;	
			die();					
		}

		//call the function to insert the detail layer into the database
		$objReturn = json_decode(insertDetail ($linkcode, $noticeid, $head,$foot,$linktext, $linkurl)); 
		
		//pass the return value from the function through
		echo json_encode($objReturn);	

	}

	// -----------------------------
	//  INSERT FACTOR INTO DETAIL LAYER
	// ---------------------------------
	elseif ($command == "addfactor") {


		//get passed data
		$detailid = getPassedData("detailid");  //the id of the detail layer to add this factor to
		$text = getPassedData("text");  //the text line of the factor
		$info = getPassedData("info");  //if the factor is clicked, this is the text that is shown
		$link = getPassedData("link");  //the URL or other detail to redirect to if a factor is clicked
		$order = getPassedData("order"); //an integer in numerical order to show the factors in 

		//data validation 
		if ($detailid == "" ) {
			$rtn = json_encode(array(
				"rtn"  => "fail",
				"rtnmsg" => "This command requires a valid detail layer id to associate the factor with"
				));
			       
			echo $rtn;	
			die();			
		}

		//data validation
		if ($text == "") {
			$rtn = json_encode(array(
				"rtn"  => "fail",
				"rtnmsg" => "This command requires at least some text to put into the factor"
				));
			       
			echo $rtn;	
			die();					
		}

		//call the function to insert the detail layer into the database
		$objReturn = json_decode(insertFactor ($detailid, $text, $link, $info, $order )); 
		
		//pass the return value from the function through
		echo json_encode($objReturn);


	}




	//-----------------------------
	// SENDEMAIL COMMAND
	//-----------------------------
	elseif ($command=="sendemail") {

		//get passed data
		$email = getPassedData("email");
		
		//format plus signs and spaces
		$email = formatSignsSpaces($email);

		//validate the  email
		if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
			$rtn = json_encode(array(
				"rtn"  => "fail",
				"rtnmsg" => "invalid email address",
				"email" => $email
				));
			       
			echo $rtn;	
			die();
		}
		
	    //!!! Send an email, passing a unique token for a user to reply with to verify that they are valid and link to a user
		//??? do I need to create a GUID for every user? The email seems like a good key, though
        $subject = 'Welcome to PrivacyUX Livestart';
        $message = 'Hello and welcome to PrivacyUX.  In order to complete your setup, please click on the following url:<br><a target="vitamax" href="' . $responseurl . '">' . $responseurl . '</a>';
        $headers = 'From: noreply@privacycheq.com' . "\r\n" .
            'Reply-To: noreply@privacycheq.com' . "\r\n" .
            'Content-Type: text/html; charset=ISO-8859-1' . "\r\n" .
            'X-Mailer: PHP/' . phpversion();
			
		//call the function to send an email message
		$objReturn = json_decode(sendEmail($email,"","",""));
			
		//pass the return value from the funciton through
		echo json_encode($objReturn);
           


	} 
	
	//--------------------------------
	// Get User ID from existing email address
	// -------------------------------
	elseif ($command=="idfromemail") {
		
		//get passed data
		$email = getPassedData("email");
		
		//format plus signs and spaces
		$email = formatSignsSpaces($email);
		
		//call the function to create database entry
		$objReturn = json_decode(getUserIdFromEmail($email));
			
		//pass the return value from the funciton through
		echo json_encode($objReturn);		
		
		
	}

	//--------------------------------
	// Set user level
	// -------------------------------
	elseif ($command=="setuserlevel") {
				
		//get passed data
		$userid = getPassedData("userid");
		$userLevel = getPassedData("userlevel");  // 0 = not a member; 1 = paid
		if ($userLevel == "") { $userLevel = "0"; }
			
		if ($userid == "") {
			
			//get passed data
			$email = getPassedData("email");
			
			if ($email == "") {  //no id and no email? 
				//fail - no user identification
				$rtn = json_encode(array(
					"rtn"  => "fail",
					"rtnmsg" => "no user identification"
					));
					   
				echo $rtn;	
				die();				
			}
			
			//format plus signs and spaces
			$email = formatSignsSpaces($email);
			
			//call the function to create database entry
			$objReturn = json_decode(getUserIdFromEmail($email));
			
			if ($objReturn -> rtn != "ok") {
				echo json_encode($objReturn);
				die();				
				
			}
				
			//get a user id
			$userid = $objReturn -> id;
			
		}
				
		//call the function to create database entry
		$objReturn = json_decode(updateUserLevel($userid,$userLevel));
			
		//pass the return value from the funciton through
		echo json_encode($objReturn);		
		
		
	}
	
	
	
	
	//-----------------------------
	// ERROR CATCH CONDITION - ELSE
	//-----------------------------
	else {

		//Invalid CMD passed
		$rtn = json_encode(array(
		"rtn"  => "error",
		"rtnmsg" => "please enter valid command"
		));
		       
		echo $rtn;	

	}




?>


