<?php

// This library holds the code to run during the build process to manage the JSON object of the PFIN
//   it delivers a block of JavaScript into the beginning of the web page


//!!! TODO: Get the JSON from the database, unless we ALREADY HAVE IT IN LOCAL STORAGE?!?

if ($userid != "") {

	//get the notice JSON from the database for that particular user
	include ("functions/retrievedataFunctions.php");	
	include ("cred.inc");
	
	$noticeid = "";
	$rtn = getNoticeFromUserId($userid);
	
	$objReturn = json_decode($rtn);
	if ($objReturn -> rtn == "ok") {
		
		$noticeid = $objReturn -> id;
		echo "var d = " . getJSONData($noticeid) . "; ";
		echo "var userid = '" . $userid . "';";
  
	} else {
		
		//!!! what to do if we don't get a valid user from the passed id?
		echo "var d = " . getJSONData(null);
	}
	
	
} else {
	
	//no userid passed - create a new placeholder user
	$rtn  = createUser();
	$objReturn = json_decode($rtn);
	echo "console.log('" . $rtn . "');";
	if ($objReturn -> rtn == "ok") {
		
		$userid = $objReturn -> id;
		echo "var userid = '" . $userid . "';";
  
	} else {
		
		echo "console.log('" . $rtn . "');";
		//unable to create a placeholder user
		//!!! what to do here?
	}
	
	//deliver the default JSON 
	echo "var d = " . getJSONData(null) . ";";

}

function getJSONData($noticeid = null) {
	
	if ($noticeid != null) {
		$doURL = SERVER_PATH . "/jsonData.php?noticeid=" . $noticeid;		
	} else {
		$doURL = SERVER_PATH . "/jsonData.php";			
	}

	//get JSON from notice id number - PUT cURL
	$ch = curl_init();

	// set url
	curl_setopt($ch, CURLOPT_URL, $doURL);

	curl_setopt($ch, CURLOPT_HTTPGET, TRUE);    
	curl_setopt($ch, CURLOPT_HEADER, FALSE);		
	
	//return the transfer as a string
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

	// $output contains the output string
	$output = curl_exec($ch);

	// close curl resource to free up system resources
	curl_close($ch); 

	return $output;		
	
}




?>